## About this Compendium

This is NOT A TEXTBOOK. This a just a collection of lecture notes
together with weekly exercises and labs. The text and figures in English
are the slides I will show in the lectures, while the text in between in
Norwegian are my notes on what I have to remember to cover during that
lecture. Almost all the figures are taken directly from the textbook
resources:  
<http://www.cs.vu.nl/~ast/books/book_software.html>  
<http://www.pearsonhighered.com/tanenbaum/>

The textbook for this course is the latest addition of Tanenbaums
“Modern Operating Systems”. All credits to Andrew S. Tanenbaum for
writing such great books. Some of the content in this compendium is also
inspired by two other great books on operating systems: William
Stallings’ “Operating Systems” and Abraham Silberschatz, Peter B. Galvin
and Greg Gagne’s “Operating System Concepts”.

We would also like to thank [AnandTech](http://www.anandtech.com/) for
great articles, [Gustavo Duartes](http://duartes.org/gustavo/) for
fantastic illustrations and explanations, and
[LiveOverflow](https://www.youtube.com/channel/UClcE-kVhqyiHCcjYwcpfj9w)
for making great videos which we make use of in many chapters.

You should spend time reading the textbook, but you can spend most of
your time on the parts of the textbook which are covered in this
compendium since those parts are the ones most relevant for the exam and
your learning outcome.

Much of the material in this compendium is inspired by Vegar Johansen,
Hårek Haugerud and Dag F. Langmyhr.

# Om emnet

## About the course

##### Teacher

  - Teacher is Erik Hjelmås  
    [www.ntnu.no/ansatte/erik.hjelmas](www.ntnu.no/ansatte/erik.hjelmas)

  - Lectures, TBL, Theory and Lab

  - Web page: Blackboard

  - Erik’s office is A117 at NTNU

  - Erik’s email is `erik.hjelmas@ntnu.no`

  - Erik’s phone/sms is `93034446`

  - *Teaching assistants Linn-Mari, Sturla and Aksel*

##### Workload, requirements and evaluation

  - 10 credits \(\sim{}\) 15 hrs per week (\(\sim{}\) 300 hrs total),  
    I recommend
    
      - 4 hrs lecture
    
      - 5 hrs lab
    
      - 2 hrs theory questions
    
      - 4 hrs textbook/articles/wiki\*

  - Evaluation based on:
    
      - Final exam

  - Lectures, lab and theory questions

  - *Mandatory work\!* see web page

##### Howto Learn Operating Systems

1.  Book and theory

2.  *Theory and Lab*

3.  Lab: write an operating system

—–

Vi bruker metode 2. Det viktigste er prinsippene og forståelsen for
rollen operativsystemer spiller, og vi tror vi får best læring ift
tidsbruk ved hjelp av mye praktiske oppgaver sammen med teorien.

Som Lab benytter vi forskjellig OS kommandoer og hjelpeapplikasjoner for
å studere hva som skjer, sammen med C-programmering for dypere
forståelse av enkelttemaer (vi oppnår mest læring og forståelse når vi
faktisk utvikler selv det samme som vi studerer). Vi skal også
innarbeide ferdigheter i bruk og drift av operativsystemer via Bash/UNIX
og PowerShell/Windows.

Men skal man virkelig grave seg ned i operativsystemer, så er metode tre
sterkt anbefalt (benyttes ved UiT og UiO), men faren er at man bruker
for mye tid på ’knoting’ og for mye tid på det helt grunnleggende (dvs
man rekker bare bygge et helt enkelt system, veldig annerledes fra
dagens store OSer som Windows og Linux).

## Outcome

##### Today’s Learning Outcome

  - Layering/Abstractions

  - Intro to OS Concepts

  - Basic History

  - Why learning OS?

## Introduction

Operativsystemer er et emne som vil være litt plagsomt for alle “som
bare vil at datamaskiner skal virke”. Dvs hele dette emnet dreier seg om
å lære seg hvordan vi får en datamaskin til å virke best mulig, å kikke
under den behagelig overflaten vi forholder oss vanligvis til. La oss
innføre de begrepene som dominerer operativsystemer:

``` 
                        |
         Abstraksjon:   |   PROSESS     ADRESSEROM     FIL
                        |
         ---------------+----------------------------------
                        |
         Fysisk enhet:  |   CPU         RAM            DISK
                        |
```

De klassiske temaene som alltid har vært en del av operativsystemer er

  - Prosesshåndtering (Process management)

  - Minnehåndtering (Memory management)

  - Input/Output (I/O)

  - Filsystemer

Vi skal i tillegg ta for oss påbygningstemaene Virtualisering og
Sikkerhet.

### What’s an OS?

##### Where’s the OS?

![image](/home/erikhje/office/kurs/opsys/01-intro/tex/../../resources/MOS_3e/01-01.pdf)

  - *kernel mode* og *user mode*

  - skifte mellom disse kalles *mode switch/mode transition*

I all hovedsak kjøres operativsystemet i kernel mode, og mens
brukerprogrammene kjøres i user mode. I kernel mode tillates alle
instruksjoner (dvs alle assemblyinstruksjonene som
datamaskinarkitekturen tilbyr) mens *i user mode tillates ikke
instruksjoner som har med direkte kontroll av maskinen eller
instruksjoner som utfører I/O*.

##### The OS Hides the Ugly Details

![image](/home/erikhje/office/kurs/opsys/01-intro/tex/../../resources/MOS_3e/01-02.pdf)

Så hva er det et OS gjør? Det gjør i all hovedsak to hovedoppgaver:

  - Gir brukeren et ryddig grensesnitt mot maskinvaren.

  - Administrerer ressurser, dvs sørge for at mange prosesser kan kjøres
    “samtidig”, dette kalles *Multitasking* og kan sies å bestå av:
    
      - multiplexing i tid.
    
      - multiplexing i rom.

Tildeling i tid for prosesser P1, P2, P3, eks. en CPU:

``` 
        CPU1 |P1--|P3|P2--------|P3--|P1-------->
                         Tid
```

Tildeling i tid og rom:

``` 
                          ^
        R            CPU1 |
        e            CPU2 |
        s     Minnemodul1 |
        s     Minnemodul2 |
        u     .           |
        r     .           |
        s     .           |
        e     .           |
        r     .           |
                          +------------------>
                                 Tid
```

Husk parallell til deling i Tid og Frekvens i datakommunikasjon.

Alle moderne operativsystemer benytter *Preemptive Multitasking* for å
løse dette, dvs hver prosess for en liten tidsluke å kjøre på CPUn før
prosessen avbrytes (“Preemptes”) og en annen prosess får kjøre.

La oss se mer på grensesnittet. En typisk oppgave i en datamaskin er å
lese inn noe data fra en disk som involverer gjerne flytting av
diskarmen, formatinformasjon om disken, diskblokkadresse, osv. Men siden
vi har et OS å prate med behøver vi ikke bekymre oss for dette, modellen
blir slik istedet:

``` 
        read(fd, buffer, nBytes)
                 |
                 V
           Operativsystemet
                 |
                 V
               Driver
                 |
                 V
           Diskkontroller (hardware)
```

##### KEY PRINCIPLE\!

Tanenbaum, page 4:

> Abstraction is the key to managing complexity. Good abstractions turn
> a nearly impossible task into manageable ones. The first one of these
> is defining and implementing abstractions. The second one is using
> these abstractions to solve the problem at hand.

Eksempelet vi nettopp så: en fil. Hvis vi var ingeniører som jobbet med
konstruksjon av en disk så hadde vi primært brydd oss om hvordan disken
faktisk ser ut fysisk og den fysiske elektronikken på disken, i all
hovedsak ingen abstraksjon. Mens som informatikere er vi opptatt av det
mer overordnede, dvs vi bryr oss bare om at vi har en fil i et filsystem
som vi skal bruke. Men denne filen finnes jo egentlig ikke\! Den er en
abstraksjon av det som egentlig finnes lagret fysisk på disken.

HUSK: begrepet abstraksjon kan kanskje oppfattes som noe vanskelig, men
bruken av det er ikke det, hele hensikten med abstraksjon er å forenkle
noe som er vanskelig.

## History

### 1.-4. Generation

##### 1\. Gen: Vacuum Tubes and Machine Language

![image](/home/erikhje/office/kurs/opsys/01-intro/tex/../img/Colossus.jpg)

Bilde viser Colossus som ble benyttet mot slutten av andre verdenskrig i
Storbritannia for å knekke koder, maskinen var rørbasert (manipulerte
elektriske signaler via vakuumrør).

Første generasjonsmaskiner ble programmert direkte i maskinkode, ikke
noe assembly kode, programmert rent fysisk med flytting av ledninger og
knapper.

*Ikke noe operativsystem.*

##### 2\. Gen: Transistors and Languages

![image](/home/erikhje/office/kurs/opsys/01-intro/tex/../../resources/MOS_3e/01-03.pdf)

Transistoren gjorde datamaskiner pålitelige slik at de kunne produseres
og selges (riktignok veldig dyrt\!).

Programmering ble gjort typisk i FORTRAN og assemblykode, overført til
hullkort og behandlet som vist i figuren: lesing og printing på IBM1401
og beregninger (real computing) på IBM 7094.

Siden mye tid gikk bort med å bære kort og taper frem og tilbake
innførte man systemet med *batch* jobber, dvs en bunke med jobber som
skal utføres. I dag bruker vi fortsatt begrepet batch jobber, dvs en
*batch jobb er en oppgave som kan utføres selvstendig uten kommunikasjon
med brukeren*. Det motsatte av en batch jobb/oppgave/prosess er en
interaktiv jobb/oppgave/prosess.

##### 2\. Gen: Typical Fortran Monitor System Job

![image](/home/erikhje/office/kurs/opsys/01-intro/tex/../../resources/MOS_3e/01-04.pdf)

Batch håndtering var altså et tidlig operativsystemkonsept, og et
primitivt grensesnitt vises her i form av kontrollkort.

Operativsystemene var stort sett FMS (Fortran Monitoring System) og
IBSYS (fra IBM).

Vis tidslinjen for operativsystemer:

<span><http://en.wikipedia.org/wiki/Timeline_of_operating_systems></span>

##### 3\. Gen: ICs and Multiprogramming

![image](/home/erikhje/office/kurs/opsys/01-intro/tex/../../resources/MOS_3e/01-05.pdf)

Etter utviklingen av integrerte kretser (ICr) kunne man bygge mer
generelle datamaskiner og via IBM System/360 kunne man ha flere
programmer i minne samtidig: *Multiprogrammering*.

Disse systemene kom rett før en annen stor innovasjon på 60-tallet:
*Timesharing* (og sammen med det: protection/access control/security).
Opprinnelig utviklet i CTSS (Compatible Time Sharing System) ved MIT i
1962, men først skikkelig realisert med MULTICS videre utover 60-tallet.
MULTICS står svært sentralt i operativsystemhistorien.

Timesharing var altså en ønsket utvikling fra batch systemer.

Se mer om Multics på <span><http://www.multicians.org></span>

##### 4\. Gen: LSI

  - With (Very) Large-Scale Integration (V)LSI, general-purpose small
    computers could be built

  - Typically our current PCs (from 1980 -)

### Windows/Unix/Mac

##### Windows

![image](/home/erikhje/office/kurs/opsys/01-intro/tex/../../resources/MOS_4e/11-1.pdf)

IBM hadde et behov for å skaffe et operativsystem til deres PC prosjekt.
De kontaktet Bill Gates, og ba om å få bruke hans BASIC-system, og om
han kunne skaffe et OS. Gates ba dem ta kontakt med Gary Kildall, som
nektet å inngå noe samarbeid. Gates endte dermed opp å kjøpe DOS.

Se Tanenbaum side 15, les avsnittet “In the early 80’s...”.

Mac var først ute med GUI etter Xerox ikke syns det var interessant.

Ingen sikkerhet på Windows før WinNT serien.

Vis Windows historikken:  
<http://upload.wikimedia.org/wikipedia/commons/6/6d/Windows_Updated_Family_Tree.png>  
og <http://www.levenez.com/windows/windows.pdf>

##### Unix/Linux

  - Ken Thompson developed a stripped-down version of MULTICS on a PDP-7
    he got hold of in 1969

  - A large number of flavors developed (SystemV or Berkely-based)

  - The GNU-project started in 1983 by Richard Stallman

  - Unified with POSIX interface specification in 1985

  - Minix in 1987 inspired Linus Torvalds to develop Linux (released in
    1991)

—–

Se <http://en.wikipedia.org/wiki/Tanenbaum-Torvalds_debate>

og Unix historien:  
<http://www.levenez.com/unix/unix.pdf>

![image](/home/erikhje/office/kurs/opsys/01-intro/tex/../img/iran-stuxnet.png)

I dag:

(Stuxnet: få noen til å dytte en USB minnepinne i en PC som er på en
eller annen måte i et nettverk sammen med andre PCer som kan være
knyttet til en Siemens PLC kontroller, kanskje Stuxnet kommer på en
laptop som tas med inn i et nettverk hvor PLC kontrollere finnes knyttet
til andre PCer i samme nettverk el.l. Stuxnet utnytter minst  
<http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2008-4250>  
<http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2010-2568>  
<http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2010-2729>  
<http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2010-2772>  
Dette er altså den første kjente ormen som er rettet mot direkte
industriell skade, dvs den gjør ikke noe ondsinnet mot maskina de hvis
du blir infisert, dem gjør ikke noe for å samle kredittkortinfo eller
for senere å kunne kreve penger for noe, den er ekstremt målrettet kun
mot industriell skade\!)

Det er en stor fordel for alle informatikere å kjenne til hvordan
datamaskiner og operativsystemer fungerer slik at vi kan forstå
hverdagen vår.

*For vår del gjelder det spesielt å forstå hvordan et dataprogram
utføres i en datamaskin slik at vi kan forstå hvordan sårbarheter
oppstår og utnyttes slik som f.eks. Stuxnet.*

## Closing Remarks

##### Metric Units

![image](/home/erikhje/office/kurs/opsys/01-intro/tex/../../resources/MOS_3e/01-31.pdf)

Bare pugg i vei...

##### KB og KiB?

*We do not separate between KB,MB,GB,TB,... and KiB,MiB,GiB,TiB,... in
this course. 1KB=1KiB is \(2^{10}\) unless otherwise stated.*

<http://en.wikipedia.org/wiki/Kibibyte>

Det har vært og er en del forsøk på å få oss til å konsekvent skrive
1KiB når vi mener \(2^{10}\) og 1KB når vi mener \(10^{3}\). Dette er en
god ide men desverre ikke gjennomførbar i praksis. Men det er viktig å
vite at dette gjennomføres som regel hos lagringsleverandører, dvs når
vi kjøper en 1TB disk og blir skuffet fordi den faktisk ikke er så stor
så er det fordi vi har kjøpt \(10^{12}B\) og ikke \(2^{40}B\), og
\[\frac{10^{12}}{2^{40}}\approx 0.909\] mao, disken din er ca 909GB
istedet for 1000GB.

##### Why Study Operating Systems?

*You will never write an OS ... (5 - 50 million lines of code)*

1.  Understand how you create a complex system (modules\!)

2.  Understand to easily troubleshoot problems

3.  Understand to program optimally

4.  Understand the security challenges (malware\!)

5.  The Concept of parallellism

6.  The Concept of abstraction and virtualization

<!-- end list -->

  - Sikkerhet\!

  - Ytelse\!

  - Ferdigheter\! (kommandolinje/scripting)

## Theory questions

1.  Forklar kort hva som er de to hovedoppgavene til et operativsystem.

2.  Hva het det operativsystemet som anses som forløperen til UNIX og
    når omtrent ble UNIX lansert første gang? Hvem skrev den første
    utgaven av UNIX?

3.  Hva mener vi med begrepet *multiprogrammering* og hvilke fordeler
    innebærer dette i forhold til ren batch-kjøring.

## Lab exercises

1)  **Windows-oppsett**  
    *NB\! Du behøver ikke gjøre denne oppgaven nå, men den er plassert
    her slik at du kan se hvordan lærern sin Windows-omgivelse er satt
    opp.*
    
    Du trenger en Windows med PowerShell og denne øvingen antar at du
    benytter Windows 10 (som du sikkert har tilgang til via DreamSpark
    som student, hvis du installerer den i en virtuell maskin så husk å
    installer også det som som regel heter “Guest additions” som gjerne
    gjør at du får bedre skjermoppløsning og kan klippe/lime mellom den
    virtuelle maskinen og laptopen din). Alternativt kan du i 90 dager
    bruke en av de du finner her:  
    <https://dev.windows.com/en-us/microsoft-edge/tools/vms>
    
    For å lett kunne installere verktøy vi trenger underveis i emnet, så
    anbefales det at du bruker den nye pakkehåndteringsmekanismen
    (PackageManagement/OneGet) i Windows 10. Via en pakkebrønn/kilde som
    heter Chocolatey finner du det meste av fri programvare pakket for
    Windows. NB\! Les denne først:  
    <http://codebetter.com/robreynolds/2014/10/27/chocolatey-now-has-package-moderation/>
    Vi bør alltid være bevisste på hva vi installerer på maskina vår, så
    sjekk at pakkene du har tenkt installere er "Approved" i
    Chocolatey-galleriet før du installerer.
    
        # PowerShell -> Run as administrator 
        
        Set-ExecutionPolicy RemoteSigned
        Install-PackageProvider Chocolatey
        
        # Oppdater
        Install-Module -Name PSWindowsUpdate
        Get-WUInstall -Verbose
        Get-WUInstall -Install
        
        # Restart PowerShell -> Run as administrator
        
        Install-Package sysinternals, nano, vim, notepadplusplus
        
        # Exit PowerShell, start PowerShell as yourself (not adminstrator)
        
        C:\chocolatey\bin\nano $profile
        # enter the following line, then save the file
        $env:Path += ";C:\Chocolatey\bin"
        
        # Restart PowerShell (as yourself), you should now be able to run e.g.
         
        nano
        procexp
        
        # For øvrig, for å se alle kommandoer du har for package management:
        Get-Command -Module PackageManagement
        # Desverre ingen upgrade/update-package kommando enda..
        # men for å se mulige oppdateringer (alt nedenfor på en linje):
        (Get-Package -ProviderName chocolatey) + 
         (Get-Package -ProviderName chocolatey | 
         ForEach-Object -Process { Find-Package 
          -ProviderName chocolatey -Name $_.Name }) | 
         Group-Object -Property Name | Where-Object { 
          $_.Group[0].Version -igt $_.Group[1].Version } | 
         Select-Object { $_.Group[0].Name, 
          $_.Group[0].Version, $_.Group[1].Version }

2)  **Linux-oppsett**  
    (*MERK: hvis du vil igang med oppgavene enklest mulig, må du gjerne
    droppe denne oppgaven og istedet logge deg inn på en linux server
    hvis du har tilgang til en slik, som f.eks. `login.stud.ntnu.no`*)  
    Sørg for at du har en kjørende linux maskin av et eller annet slag,
    gjerne Ubuntu (<http://www.ubuntu.com>) eller lubuntu hvis du vil ha
    en minst mulig ressurskrevende versjon av en Linux distribusjon,
    <http://lubuntu.net/>). Hvis du bruker windows til vanlig, så
    installer gjerne linux på en virtuell maskin. Du kan lage virtuelle
    maskiner med VMware Player
    (<http://www.vmware.com/go/downloadplayer/>) eller VirtualBox
    (<http://www.virtualbox.org>). Anbefalt oppskrift er
    
    1.  Last ned og installer VMware Player/VirtualBox.
    
    2.  Lag en virtuell maskin i VMware Player/VirtualBox (velg at det
        skal være linux og ubuntu).
    
    3.  Last ned sist LTS-release (Long Term Support) CD-iso-fil av
        Ubuntu Desktop (<http://www.ubuntu.com>) (eller siste versjon av
        lubuntu).
    
    4.  Endre settings i den virtuelle maskinene du laget slik at
        CD-ROM-leseren er knyttet til iso-filen du nettopp lastet ned.
    
    5.  Start den virtuelle maskinen og installer Ubuntu på den.
    
    6.  Last ned en klient for en eller annen skytjeneste (f.eks
        Dropbox), og opprett en bruker for denne dersom du ikke allerede
        har dette. Sett deretter opp en mappe som synkroniseres mellom
        din virtuelle maskin og den fysiske maskinen.
    
    Alternativt kan du enable Developer mode på Windows 10, og
    installere Windows Subsystem for Linux og deretter installere Ubuntu
    fra Microsoft Store, se [oppskriften fra Adrian
    Lund-Lange](https://gist.github.com/p3lim/7174909c78360606f3334bad4a0262f5).
    
    Se også videoen [Introduction to Linux - Installation and the
    Terminal - bin 0x01
    (LiveOverflow)](https://www.youtube.com/watch?v=navuBR4aJSs)

3)  **(VIKTIGST\!\!\!) Kommandolinje Unix/Linux**  
    Bli kjent med kommandolinje Unix/Linux hvis du ikke er godt kjent
    med den fra før. Gjennomfør tutorial en til fem på
    <http://www.ee.surrey.ac.uk/Teaching/Unix/index.html> (les også
    gjennom “introduction to the UNIX operating system” før første
    tutorial). Merk: i del 2.1 av denne tutorialen så bes du om å
    kopiere en fil `science.txt`. Denne filen finnes ikke hos deg, så
    last istedet den ned med kommandoen  
    `wget http://www.ee.surrey.ac.uk/Teaching/Unix/science.txt`  
    *MERK: Neste uke forventes det at du kan alle disse kommandoene i
    fingrene\!*
    
    Hvis du vil lære Linux på en litt mer moderne og kanskje morsommere
    måte så prøv [Linux Journey](https://linuxjourney.com/).

4)  Bli kjent med programvarepakkesystemet til Debian-baserte
    distribusjoner (lærer går gjennom dette).

5)  Bli kjent med en teksteditor. Hvis du er logget inn på en
    linux-server via ssh så kan du bruke `nano` som viser deg en meny
    nederst på skjermen med hva du kan gjøre. Hvis du har Linux med GUI
    (f.eks. Ubuntu i en virtuell maskin) og ønsker å gjøre det enklest
    mulig, bare bruk `gedit` som er som notepad. Hvis du vil lære deg en
    teksteditor som alle systemadministratorer må kunne, så kan du bruke
    anledning til å lære deg `vi` med denne utmerkede tutorial:
    <http://heather.cs.ucdavis.edu/~matloff/UnixAndC/Editors/ViIntro.pdf>.

6)  **(VIKTIGST\!\!\!) C-programmering på Linux**  
    Lag deg en directory `hello` og i denne lag en fil `hello.c` som
    inneholder:
    
        #include <stdio.h>
        int main(void) {
          printf("hello, world\n");
          return 0;
        }
    
    Kompiler denne til en kjørbar (eksekverbar) fil med `gcc -Wall -o
    hello hello.c` (`-Wall` betyr Warnings:All og er ikke nødvendig, men
    kjekk å ta med siden den hjelper oss programmere nøyere) og kjør den
    med `./hello`. Kjør den også ved å angi full path (dvs start
    kommandoen med `/` istedet for `./`). Sjekk om environment
    variabelen din `PATH` inneholder en katalog `bin` i hjemmeområdet
    ditt (`echo $PATH`). Hvis den ikke gjør det, så lag `bin` (`mkdir
    ~/bin`) hvis den ikke finnes, og legg til den i path’n med
    `PATH=$PATH:~/bin` (gjør evn endringen permanent ved å editere filen
    `~/.bashrc`). Kopier `hello` til `bin` og kjør programmet bare ved å
    skrive `hello`.
    
    Hvis du ikke har programmert i C før, så les gjennom f.eks. denne  
    [Learn C Programming, A short C
    Tutorial](http://www.loirak.com/prog/ctutor.php)
    
    Det anbefales også å se videoen [Writing a simple Program in C - bin
    0x02 (LiveOverflow)](https://www.youtube.com/watch?v=JGoUaCmMNpE)
    
    Sørg for å ha gode rutiner for kvalitetssikring av koden din, f.eks.
    statisk kodeanalyse med `clang-tidy`  
    `clang-tidy -checks='*' kode.c --`
    
    Egentlig burde vi også lese denne  
    [How to C in 2016](https://matt.sh/howto-c) og denne  
    [Modern C](http://icube-icps.unistra.fr/index.php/File:ModernC.pdf)
    samt være klar over hva som finnes her  
    [SEI CERT C Coding
    Standard](https://wiki.sei.cmu.edu/confluence/display/c/SEI+CERT+C+Coding+Standard)

7)  **Litt mer C-programmering og kodekvalitet**  
    Klipp og lim inn det siste eksempelet under “3. Loops and
    Conditions” fra [Learn C Programming, A short C
    Tutorial](http://www.loirak.com/prog/ctutor.php) og kjør  
    `gcc -Wall kode.c`  
    `clang-tidy -checks='*' kode.c --`  
    Klarer du forbedre koden ut fra hva gcc and clang-tidy forteller
    deg?
    
    Vi skal ikke bli ekspert-programmerere i C i dette emnet, vi skal
    bare bruke C littegrann for å lære oss om operativsystemer, men det
    er viktig at vi blir vant til å sjekke kodekvaliteten vår nå som vi
    har programmert i flere emner allerede. I dette emnet er det greit
    at vi ikke alltid skrive optimal og sikker kode (siden det fort blir
    mange ekstra kodelinjer som ikke nødvendigvis hjelper oss lære
    operativsystemer bedre), MEN VI MÅ ALLTID VÆRE BEVISSTE PÅ AT KODEN
    VÅR KAN HA SVAKHETER/SÅRBARHETER.

# Datamaskinarkitektur

## Outcome

##### Learning Outcome

  - Layering/Abstraction

  - Performance

  - Basic hardware overview

  - Conceptual framework

Nå i begynnelsen kan man benytte følgende figur for å forstå hvordan en
datamaskin henger sammen:

##### CPU, Memory, I/O

![image](/home/erikhje/office/kurs/opsys/02-hw-review/tex/../../resources/MOS_3e/01-06-crop.pdf)

De viktigste komponentene, som vi snart skal lære en hel del mer om, er:

  - **CPU**: Central Processing Unit - Hovedprosessoren. Dette er
    datamaskinens "hjerne", og er den enheten som vanligvis er ansvarlig
    for å kjøre programmene vi ønsker å kjøre.
    
      - **MMU**: Memory Management Unit - En sentral del som lar hvert
        program som kjøres få sitt eget minneområde. Denne skal vi se
        nærmere på i kapittel [7](#chap:memman-vm-pagigng-seg).

  - **Memory/RAM** Random Accessible Memory - Hovedminnet til en
    datamaskin. Dette benyttes av CPU til å lagre informasjon den jobber
    med; samt at den leser programmene sine fra minnet.

  - **Controllers** Diverse kontrollere er ulike enheter som er knyttet
    til datamaskinens sentrale bus, og blir benyttet av CPU for å få
    informasjon ut og inn av datamaskinen. Disse består normalt sett av
    en egen prosessor, litt minne, noe programvare (firmware) og noen
    grensesnitt.

## Basic hardware

##### What is software?

What is the software running on your CPU made of?

  - The icon clicked to start the program?

  - The GUI appearing?

  - The C-code written to create the program?

  - Something else?

Den første enheten en skal se på er hovedprosessoren (CPU). Man kaller
ofte CPU for hjernen til datamaskinen, da det er CPU sin oppgave å
utføre programmet. De fleste programmer utfører beregninger på ett
eller annet nivå, og da er det CPU som regner, eller "tenker" seg
gjennom disse beregningene.

Hva er egentlig ett program? "Alle" har brukt en datamaskin, og har da
dobbelklikket på ett ikon for å starte ett program. For brukere som bare
er vant med ett grafisk grensesnitt er gjerne ett program ikonet de
klikker på, og grensesnittet som så kommer opp. Men hva er under
panseret?

De fleste IT-studenter har også skrevet litt kode (f.eks i C) som de har
kompilert og kjørt. Da vet en at program gjerne lages ved å skrive
programkode (tekst) som så på ett eller annet vis blir tolket, og
deretter kjørt. Men hva skjer egentlig under panseret? Hva gjør en
kompilator?

### Instructions

##### Instructions

Consider these simple C-Programs

    int main() {        int main() {
      int a = 2;          return 2 + 3;
      int b = 3;        }
      int a = a + b;  
      return a;       
    }

These programs can be expressed somewhat like so:

    # Store the value 2 somewhere (called a)
    # Store the value 3 somewhere (called b)
    # Add the value of b to a
    # Return the value of a

Expressed in a language like assembly it might look something like this

    movl eax, 0x2
    movl ebx, 0x3
    addl eax, ebx
    ret

Når man skriver programvare i leselige språk (C, C++, Python, Bash, PHP
etc.) må disse språkene gjennom en "konvertering" før en CPU kan kjøre
programmet. En CPU kan ikke lese C. Denne konverteringen kalles gjerne
for tolking (Python, Bash, PHP) eller kompilering (C, C++).

Når kode kompileres vil kompilatoren analysere koden som er skrevet, og
deretter bryte den ned til små enkeltoperasjoner som CPU kan utføre. I
eksempelet over er to ulike C-program begge beskrevet språklig med de
samme 4 enkeltoperasjonene. Siden en CPU ikke leser språk er disse 4
enkeltoperasjonene beskrevet i ett språk som heter assembly. En linje
assembly tilsvarer en CPU instruksjon.

#### ISA

##### Instruction Set Architecture

  - Elec. Engineer: Microarchitecture

  - Comp. Scientist: Instruction Set Architecture (ISA):
    
      - native data types and *instructions*
    
      - *registers*
    
      - addressing mode
    
      - memory architecture
    
      - interrupt and exception handling
    
      - external I/O

Hver CPU type har et bestemt sett med instruksjoner den kan utføre
(instruksjonssettet, som er forskjellig mellom de ulike arkitekturene
f.eks. Intel/AMD X86 som vi skal bruke, Arm (som er i mobiltelefonene og
nettbrettene) og Sun SPARC som var en slager på kraftige
arbeidsstasjoner før). Instruksjonssettet er det vi som programmerere
ser av datamaskinarkitekturen, dvs det vi kan bruke direkte for å
programmere på lavest mulige nivå. Vanligvis bruker vi den symbolske
representasjonen av selve maskininstruksjonene: assembly kode.

Mens vi som informatikere som regel ikke bryr oss om lavere nivå enn
instruksjonssettet, er derimot elektroingeniørene opptatt av
*Mikroarkitekturen* som dreier seg om hvordan instruksjonene faktisk
skal implementeres i elektroniske komponenter for å lage en fysisk CPU.

##### Regular instructions

Some instructions are "always" available:

  - **Moving data**: mov

  - **Math functions**: add, sub

  - **Function related**: call, ret

  - **Jumping**: jmp, je, jne

  - **Comparing**: cmp

  - **Stack-manipulasjon**: push, pop

### Registers

##### Registers

  - **Data-registers (General Purpose registers)**: Registers used to
    hold information used in the running program.
    
      - AX, BX, CX, DX, SP, BP, SI, DI
    
      - R8, R9 .... R15 (64-bit only)

  - **Instruction Pointer/Program Counter (EIP/RIP)**

  - **Instruction Register (IR)**

  - **Stack pointer (ESP/RSP)**

  - **Base pointer (EBP/RBP)**

  - **PSW - program status word (EFLAG/RFLAG)**

[Se sammenheng mellom 16/32/64-bits versjoner av
registerne](https://en.wikibooks.org/wiki/X86_Assembly/X86_Architecture#General-Purpose_Registers_\(GPR\)_-_16-bit_naming_conventions).

Når lange lister med instruksjoner (les: programmer) kjøres på CPU er
det ofte behov for å lagre små biter med informasjon som benyttes i
utførelsen av programmet. En CPU har en rekke med "general purpose"
registre, som er små minneområder hvor CPU kjapt kan lagre informasjon
den trenger igjen snart.

Det finnes også små registre som benyttes av maskinvaren for å kunne
kjøre programvare. Det er for eksempel behov for å ha ett register til
å holde den instruksjonen som kjøres på ett gitt tidspunkt. Det er også
behov for ett register som inneholder adressen til (les: peker på) neste
instruksjon som skal kjøres. Det er og normalt å ha ett eller flere
registre som inneholder bits som påvirker hvordan CPU kjører, og bits
som gir tilleggsinformasjon fra instruksjoner som allerede er kjørt.
Disse kalles gjerne for flagg-registre.

  - Instruction Pointer/Program Counter (EIP/RIP)  
    Instruksjonspekeren. Den peker på neste instruksjons som skal
    kjøres.

  - Instruction Register (IR)  
    instruksjonsregisteret (ikke synlig for oss), den er et internt
    register på CPUn som inneholder den instruksjonen som kjøres akkurat
    nå.

  - Stack pointer (ESP/RSP)  
    peker til toppen av stack

  - Base pointer (EBP/RBP)  
    peker til starten av “current stack frame”, dvs nåværende
    arbeidsområde på stacken (hver gang du kaller en funksjon i koden
    din startes en ny stack frame)

  - PSW - program status word (EFLAG/RFLAG)  
    kontrollbits (av og til kalt flaggregisteret), f.eks. bit 12 og 13
    er user mode/kernel mode bits

Operativsystemet må være klar over alle registre siden disse må lagres
unna når det svitsjes mellom prosesser (tidsmultiplexing).

### Running a program

Basert på det vi kan til nå kan vi tenke oss at det CPU gjør tilsvarer
ca følgende pseudokode:

##### CPU Workflow

    while(not HALT) { # Så lenge maskinen kjører
      IR=Program[PC]; # Hent instruksjonen PC peker på til IR
      PC++;           # Tell opp hva PC peker på
      execute(IR);    # Kjør instruksjonen som ligger i IR
    }

Demo:  
<https://www.cs.ucy.ac.cy/courses/EPL222/Flash/ANIMATIO/INSTRUCT/IECYCLE.SWF>

### Interrupts

##### CPU Workflow - With interrupts

``` 
    while(not HALT) {
      IR = mem[PC];   # IR = Instruction Register
      PC++;           # PC = Program Counter (register)
      execute(IR);
      if(IRQ) {       # IRQ = Interrupt ReQuest
        savePC();
        loadPC(IRQ);  # Hopper til Interrupt-rutine
      }
    }
```

For at en skal kunne behandle hendelser som ikke kan planlegges på
forhånd åpner man opp for avbrudd, eller "interrupt". I praksis
fungerer dette slik at når ett interrupt kommer vil CPU starte å
behandle dette interruptet istedet for å fortsette på programmet som
kjørte. Når interruptet er behandlet vil CPU gå tilbake til programmet
som kjørte.

### RAM

##### Registers vs Memory

A typical Intel CPU contains registers enough to hold some hundred bytes
of data.

All useful software needs more code and variables than that. This is why
we have RAM.

RAM is a much larger (and slower\!) data storage. Typically a computer
have multiple gigabytes of RAM.

Da vi normalt sett har behov for en del plass for å lagre programkode og
variabler, så har vi behov for mer lagringsplass enn bare registrene.
Man har derfor internminne (RAM) hvor vi kan lagre kode og data som CPU
ikke arbeider på "akkurat nå". CPU kan da kopiere data til/fra
RAM/registre.

##### Registers vs Memory

Register contents *belong to the current running program* and the
operating system

There can be *several programs loaded in memory* (this is called
multiprogramming)

#### Memory structure

##### Stack

![image](/home/erikhje/office/kurs/opsys/02-hw-review/tex/../img/stack.png)

Før vi begynner å se hvordan minnet er oganisert trenger vi å forstå
hvordan datastrukturen "stack" fungerer.

En stack er en veldig enkel datastruktur som har to operasjoner. Den har
"push" som betyr at man legger ny data på stacken, og den har "pop" som
betyr at man tar ut data fra stacken. En stack skal alltid få lagt data
inn på toppen, og data fjernet fra toppen. Det som først blir lagt inn
på stacken er det siste som blir fjernet.

Se for deg en stabel med papir. Push blir da at du legger ett nytt papir
på toppen av stabelen, og pop blir at du tar ett papir av stabelen.

##### The Segments of Program Loaded in Memory

![image](/home/erikhje/office/kurs/opsys/02-hw-review/tex/../../resources/MOS_3e/01-20.pdf)

Minneområdet er delt opp i ulike deler som har hver sin hensikt. Den
vanligste inndelingen av minneområdet består av følgende 3 deler:

  - **Stack**: Stacken inneholder diverse lokale data som er viktige for
    kjøringen til ett program. Den inneholder lokale variabler for
    funksjoner, returadresser for å finne tilbake etter funksjonskall og
    lignende. Stacken er organisert som en stack som ligger opp-ned, og
    vokser dermed nedover i minnet.

  - **Data**: Dataområdet inneholder globale variabler. Altså variabler
    som alle funksjoner i ett program skal ha behov for å få tak i.
    Dataområdet inneholder også minneområder som programmet ber om å få
    allokert. Dataområdet er gjerne organisert som en heap og vokser
    oppover i minnet etterhvert som det blir større.

  - **Text**: Text inneholder koden som programmet består av. Det er i
    fra denne delen at CPU henter instruksjoner som skal kjøres.

### Useful videos

Det er noen veldig nyttige videoer på youtube som oppsummerer hvordan en
CPU kjører, og hva programvare består av:

Den første videoen er [How a CPU
Works](http://www.youtube.com/watch?v=cNN_tTXABUA) som forklarer steg
for steg hvordan en CPU er bygget opp, hvordan den er koblet til minnet,
og hvordan den kjører programkode. Den tar ca 20 minutter.

Neste video fokuserer mer på kodeflyt og assembly. Denne videoen [How a
CPU works and Introduction to Assembler - bin 0x04
(LiveOverflow)](https://www.youtube.com/watch?v=6jSKldt7Eqs) varer ca 11
minutter.

## Software

### Compiling

##### From C Code to Executable

![image](/home/erikhje/office/kurs/opsys/02-hw-review/tex/../../resources/MOS_3e/01-30.pdf)

Figuren viser hvordan et C program kompileres i flere trinn før det blir
en ferdig eksekverbar fil. Hvis vi skal ha ut assemblykode så henter vi
det ut etter kompileringen istedet for en objektfil (.o-fil).
Objektfilene inneholder maskinkode.

Generelt kan vi betrakte abstraksjonsnivåene i en datamaskin ut fra
følgende figur:

``` 
  høynivå
  A
  |                                               A
  | 4GL: modellering med kodegenerering           |
  |                                            Laget mtp
  | Høynivå progspråk                          mennesker
  | (C,C++,Java,...)    (portabel)    
  |
  | - - (bytecode?) - - - - - - - - - - - - - - - - - - -  
  |
  | Assembly         (ikke portabel)           Laget mtp   
  |                                            maskiner
  | Maskinkode                                    |
  |                                               V
  | (Mikroarkitektur, Mikrooperasjoner)
  |
  | (Digital logikk) 
  V                                               
  lavnivå
```

DEMO1 - Se assemblykode og maskinkode laget av gcc fra C:

``` 
  cat asm-0.c
  gcc -S asm-0.c       # lag assembly kode
  gcc asm-0.c          # lag maskinkode
  cat asm-0.s          # se linjen "ret"
  hexdump -C asm-0     # se ferdiglinket maskinkode som hex
```

DEMO2 - Bruke en disassembler for å lese maskinkode

``` 
  # mapping finnes i http://ref.x86asm.net/coder64-abc.html
  # eller se via innholdsfortegnelsen (ark nr 1000) i
  # http://download.intel.com/products/processor/manual/325383.pdf
  gdb asm-0            # åpne maskinkoden i GNU Debugger
   disassemble /r main  # disassemble og vis tilhørende maskinkode
   set disassembly-flavor intel # vis i Intel syntax istedet
   disassemble /r main
  # alternativt:
  objdump -d asm-0 | less
  # med Intel syntax:
  objdump -d --disassembler-options=intel asm-0 | less
```

Det er lettest å disassemble opcodes som ikke har operander, slik som
`ret`. La oss se hvordan `retq` mappes til C3 i URLn over, dvs søk på C3
og etterhvert finner du en mapping til `retn` som er den samme, dvs en
return av type near (near = innen samme minnesegment, far = return til
et annet minnesegment).

Benytt anledningen til å se litt om diverse gode verktøy som finnes for
å inspisere program / reverse engineering: [Simple Tools and Techniques
for Reversing a binary - bin 0x06
(LiveOverflow)](https://www.youtube.com/watch?v=3NTXFUxcKPc)

### gcc

Man kan benytte gcc til å kompilere C kode til assembly og maskinkode,
samt til å konvertere til/fra assembly/maskinkode.

##### GNU Compiler Collection: gcc

``` 
    gcc -S tmp.c      # from C to assembly
    gedit tmp.s       # edit it
    gcc -o tmp tmp.s  # from assembly to machine code
    ./tmp             # run machine code
    
    # sometimes nice to remove lines with .cfi
    gcc -S -o - tmp.c | grep -v .cfi > tmp.s
    # or avoid .cfi-lines in the first place
    gcc -fno-asynchronous-unwind-tables -S tmp.c
```

### 32 vs 64 bit

##### 32 vs 64 bit code

Same C-code, different assembly and machine code

``` 
    gcc -S asm-0.c      # 64-bit since my OS is 64-bit
    grep push asm-0.s   # print lines containing "push"
    gcc -S -m32 asm-0.c # 32-bit code
    grep push asm-0.s   # print lines containing "push"
```

Difference in instructions and registers

### Syntax

##### Assembly Language

``` 
          .file   "asm-0.c"    # DIRECTIVES
          .text
          .globl main
```

``` 
          main:                # LABEL
```

``` 
          push   rbp           # INSTRUCTIONS
          mov    rsp, rbp 
          mov    0, eax
          ret
```

Direktiver starter med et punkt (`.`) og merker (labels) avsluttes med
kolon (`:`).

Assemblykode oversettes til maskinkode med et program som kalles en
assembler (f.eks. GAS, NASM, el.l. program). Assemblykode består av
instruksjoner (som oversettes som oftest en-til-en til maskinkode) og
direktiver som er informasjon til assembleren. I tillegg benyttes også
labels (merker) for å henvis til bestemte deler av koden.

Linje for linje betyr denne assemblykoden:

  - .file "asm-0.c"  
    metainformasjon som sier hvilken kildekode som er opphavet til denne
    koden

  - .text  
    sier at her starter kodedelen (dvs selve instruksjonene i et program
    kalles ofte text, i motsetning til andre deler som f.eks. kan være
    bare data)

  - .globl main  
    sier at main skal være global i scope (synlig for annen kode som
    ikke er i akkurat denne filen, dvs kode som linkes inn, delte
    biblioteker o.l.)

  - main:  
    et label (merke), det er vanlig å kalle hovedfunksjonen i et program
    for main

  - push rbp  
    legger base pointer på stacken

  - mov rsp, rbp  
    setter base pointer (registeret rbp) lik stack pointer (registeret
    rsp)

  - mov 0, eax  
    setter et “general purpose register” eax til å være 0, det er vanlig
    å legge returverdien til et program i eax registeret

  - ret  
    legger den gamle instruksjonspekern som befinner seg på stacken
    tilbale i rip registeret hvis denne finnes slik at den funksjonen
    som kallet meg kan fortsette der den slapp

##### GNU/GAS/AT\&T Syntax

<span><http://en.wikibooks.org/wiki/X86_Assembly/GAS_Syntax></span>

  - instruction  
    mnemonic

  - mnemonic suffix  
    b (byte), w (word), l (long), q (quadword), ...

  - operand prefix  
    % (registers), $ (constants)

  - address op.  
    `movl -4(%ebp, %edx, 4), %eax`  
    “load \((\mathrm{ebp}-4+(\mathrm{edx}\times4))\) into eax”  

<span><http://en.wikibooks.org/wiki/X86_Assembly/GAS_Syntax#Address_operand_syntax></span>

En mnemonic tilsvarer en maskinkode opcode. Dvs det kalles mnemonic
fordi det er bare et kort navn som brukes istedet for å måtte huske den
egentlig numeriske maskinkode opcode’n.

##### Overview of X86 assembly

<span><http://en.wikipedia.org/wiki/X86_instruction_listings></span>

### Eksempler

:

`asm-0.c` Eksempelet over

`asm-1.c` En ekstra kodelinje fordi 0 skrives til stacken, og deretter
kopieres fra stacken til registeret (MERK: plutselig to skrivinger til
minne som er mye (dvs minst 10x) tregere enn å skrive til et register)

MERK: *parenteser betyr altså at vi prater om en adresse i minne.*

``` 
  diff asm-0.s diff asm-1.s
```

`asm-2.c` Lokale og globale variable, merk at det har dukket opp en
section `.data` eller `.bss`. Data er området der globale variabler som
har en initiell verdi er lagret. Disse verdiene må være lagret i
programfilen. BSS er området der globale variabler uten noen initiell
verdi ligger lagret. Det er nok å bare ha størrelsen på disse variablene
i programfilen, siden de ikke skal ha noen verdi.

Merk at den globale variabelen `j` brukes til å adressere med
utgangspunkt i instruksjonspekern `rip`. Dette er et tilfelle av
PC-relative adressering
(<http://software.intel.com/en-us/articles/introduction-to-x64-assembly>):

> RIP-relative addressing: this is new for x64 and allows accessing data
> tables and such in the code relative to the current instruction
> pointer, making position independent code easier to implement.

Se også <http://en.wikipedia.org/wiki/Addressing_mode#PC-relative_2>

Her ser vi også `add` instruksjonen.

Det området på stacken som en funksjon benytter kalles en *stack frame*,
og består av det området som pekes ut mellom base pointer og stack
pointer. Når en funksjon kalles lagres base pointer unna på stacken og
base pointer settes lik stack pointer, dermed har vi startet en ny stack
frame, og vi kan gå tilbake til forrige stack frame når funksjonen er
ferdig.

Stack frame forklart: [First Stack Buffer Overflow to modify Variable -
bin 0x0C (LiveOverflow)](https://www.youtube.com/watch?v=T03idxny9jE)

DEMO: `asm-3-stack.c`

##### Why Learn Assembly

From Carter (2006) *PC Assembly Language*, page 18:

  - Assembly code *can be faster* and smaller than compiler generated
    code

  - Assembly allows access to *direct hardware features*

  - Deeper understanding of *how computers work*

  - Understand better how *compilers* and high level languages like C
    work

Eksempelvis vil spillprogrammerere ønske å utnytte hardware egenskaper
maksimalt, mens sikkerhetsdudes (and dudettes) ønsker å benytte
assembly-nivå for effektiv implementering av lavnivå kryptooperasjoner
hvor det ofte er snakk om å flytte bit i et register som en del av en
kryptoalgoritme.

La oss sjekke at vi har noenlunde kontroll på assemblykode,
programutførelse og stack (basert på X86 og C++ kode) og en del
tilhørende kommandolinjeverktøy: <http://vimeo.com/21720824>

En siste god link å lese for å få enda en gjennomgang av grunnleggende
assembly er [Understanding C by learning
assembly](https://www.recurse.com/blog/7-understanding-c-by-learning-assembly)

## CPU - Advanced

##### Important Concepts

  - *Clock* speed/rate

  - *Pipeline*

  - Machine instructions into *Micro operations* (\(\mu\mathrm{ops}\))

  - *Out-of-Order* execution

Selve utførelsen av alt som skjer i en datamaskin baseres på en
klokketakt, dvs hvis vi har en klokkehastighet på 1GHz så betyr det at
det kommer en milliard pulser (generert av en oscillator) hvert sekund,
og hver slik pulse skyver utførelsen av instruksjonene et hakk videre.
Litt forenklet kan vi tenke på at CPUn da klarer å utføre en instruksjon
for hver puls (klokkeperiode), som vil si at den bruker et nanosekund
(et milliard’te-dels sekund) på å utføre en instruksjon (dette er ikke
helt presist siden en superskalar CPU kan utføre flere
mikroinstruksjoner hver klokkeperiode).

### Structure

##### Pipelined and Superscalar CPU

![image](/home/erikhje/office/kurs/opsys/02-hw-review/tex/../../resources/MOS_3e/01-07.pdf)

Utførelsen av en instruksjon skjer i flere steg, som vi kaller for
mikrooperasjoner. For eksempel vil det å legge sammen to tall som ligger
i hvert sitt register kunne bli brutt opp i minst følgende steg:

1.  Hente instruksjonen fra minne

2.  Dekode den, for å se hva som trengs å bli gjort

3.  Hente inn registrene som skal legges sammen

4.  Legge sammen tallene

5.  Lagre resultatet i ett register

For at CPU skal jobbe mest mulig effektivt lager en derfor egne klart
adskilte enheter der hver av disse mikrooperasjonene utføres, og så lar
vi instruksjonene passere disse enhetene en etter som, som på ett
samlebånd. En slik organisering av CPU kalles for en "pipeline", og er
illustrert i del a av figuren over.

For å gjøre CPU enda mer effektiv, så kan en duplisere deler av
pipelinen slik at man behandler mer enn en instruksjon om gangen. Man
kan for eksempel lage 2 sett med de enhetene som henter instruksjoner
fra RAM og dekoder disse. Man kan og lage flere enheter som kan kjøre
instruksjonene (eksekveringsenheter), slik at det kan utføres flere
instruksjoner i paralell. Da får man en super-skalar arkitektur, som er
illustrert i del b av figuren over.

Moderne prosessorer som er i "vanlige" datamaskiner er normalt sett en
super-skalar CPU, og eksekveringsenhetene i disse CPU-ene er ofte veldig
spesialisert (Noen kan jobbe med heltall, andre flyttall, og andre er
kanskje mer generelle), og så vil kontrollogikken til CPU passe på at
rett instruksjon går til rett eksekveringsenhet. En super-skalar CPU kan
og kjøre instruksjonene i en annen rekkefølge (*out-of-order
eksekvering*) enn programmereren skrev de dersom kontrollogikken
oppdager at en eksekveringsenhet er ledig, og det er en instruksjon som
skal kjøres litt senere som kan kjøres der. Kontrollogikken jobber med å
holde mest mulig av enhetene i arkitekturen opptatt til enhver tid, slik
at flest mulig instruksjoner blir utført fortest mulig.

Moderne prosessorer bedriver også *spekulativ eksekvering*, dvs den
forsøker å gjette på hva som blir utfallet av branch-instruksjoner
(f.eks. jump-instruksjoner) og så reverserer hvis den tok feil. Både
out-of-order eksekvering og spekulativ eksekvering gjøres for å få
forbedret ytelse. Spekulativ eksekvering fikk mye oppmerksomhet i 2018
pga sårbarhetene [Spectre og
Meltdown](https://googleprojectzero.blogspot.no/2018/01/reading-privileged-memory-with-side.html).

### HyperThreading/SMT

##### Hyperthreading/SMT

![image](/home/erikhje/office/kurs/opsys/02-hw-review/tex/../../resources/Illustrations/Hyper-threaded_CPU.png)

(De fire fargene på boksene betyr instruksjoner fra fire forskjellige
program)

En videre utvidelse av super-skalar arkitekturen er Simultaneous
multithreading (SMT), eller Hyper-Threading (HT) som Intel kaller det.
For å øke mulighetene til å holde alle enhetene av CPU-en opptatt til
enhver tid er det mulig å utvide CPU til å holde to prosesser samtidig
(ved å ha dobbelt sett av alle registre programmer bruker (inkludert SP,
BP, IP etc.)). Da kan CPU til en hver tid plukke instruksjoner fra disse
to prosessene, alt etter hvilke eksekveringsenheter som er ledige til
enhver tid. En CPU med SMT/HT vil se ut som flere prosessorer for
operativsystemet (2 stk ved Intel’s HT). Konseptet hyperthreading kalles
også for "Virtuelle kjerner" i noen medier. Navnet der viser til at det
ser ut som om datamaskinen din har mange kjerner, men i realiteten er
det færre fysiske kjerner tilgjengelig.

Eksempel:  
Intel Core i7 2640M (<http://ark.intel.com/products/53464>) er en
tokjerneprosessor med hyperthreading. Denne har derfor fire tråder, noe
som gjør at OS tror at den har fire CPU-er:

``` 
  $ cat /proc/cpuinfo 
  processor       : 0
  vendor_id       : GenuineIntel
  model name      : Intel(R) Core(TM) i7-2640M CPU @ 2.80GHz
     ...
  processor       : 1
  vendor_id       : GenuineIntel
  model name      : Intel(R) Core(TM) i7-2640M CPU @ 2.80GHz
     ...
  processor       : 2
  vendor_id       : GenuineIntel
  model name      : Intel(R) Core(TM) i7-2640M CPU @ 2.80GHz
     ...
  processor       : 3
  vendor_id       : GenuineIntel
  model name      : Intel(R) Core(TM) i7-2640M CPU @ 2.80GHz
     ...
```

### Multicore

##### Multicore Chips and Cache Structure

![image](/home/erikhje/office/kurs/opsys/02-hw-review/tex/../../resources/MOS_3e/01-08.pdf)

Det har og blitt vanlig med flere fullverdige prosessorkjerner på en
brikke, der hele arkitekturen er duplisert. Da blir det litt spørsmål om
hvor f.eks de ulike lagene med cache skal plasseres. Man har normalt
sett minst ett lag (L1) med cache knytta til hver prosessorkjerne, og på
intel cpu-er plasseres neste lag (L2) felles for alle kjerner (som fører
til en mer komplisert cache-kontroller) mens AMD har en L2 cache per
kjerne (Som gir utfordringer med å holde cachene konsistente).

### Protection

##### PSWs Mode Bits: User/Kernel Mode

![image](/home/erikhje/office/kurs/opsys/02-hw-review/tex/../img/modes-switches.pdf)

PSW registeret (X86: RFLAG/EFLAG) har to bits (nr 12 og 13) som styrer
hvilken mode/tilstand prosessoren er i:  
<http://en.wikipedia.org/wiki/FLAGS_register>  
Dette er hvordan prosessoren vet om den er i user mode eller kernel
mode, og om den har lov utføre de instruksjonene den skal utføre.
Programmene som kjører skifter ofte mellom og kjøre sin egen kode, og å
låne tjenester fra operativsystemet (f.eks. hver gang det skal leses fra
en disk eller skrives til skjerm), og det er dette skiftet vi kaller
mode switch. Operativsystemet har også ansvaret for å skifte mellom alle
applikasjoner som ønsker å bruke prosessoren, og dette skiftet er det vi
kaller context eller process switch. Mode switch er raskt,
context/process switch er tregt. Merk i denne figuren at det er
forskjellige applikasjoner (APP1, APP2, ..., APPN) men alle ser det
samme operativsystemet (OS).

*En mode switch tar typisk rundt hundre nanosekunder (ns), mens en
context switch tar noen mikrosekunder (\(\mu\)s).*

Hvor ofte gjøres en contekst switch?

``` 
  c1=$(grep ctxt /proc/stat | awk '{print $2}')
  sleep 1
  c2=$(grep ctxt /proc/stat | awk '{print $2}')
  echo "Antall context switch'er siste sekund var $(($c2-$c1))"
```

Hvor mye tid bruker en prosess i user og kernel mode? Det avhenger i
stor grad av hvor mye I/O som må gjøres, demo windows `perfmon`
(Performance monitor - Add - ProcessorMedPilOpp - Add privileged og user
time, flytt musepekern/flytt hele vinduet og se hvordan privileged
grafen går opp for å håndtere interrupts siden det grafiske systemet til
Windows OSet kjører i kernel mode).

## Cache

For en CPU er RAM utrolig tregt. For å kunne unngå en del av ventingen
man får når en prøver å lese/skrive til RAM vil maskinvaren prøve å gå
til RAM så sjelden som mulig. Det handler f.eks om å lese større mengder
data fra RAM når en først leser fra ram for å så mellomlagre disse
dataene ett annet sted. Dette andre stedet er det man kaller for en
"cache".

NB\! Cache er ett veldig generisk begrep som benyttes mange steder i
moderne datamaskiner. Cache som blir beskrevet her i kapittel
[2.5](#sec:hw-review:cache) er en variant som er bygget fysisk i
maskinvare veldig nærme prosessorkjernen(e).

##### Memory Hierarchy

![image](/home/erikhje/office/kurs/opsys/02-hw-review/tex/../../resources/MOS_4e/1-9.pdf)

NB\!: Tallene i figuren er veldig cirkatall.

Andre eksempler på noen av disse tallene er [What Your Computer Does
While You
Wait](http://duartes.org/gustavo/blog/post/what-your-computer-does-while-you-wait)
og [Advanced Computer Concepts for the (Not So) Common Chef: Memory
Hierarchy: Of Registers, Cache and
Memory](https://software.intel.com/en-us/blogs/2015/06/11/advanced-computer-concepts-for-the-not-so-common-chef-memory-hierarchy-of-registers)

Flaskehalsen som oppstår mellom hastigheten på CPU og tiden det tar å
gjøre minneaksess kalles ofte *von Neumann flaskehalsen*, og i praksis
løses den med bl.a. cache-mekanismen.

##### Why Cache Works

  - Locality of reference
    
      - *spatial locality*
    
      - *temporal locality*

Cache gjør at programmer kjører raskere fordi instruksjoner gjenbrukes
ofte (samlokalisert i tid) mens data ofte aksesseres blokkvis
(samlokalisert i rom, har du lest en byte skal du sikkert snart ha
byte’n ved siden av den også).

Caching står sentral i mange deler av informatikken og dukker opp flere
steder i OS-verdenen. Sentrale problemstillingene knyttet til cache er
nevnt på side 25 i boka:

1.  Når skal cache benyttes

2.  Hvilken cache line skal overskrives

3.  Hva gjøres med modifiserte (dirty) cache lines

Ved caching av minne behandles minne i form av *cache lines*, som oftest
består av 64 bytes (det kan være mellom 8 og 512 bytes avhengig av
arkitekturen). Det er altså hele cache lines som caches, ikke
enkeltbytes.

Et stort spørsmål knyttet til cache er hvor plasseres en ny cache line,
spesielt “når cachen er full”. Vi skal se mer på dette når vi ser på
minnehåndtering spesielt (page replacement algoritmer kalles det da),
men slik det gjerne gjøres for CPU cache er helt enkelt, Tanenbaum side
26:

> For example, with 4096 cache lines of 64 bytes and 32 bits addresses,
> bits 6 through 17 might be used to specify the cache line, with bits 0
> to 5 the byte within the cache line.

``` 
        +-------+
  4096  |       |
  lines |       |
  (2^12)|       |
        |       |
        |       |
        +-------+
      64bytes (2^6)
```

  - Hvor mye adresseres med 32 bit? (\(2^{32}\)=4GB)

  - Hvor mye adresseres med 18 bit? (\(2^{18}\)=256KB)

Altså, hver adresse i minne som befinner seg med 256KB mellomrom vil
plasseres samme sted i cache (overskrive hverandre), en dum men enkel
“cache line replacement algoritme”. Dette er nok et eksempel på en
hash funksjon som vi sikkerhet husker fra databaser og fra algoritmer,
se  
<http://en.wikipedia.org/wiki/Hash_function#Caches>

Denne type cache kalles en *Direct mapped cache* som er en av tre typer
cache.

### Set Associative Cache

##### Set Associative Cache

  - *Direct mapped* cache (only one “way”)

  - (N-way) *set-associative* cache

  - *Fully associative* cache (only one “set”)

Direct mapped og Fully associative er bare spesialtilfeller av
set-associative caching.

Les all teksten mellom de to figurene på [Cache: A Place for Concealment
and Safekeeping](http://duartes.org/gustavo/blog/post/intel-cpu-caches)
(dvs teksten som starter med “The unit of data...”).

Det er tre parametere som gjelder:

  - L  
    antall bytes pr cache line

  - K  
    hvor mange-veis assosiativ (antall “ways”)

  - N  
    antall set

F.eks. har Intel Nehalem en 32KB L1 instruksjonscache som har L=64, K=4
og N=128 og en 32KB L1 datacache som har L=64, K=8 og N=64.

(\(64\times4\times128B=2^{6}2^{2}2^{7}B=2^{15}=32KB\))

Merk: for set associative cache og fully associative cache må vi gjøre
en litt mer avansert beslutning ift hvilken cache line som skal
overskrives og da benyttes som regel algoritmen *Least Recently Used
(LRU)*. Da skriver man over den cache line som er minst nylig brukt.

### Write Policy

##### Write Policy

  - Write-through  
    Write to cache line and immediately to memory

  - Write-back  
    Write to cache line and mark cache line as *dirty*

Ved write-back skrives dataene til minne først når cache line’n skal
overskrives av en annen, eller i andre tilfeller som f.eks. en context
switch.

*Et viktig poeng er at Write-back cacher er spesielt utfordrende for når
det er flere prosessorkjerner tilstede: hva hvis flere prosessorkjerner
har cachet de samme dataene? Hvordan vet en prosessorkjerne at ikke en
annen prosessorkjerne har skrevet til de dataene den har cachet?* Dette
løses selvfølgelig med en *cache coherence protokoll*, f.eks. MESI.

#### Write-through

![image](/home/erikhje/office/kurs/opsys/02-hw-review/tex/../img/Writethrough.png)

#### Write-back

![image](/home/erikhje/office/kurs/opsys/02-hw-review/tex/../img/Writeback.png)

Begge figurene er hentet fra
<http://en.wikipedia.org/wiki/Cache_(computing)>

Ved Write-back caching må vi altså sjekke om cache-blokken vi ønsker
cache i er dirty, dvs inneholder data som ikke er skrevet til neste nivå
datalagring enda. Dette gjelder både for lese og skrive forespørsler.
Write through caching er det enkleste og tryggeste (siden caching bare
vil inneholde kopi av data som finnes et annet sted), men skal systemet
gi god ytelse for skriveforespørsler så må man benytte write-back
caching.

*I operativsystemer er det et poeng at når vi bytter om fra å jobbe på
et program til et annet (context switch) så er cachen full av data fra
det første og det tar tid å bytte det ut\! Derav sier vi at context
switch er ganske kostbart.*

## I/O

##### Classic Disk Drive Structure

![image](/home/erikhje/office/kurs/opsys/02-hw-review/tex/../../resources/MOS_3e/01-10.pdf)

For å adressere på en klassisk disk må man vite sylinder, sektor og
plate. En sektor har typisk 512 bytes, og det er ofte flere sektorer i
de ytre sporene enn i de indre. Dette gjør oppgaven med å avgjøre hvor
vi fysisk på disken finner dataene vi leter etter kompleks.

OSet har en device driver som prater med disk kontrolleren (ved å skrive
til dens registre) og disk kontrollerer tar seg av alle de detaljerte
lavnivå diskoperasjonene (avgjøre hvor på hvilken plate dataene er,
hente ut bit og bytes fra disk, feilsjekk, osv).

*Husk at konseptet med et minnehierarki alltid vil være varig
kunnskap\!*

I nyere tid har også SSD-er (Solid State Drive’s) begynt å dukke opp.
Disse plasserer seg pent inn i minnehierakiet mellom harddisker og RAM.
Disse har betydelig mye bedre ytelse (random aksess tid for disk endres
dramatisk ved SSD), men til en høyere pris.

##### I/O and Interrupts

![image](/home/erikhje/office/kurs/opsys/02-hw-review/tex/../../resources/MOS_3e/01-11.pdf)

I/O kan gjøres på tre måter:

1.  Busy waiting/Polling

2.  Interrupt

3.  DMA (Direct Memory Access)

## Interrupts

Interrupt håndteres via en egen interrupt kontroller, og som regel
nederst i fysisk minne befinner det seg en *interrupt vektor tabell*
hvor hver *interrupt vektor* er en adresse til koden for den tilhørende
interrupt handleren/rutinen i minne.

Interrupt er brukt slik at maskinvaren kan signalisere til
CPU/programvaren om at noe har skjedd, og at det er behov for å håndtere
dette.

Hvor ofte kommer et interrupt?

``` 
  i1=$(grep intr /proc/stat | awk '{print $2}')
  sleep 1
  i2=$(grep intr /proc/stat | awk '{print $2}')
  echo "Antall interrupts siste sekund var $(($i2-$i1))"
```

##### Interrupts

  - Hardware interrupt (asynchronous)

  - Exceptions (synchronous)

  - Traps/Software interrupt (synchronous)

<!-- end list -->

  - Hardware interrupt  
    er som regel knyttet til I/O slik via akkurat så.

  - Exceptions  
    er internt generert hardware interrupt typisk generert ved deling på
    null eller forsøkt tilgang til et minneområde som ikke finnes el.l.

  - Traps/Software interrupt  
    brukes ved systemkall.

Trap brukes også av og til som betegnelse på det å overføre kontroll fra
user mode til kernel mode. Vær klar over at disse begrepene brukes ofte
med litt forskjellig betydning avhengig av hvem du prater med (slik det
ofte er. f.eks. sier noen at trap er en type exception). Hardware
interrupt er asynkrone fordi de kan oppstå når som helst, mens
exceptions/traps/software interrupts er synkrone fordi de kan bare
oppstå som konsekvens av en instruksjon.

Oppsummerende video med I/O kontroller m/register, memory-mapped vs
porter, polling vs interrupt, interrupt vektor tabell, exceptions.  
<http://www.youtube.com/watch?v=lDLmuIXzk0A>

### DMA

##### Direct Memory Access (DMA)

![image](/home/erikhje/office/kurs/opsys/02-hw-review/tex/../../resources/MOS_3e/05-04.pdf)

DMA er en teknikk som gjør I/O langt mer effektivt. DMA styres av en
egen kontroller i hardware som gjør I/O på vegne av CPUn. Istedet for at
CPU henter byte for byte (eller word for word) fra en device, så ber den
DMA- kontrolleren gjøre det istedet og si ifra (dvs. gi et interrupt)
når den er ferdig.

På figuren skjer trinn 1 en gang, mens trinn 2-4 gjentas mange ganger
avhengig av hvor mye data som skal overføres.

## System

### Architecture

##### Structure of a X86 System

![image](/home/erikhje/office/kurs/opsys/02-hw-review/tex/../../resources/MOS_4e/1-12.pdf)

Det er mange busser i et typisk moderne datasystem. Den opprinnelige PCI
bussen kjørte på 66MHz og hadde en bredde på 64 bit som betydde at den
kunne frakte 528 MB/sek.

Se også Core og I7 på side 9 og 15 i Mikroprosessorkompendiet, samt
Tanenbaum side 32-34.

### Booting

##### Booting a PC

1.  BIOS (Basic Input Output System) runs and scans all devices

2.  BIOS determines boot device from list i CMOS memory

3.  First sector (MBR) from boot device is read and executed

4.  Active partition read from partition table at end of MBR

5.  Second boot loader read and executed from active partition which
    loads OS

6.  OS queries BIOS for devices and loads device drivers

7.  OS initializes tables, background process and starts login/GUI

## Theory questions

1.  Hva er et “Directive” i assemblykode?

2.  Hva forbinder du med begrepene *superskalar* og *pipelining* i en
    prosessor?

3.  Hva menes med *kernel mode* og *user mode*?

4.  Hva menes med *hyperthreading/multithreading* som ble innført med
    Pentium 4? Hva var hensikten med dette?

5.  Hva er ulempen ved en direkte mappet (“direct mapped”) cache?

6.  Anta en CPU som skal sjekke om dataene på en gitt adresse i minne
    finnes i en sett-assosiativ cache. Hvordan finner CPU’en frem til
    riktig sett og riktig vei (“way”)?

7.  Nevn tre måter interrupt kan oppstå på i en datamaskin, og beskriv
    de kort.

8.  Hva er oppgaven til en C kompilator? Hva er forskellen mellom
    C-kode, assemblykode og maskinkode?

9.  Hvor store cache lines har en 2-veis sett-assosiativ cache som er
    32KB stor og har 128 sett?

## Lab exercises

1)  Forklar hva hver linje i følgende assemblykode gjør:
    
        01         .text   
        02 .globl main
        03 main:   
        04         pushq   %rbp
        05         movq    %rsp, %rbp
        06         movl    $0, -4(%rbp)
        07         cmpl    $0, -4(%rbp)
        08         jne     .L2 
        09         addl    $1, -4(%rbp)
        10 .L2:    
        11         movl    $0, %eax
        12         popq    %rbp
        13         ret
    
    Denne assemblykoden ble generert av et C-program på ca fem linjer.
    Hvordan så programkoden til dette C-programmet ut? (Hint: Løs denne
    ved å prøve deg frem med å lage enkel C-kode som du kompilerer til
    assemblykode og sammenlikner med koden over, sjekk gjerne ut
    [Compiler Explorer](https://godbolt.org) til dette.)

2)  Forklar hva hver linje i følgende assemblykode gjør:
    
        01         .text   
        02 .globl main
        03 main:   
        04         pushq   %rbp
        05         movq    %rsp, %rbp
        06         movl    $0, -4(%rbp)
        07         jmp     .L2 
        08 .L3:    
        09         addl    $1, -4(%rbp)
        10         addl    $1, -4(%rbp)
        11 .L2:    
        12         cmpl    $9, -4(%rbp)
        13         jle     .L3 
        14         movl    $0, %eax
        15         popq    %rbp
        16         ret
    
    Denne assemblykoden ble generert av et C-program på ca fem linjer.
    Hvordan så programkoden til dette C-programmet ut?

# Introduksjon til operativsystemer

## Outcome

##### Learning Outcome

  - Layering/Abstraction

  - Conceptual framework

## OS Zoo

##### Types of Operating Systems

  - Mainframes

  - *Server/Multiprocessor/Personal/Handheld*

  - Embedded

  - Sensor node

  - Real-time

  - Smart card

Mainframes er fortsatt i stor grad levende og vil “alltid” være det,
typisk IBM System Z, se <http://en.wikipedia.org/wiki/System_z>.
Mainframes kan også kjøre Linux (og gjør ofte det).

Se <span>
<http://debatt.digi.no/58823/ibm-leverer-nordens-storste-linux-maskin></span>

og mer nylig <http://hardware.slashdot.org/story/15/01/15/0218238/>

For Server/Multiprocessor/Personal/Handheld type operativsystemer er det
gjerne en eller annen variant av Linux eller Windows som benyttes.

Embedded systems er f.eks. en MP3-spiller eller en mikrobølgeovn, OSer
som benyttes er f.eks. QNX eller VxWorks, ingen andre applikasjoner kan
kjøres enn de som er laget kun for den enheten det kjører på.

Sensor nodes er små enheter som typisk brukes til overvåkning av
luftkvalitet eller detektere røyk eller bevegelses/trykkendringer,
typisk OS er TinyOS.

Real-time systems er systemer hvor tiden er svært viktig. *Soft
real-time* er f.eks. avspilling av lyd og video hvor forsinkelse til en
hvis grad kan være akseptabelt, mens *hard real-time* er f.eks.
styringssystemet til en robot som skjærer ut noe som passerer den med
sag hvor forsinkelse ikke er akseptabelt. Et typisk eksempel er e-Cos,
men merk at OSer som benyttes i embedded systems og sensor nodes også
gjerne kan være real-time systems.

Smart kort er minidatamaskiner på kredittkort og OS som gjerne benyttes
er MULTOS eller JavaCard.

Man prater ofte om preemptive og non-preemptive operativsystemer, vi
kommer tilbake til dette når vi skal se på scheduling. Poenget er at
real-time systemer må hvertfall være preemptive. Dvs preemptive betyr “å
ta bort”, dvs å kunne ta fra en prosess CPU’n uten at den egentlig selv
vil det (som regel basert på klokkeinterrupt).

## Concepts

### Process

##### A Process Tree

![image](/home/erikhje/office/kurs/opsys/03-os-intro/tex/../../resources/MOS_3e/01-13.pdf)

Konsepter/Abstraksjoner:

  - Prosess

  - Adresserom

  - Filsystem

  - Pipe (rør)

Husk:

``` 
 Prosess       Adresserom       Fil
 ------------------------------------
 CPU           RAM              DISK
```

*En prosess er et program under utførelse*. Et OS må håndtere flere
prosesser samtidig, men er det bare en CPU kan bare en prosess kjøre om
gangen.

En prosess består av innholdet i mange registre, et adresserom i minne,
samt en del annen informasjon (liste over åpne filer, liste over
relaterte prosesser, ventende alarmer, o.l.). *En prosess er en
container for et kjørende program (kan også kalles konteksten som et
program utføres innen).*

Hver prosess har en prosess control block (PCB) som er en entry i OSet’s
prosesstabell. En prosess som midlertidig stoppes lagrer unna all
informasjon i PCBn og tar vare på adresserommet sitt.

I Unix/Linux verdenen lages alltid prosesser som barn av andre prosesser
som i figuren. I Windows lages også prosesser på en måte slik, men de
blir mer frittstående prosesser, dvs prosesshierarkiet i Unix/Linux
dyrkes ikke i like stor grad i Windows (selv om det egentlig finnes der
også).

Prosess er et konsept/en abstraksjon, og en annen relatert abstraksjon
er *adresserom*. Adresserom er et konsept som må til for å la prosesser
forholde seg til et virtuelt adresserom (virtual memory) som ikke er det
samme som fysisk minne. F.eks. vil en 32-bit PC ha mulighet til å
adressere 4GB (32-bits arkitektur gir \(2^{32}\)B=4GB) mens fysisk minne
kanskje bare er 1GB (som var typisk for en netbook). 64-bit systemer har
en teoretisk mulighet til å adressere \(2^(64)\)B=16EB, selv om
minnemengden tilgjengelig er langt lavere enn dette.

For å se den nåværende prosesstabellen på en datamaskin kan for eksempel
ett av følgende verktøy brukes:

  - `top htop ps pstree` i Unix/Linux.

  - `taskmgr` eller helst `procexp` i Windows.

### File

##### A File System

![image](/home/erikhje/office/kurs/opsys/03-os-intro/tex/../../resources/MOS_3e/01-14.pdf)

*Filsystem* er også et konsept/abstraksjon fordi det egentlig ikke
finnes rent fysisk slik vi ser det for oss i figuren her.

I filsystemer opererer vi med konseptene *root directory* og *current
working directory*.

DEMO:

`pwd; df -h; du -sh; mount; cat /etc/fstab`

##### Mounting a File System

![image](/home/erikhje/office/kurs/opsys/03-os-intro/tex/../../resources/MOS_3e/01-15.pdf)

I Windows har vi flere filsystemer som vises adskilt knyttet til
stasjonsnavn (A:, B:, C:, ..., Z:) *(a)* mens i Unix/Linux knyttes alt
sammen til et filsystem *(b)* som vist i figuren.

I Unix/Linux har man *block special files* og *character special files*
som en del av filsystemet. Disse befinner seg i `/dev` directory, og
representerer I/O enheter slik at man kan benytte filoperasjoner på I/O
enheter. Hvis man akseresser data blokkvis (som for disker) så er det en
block device mens hvis man aksesserer en bytestream (characterstream) så
er det en character device (f.eks. lydkort, mus).

DEMO:

`ls -l /dev`

### Pipe

##### A Pipe

![image](/home/erikhje/office/kurs/opsys/03-os-intro/tex/../../resources/MOS_3e/01-16.pdf)

En *pipe* (rør) er en måte for to prosesser å kommunisere på, dvs sende
data til hverandre. En pipe er en slags pseudofil, dvs for en prosess er
å skrive til en pipe omtrent som å skrive til en fil.

DEMO:

`ls -l /dev | wc -l`

## System Calls

##### System Call: `read(fd,&buffer,nbytes)`

![image](/home/erikhje/office/kurs/opsys/03-os-intro/tex/../../resources/MOS_3e/01-17.pdf)

``` 
 Applikasjoner/biblioteker
'-------------------------  <--- SYSTEMKALL
 Operativsystemet
'-------------------------  <--- INSTRUKSJONER
 Datamaskinarkitekturen
```

Systemkall er selve grensesnittet mot operativsystemet. Som regel
benyttes ikke systemkallene helt direkte, men via et bibliotek (f.eks.
libc). På dagens linux kan du få en mer direkte oversikt over
systemkallene som tilbys i  
`/usr/src/linux/include/linux/syscalls.h` (eller ved `man syscalls`)

Oversikt over alle Linux X86 systemkallene finnes også på  
<http://syscalls.kernelgrok.com/> og X86-64 på  
<https://filippo.io/linux-syscall-table>

Et systemkall er altså en funksjon vi kaller når vi trenger
operativsystemet til å gjøre noe, f.eks. I/O som å lese noe fra disk.

*Hvert systemkall innebærer en overgang fra user mode til kernel mode
(og tilbake til user mode etterpå)*.

I figuren er det vist hvordan dette skjer (dvs hvordan er trinnene i den
ferdigkompilerte koden for et program som foretar et systemkall):

1.  `nbytes` pushes på stacken

2.  adressen (siden det står &) til `buffer` pushes på stacken

3.  `fd` pushes på stacken

4.  selve funksjonskallet `read`

5.  funksjonen plasserer systemkallnummer (eller noe annet som
    identifiserer systemkallet) et sted hvor OSet forventer det (et
    register)

6.  funksjonen utfører `trap` instruksjonen som forårsaker et skifte til
    kernel mode og et hopp til et bestemt sted i *kernel space*

7.  OSet ser hvilke systemkallnummer som skal utføres og bruker dette
    som en index inn i en tabell med adresser til de respektive
    systemkallhandlene (dvs selve systemkallkoden i kjernen)

8.  systemkall kjører

9.  systemkall returnerer tilslutt til user mode funksjonen som kalte
    den opprinnelig

10. funksjonen returnerer til hovedprogrammet

11. hovedprogrammet rydder opp stacken ved å oppdatere stack pekeren
    (SP)

![image](/home/erikhje/office/kurs/opsys/03-os-intro/tex/../../resources/MOS_3e/01-20.pdf)

DEMO:

`strace -c ls`

##### A Simple Example

    .data
    str:
            .ascii "hello world\n"
    .text
    .global _start
    _start:
        movq $1, %rax   # use the write syscall
        movq $1, %rdi   # write to stdout
        movq $str, %rsi # use string "Hello World"
        movq $12, %rdx  # write 12 characters
        syscall         # make syscall
        
        movq $60, %rax  # use the _exit syscall
        movq $0, %rdi   # error code 0
        syscall         # make syscall

Se fil `asm-syscall-2017.s` for litt mer kommentarer. Se også klassisk
systemkall med bruk av assembly instruksjonen `int 0x80` dvs vi
"genererer et interrupt nr 80" i filen `asm-syscall.s`. Idag brukes ikke
`int 0x80` instruksjonen siden `syscall` er kjappere.

Se gjennomgang av systemkallmekanismen ved å se de seks første minuttene
av [Syscalls, Kernel vs. User Mode and Linux Kernel Source Code - bin
0x09](https://www.youtube.com/watch?v=fLS99zJDHOc)

Sjekk hva som gjøres med libc wrapper og uten:

    gcc -o asm-syscall asm-syscall.s
    strace -c ./asm-syscall
    sed -i 's/main/_start/g' asm-syscall.s
    gcc -o asm-syscall asm-syscall.s -nostdlib
    strace -c ./asm-syscall

Les om `syscall` ca side 1320 i [Intel 64 and IA-32 Architectures
Software Developer Manual: Vol
2](http://www.intel.com/content/www/us/en/architecture-and-technology/64-ia-32-architectures-software-developer-instruction-set-reference-manual-325383.html)

### POSIX

##### POSIX System Calls

![image](/home/erikhje/office/kurs/opsys/03-os-intro/tex/../../resources/MOS_3e/01-18.pdf)

En oversikt over POSIX systemkallene. Merk at vi av og til kaller
systemkall det som egentlig er c-bibliotek-wrapperne til systemkallene
med samme navn.

##### `fork` System Call

![image](/home/erikhje/office/kurs/opsys/03-os-intro/tex/../../resources/MOS_3e/10-04.pdf)

Vi skal se nærmere på det systemkallet som lager prosesser på
Unix/Linux: `fork`:

    +--------------------+
    | PARENT PROCESS     |
    | PID=2313           |
    |                    |
    | int p;             |
    | p = fork();        |--+ Systemkallet fork() skaper en prosess som
    | if (p==0) //p=2321 |  | child av seg selv. Den nye child-prosessen
    |  printf("Child!"); |  | får tildelt sin egen PID (Prosess-ID), 
    | else               |  | f.eks. 2321. Slik at det fork() returnerer
    |  printf("Parent!); |  | i parent-prosessen er PID'n til den nye
    | exit(0);           |  | child-prosessen, altså 2321.
    +--------------------+  |
                            |
                            |
                            |
    +--------------------+  |
    | CHILD PROCESS      |  |
    | PID=2321,PPID=2313 |  |
    |                    |  |
    | int p;             |  | Den nye child-prosessen har et minneområde
    | p = fork();        |<-+ som er en eksakt kopi av parent-prosessen
    | if (p==0) //p=0    |    med et unntak: variabelen p (fork() sin 
    |  printf("Child!"); |    returverdi), DENNE ER ALLTID 0 i child-
    | else               |    prosessen og er dermed den vi kan bruke 
    |  printf("Parent!); |    for å skille mellom child og parent i koden
    | exit(0);           |
    +--------------------+  NB! første kodelinje som kjøres av child er
                                altså if (p==0), dvs child arver verdien 
                                av parent sin IP (instruction pointer)
                                (child arver også det meste av det andre i 
                                 parent sin PCB (Process Control Block))

DEMO

  - `fork0`  
    enkel fork og de relevante PIDene

  - `fork1-getpid`  
    hvordan vi kan bruke PIDene til å skille parent og child, samt
    systemkallet `getpid`

  - `fork2-fork-wait`  
    fork-i-fork, hva blir utskriften, samt en wait

  - `fork3-sleep`  
    kun mer fokus på `wait`, Merk: wait er altså en enkel form for
    *synkronisering*

  - `fork4-exec`  
    exec erstatter altså hele minnområde til prosessen, mao en ny
    prosess bør lages ved en kombinasjoner av `fork` og `exec` (disse
    brukes i praksis nesten alltid i sammen)

##### A Stripped-Down Shell

![image](/home/erikhje/office/kurs/opsys/03-os-intro/tex/../../resources/MOS_3e/01-19.pdf)

`fork` lager en eksakt kopi av seg selv, `exec` erstatter hele denne
kopien med en nytt program.

### WinAPI

##### Windows API Calls

![image](/home/erikhje/office/kurs/opsys/03-os-intro/tex/../../resources/MOS_3e/01-23.pdf)

På Windows endres selve systemkallene ofte mellom ulike versjoner av
windows og Microsoft anbefaler dermed at man ikke forholder seg direkte
til dem, men heller til Win32 APIet som skal være stabilt. Dvs dette er
tilsvarende mellomlag som vi akkurat så i Unix/Linux med et c-bibliotek
(libc) som man bruker istedet for å bruke systemkallene direkte.

(Alikevel er det selvfølgelig noen som forsøker dokumentere
systemkallene selv om de ikke er offisielt publisert fra Microsoft, se
<http://j00ru.vexillium.org/ntapi/>)

WinAPI er stort (over tusen funksjoner), og det er ofte litt uklart hva
som er et systemkall (dvs utføres i kernel mode) og hva som ikke er det
(WinAPI funksjoner som tidligere ble utført i kernel mode kan i ny
windows versjon kanskje være implementert i user mode).

Figuren (og vi) skal dermed fokusere på de WinAPI funksjonene som
noenlunde tilsvarer de vi skal se på i Unix/Linux verdenen.

## OS Structure

### Monolithic

##### Linux

![image](/home/erikhje/office/kurs/opsys/03-os-intro/tex/../../resources/MOS_3e/10-03.pdf)

##### Windows

![image](/home/erikhje/office/kurs/opsys/03-os-intro/tex/../../resources/MOS_3e/11-13.pdf)

Windows og Linux er begge i all hovedsak monolittiske
operativsystemkjerner. Dette betyr at alle metoder i kjernen har tilgang
til alle datastrukturer slik at kjernen kan kjøre mest mulig effektivt
(dvs uten å måtte skifte mellom user mode/kernel mode hele tiden).

##### Structure for a Monolithic System

![image](/home/erikhje/office/kurs/opsys/03-os-intro/tex/../../resources/MOS_3e/01-24.pdf)

Hvis alle deler av operativsystemet kompileres og linkes sammen til en
stor eksekverbar binærfil, kalles dette en monolittisk struktur. Linux
(OS-kjernen befinner seg i en fil som heter `vmlinuz`) er et eksempel på
dette. Windows (OS-kjernen befinner som i en fil som heter
`ntoskrnl.exe`) er også i praksis dette, selv om den også kan kalles en
hybrid mellom monolittisk og mikrokjerne.

I en monolittisk struktur kan det fortsatt være (og er selvfølgelig) en
indre struktur som vi f.eks. kan se for oss som vist i figuren, hvor
’service procedures’ er det kodesnuttene som utfører de respektive
systemkallene.

Effektivt, men fare for rotete struktur og vanskelig å få “100%”
stabilt.

### Layered

##### Structure of the THE OS

![image](/home/erikhje/office/kurs/opsys/03-os-intro/tex/../../resources/MOS_3e/01-25.pdf)

Lagdeling er et mye brukt konsept i informatikken (kanskje mest kjent
fra nettverk med TCP/IP modellens 5 lag), og ble foreslått for
operativsystemer av Dijkstra i 1968 i OSet fra THE (Technische
Hogeshcool Eindhoven).

Laginndeling var først og fremst rettet mot et ryddig design av
operativsystemet.

Hensikten med laginndeling er jo at hvert lag skal kunne kodes uavhengig
av hverandre og at kommunikasjon mellom lagene bare skjer oppover eller
nedover til nærmeste lag. I hvilken grad denne kommunikasjonen ble
overholdt i THE OSet er noe uklart.

Men dette konseptet likner litt på ring-konseptet innført med MULTICS og
vanlig på dagens x86 arkitektur.

### Microkernel

##### Structure of MINIX 3

![image](/home/erikhje/office/kurs/opsys/03-os-intro/tex/../../resources/MOS_3e/01-26.pdf)

Hvis hele OSet kjører i kernel mode så kan en feil i en driver (f.eks.
feil minneadressering i lydkortdriveren) føre til at OSet kræsjer.
Studier viser at gjennomsnittlig antall feil pr 1000 linjer kode er
gjerne fra 2 til 10. Dette betyr at et monolittisk OS med 5 millioner
linje kode fort kan ha minst 10.000 feil. Ikke alle disse er
nødvendigvis alvorlige, de fleste er sikkert harmløse, men en god del
vil kunne føre til systemkræsj.

Poenget med Microkernel er å dele OSet opp i små veldefinerte deler
hvorav kun kernel kjører i kernel mode.

Mikrokjernen i MINIX 3 er omtrent 1200 linjer C kode og 1400 linjer
assembly kode. Figuren viser hvordan MINIX 3 er strukturert.

Mikrokjerne er spesielt aktuelt i sikkerhets-kritiske systemer
(rakettstyringssystemer o.l.).

Tanenbaum sier at OSX er basert på Mach microkernel, men dette er
egentlig litt upresist, hvis du vil grave i detaljene kan du starte her:
<http://www.roughlydrafted.com/0506.mk1.html>

### Client-server

##### Client-Server Model

![image](/home/erikhje/office/kurs/opsys/03-os-intro/tex/../../resources/MOS_3e/01-27.pdf)

Modulene i et OS kan også arrangeres som en klient/tjener modell som
vist i figuren, og dermed muliggjør et distribuert OS.

### Virtual machines

##### Structure of VM/370 with CMS

![image](/home/erikhje/office/kurs/opsys/03-os-intro/tex/../../resources/MOS_3e/01-28.pdf)

IBMs VM/370 (fra 1972) var første populære hypervisor (opprinnelig kalt
virtual machine monitor) og gjorde at det for gjesteOSene (CMS i
figuren) så ut som de hadde hver sin fullblods System 370 maskin.

##### Type 1 and Type 2 Hypervisors

![image](/home/erikhje/office/kurs/opsys/03-os-intro/tex/../../resources/MOS_3e/01-29.pdf)

På x86 arkitekturen har virtualisering vært uinteressant inntil sent på
90-tallet, fordi ytelsen har vært for dårlig og arkitekturen har ikke
støttet virtualisering, dvs hvis et OS forsøker gjøre en instruksjon i
user mode som den ikke skal gjøre så skal det forårsake en trap, dette
skjer ikke på Intel og AMD arkitekturen. Men i Intel og AMDs
hardwarevirtualiseringstøtte som kom i 2005 løses dette problemet ved å
innføre et enda mer priviligert nivå enn kernel mode, dvs et ’kernel
kernel mode’ som en type 1 hypervisor kan kjøre i (slik at OS og
brukerapplikasjoner kan kjøre som før i kernel mode og user mode).

(i praksis er dette femte nivået egentlig slik at alle de fire
ringene/nivåene som vi er vant til (hvor kernel mode er ring 0 og user
mode ring 3) kalles non-root mode, og det innføres en egen root mode som
da en hypervisor kan kjøre i).

Derav utviklet vmware teknologien seg på slutten av 90-tallet, en type 2
hypervisor, mens klassisk type 1 hypervisor er blitt populært på x86
arkitekturen etter 2005.

Et annet alternativ er å modifisere gjesteOSet slik at det tilpasses
hypervisoren, det kalles paravirtualisering og ble populært med Xen.

## Theory questions

1.  Hva menes med *preemptive* og *non-preemptive* operativsystemer?

2.  Hva mener vi når vi sier at et OS er et *monolittisk system*? Nevn
    eksempel på OS med en slik design.

3.  Forklar sammenhengen mellom kommandolinje kommandoer, systemkall og
    programvareinstruksjoner. Gi eksempler.

4.  Hva er forskjellen på en trap og et interrupt?

5.  Beskriv mest mulig detaljert hva som skjer når systemkallet
    `read(fd,&buffer,nbytes)` utføres.

6.  Vi har gitt følgende c-program:
    
        main() {    
           int p;
           p = fork();
           printf ("%d \n", p);
        }
    
    Hvilke utskrifter kan vi få når denne koden kjøres om vi forutsetter
    at `fork()` systemkallet ikke feiler? Forklar hva tallene i
    utskriften faktisk betyr; ikke bare kjør programmet en gang for å så
    gi det som ett svar.

## Lab exercises

1)  Skriv en kommandolinje som teller antall prosesser som eies av
    `root`. Start kommandolinjen med kommandoen `ps aux`.

2)  Last ned C-kode eksemplene fra forelesningen (`fork` eksemplene) og
    studer de nøye.

3)  **Kommandolinjeargumenter og systemkall**  
    Lag deg en directory `execdemo` og i denne lag en fil `execdemo.c`
    som inneholder:
    
        #include <stdio.h>        /* printf */
        #include <unistd.h>       /* execve */
        
        int main(int argc, char *argv[]) {
          if(argc < 2) {
            printf("Usage: %s path1 [path2 [..]]\n", argv[0]);
            return 1;
          }
          char *path[2];
          path[0] = argv[argc-1];
          path[1] = NULL;
        
          printf("Et program som lett tryner, men enkel demo av exec\n");
          execve(path[0], path, NULL);
          printf("Denne blir aldri skrevet ut med mindre execve feilet\n");
        
          return 0;
        }
    
    Kompiler (som i forrige punkt) og kjør med tre forskjellige
    kommandolinjeargumenter og tenk nøye etter hva som skjer:
    
        ./execdemo ~/bin/hello
        ./execdemo /bin/ps ~/bin/hello
        ./execdemo ~/bin/hello /bin/ps
    
    Beskriv hva som skjer i hver av de tre kjøringene av programmet som
    er listet over her?
    
    NB\! Denne oppgaven forutsetter at laboppgaven i kapittel 1 der
    programmet hello ble laget er fulgt. Sørg for at programmet hello
    kjører om du skriver "\(\sim\)/bin/hello" i terminalen.

4)  *(Nå må vi også nevne at det er naturlig å lære seg verktøyet
    `make`, men det er opp til den enkelte av dere og vurdere om dere
    vil investere tid i)*

# Prosesser og tråder

## Outcome

##### Today’s Learning Outcome

  - Parallell programming

  - Troubleshooting

  - Conceptual framework

## Processes

### Model

##### Multiprogramming/Multitasking

![image](/home/erikhje/office/kurs/opsys/04-procsandthreads/tex/../../resources/MOS_3e/02-01.pdf)

Intro: Nå skal vi fokusere på hvordan vi deler CPU’n, men husk at det
egentlig skjer mange oppgaver parallellt i en datamaskin. En datamaskin
er egentlig et nettverk, det finnes mange prosessorer (stort sett en i
hver kontroller, en DMA kontroller, GPU’n osv) men nå fokuserer vi mest
på CPU’n på hovedkortet.

En prosess er et program under utførelse, tenk over følgende analogi: Du
skal bake en kake og har en oppskrift og et sett med ingredienser (mel,
egg, osv). A bake kaken er prosessen, du er CPUn, oppskriften er
programkoden og ingrediensene er inputdataene til programmet. Mens du
baker kommer plutselig en god venn av deg løpende inn på kjøkkenet med
en nese som blør som en foss. Du må da skynde deg å stoppe
kjøkkenmaskina di, sette unna ingrediensene og notere deg hvor langt du
var kommet i oppskriften, slik at du kan fortsette å bake litt senere.
Det å skifte ditt fokus over til å hjelpe å stappe papir opp i nesa på
vennen din, er det samme som en *context switch* (kalle også av og til
en *process switch*). Du må lagre unna alt du trenger for å starte på
igjen senere, for å jobbe med noe annet (starte en annen prosess).

Å skifte mellom mange prosesser, dvs kjøre mange programmer “samtidig”
kalles *multiprogramming/multitasking*. Dvs, strengt tatt kalles det å
ha flere programmer i minnet samtidig (og disse får kjøre hver for seg
inntil en av de blokkerer på I/O) for multiprogramming, mens det å
fordele tiden mellom disse programmene (enten ved at disse frivillig gir
bort CPU’en til hverandre eller operativsystemet aktivt fordeler tiden
basert på tidsluker) kalles multitasking.

Figuren viser forskjellige måter å se for seg hva som skjer i
prosessmodellen. Figur a viser hvordan instruksjonspekern (IP, også
kallt Program Counter - PC) kan behandle programmene i fysisk minne,
mens figur b viser hvordan vi kan tenke på dette som fire logiske
programtellere (og det finnes en for hver prosess men bare en brukes om
gangen). Figur c viser hvordan bare en prosess får CPU tid om gangen
(siden det finnes bare en fysisk CPU i dette tilfellet).

Prosesser i Unix/Linux verdenen er organisert i et hierarki, se DEMO  
`pstree -p` og `procexp` i windows (se hvordan procexp er under
powershell som er under GUI-shellet).

Dette er mye av historiske årsaker, dvs at den eneste måten å skape en
prosess på er for en prosess å fork’e (som regel kombinert med `exec`).
I Windows bruker man ikke dette hierarkiet (selv om det egentlig finnes
der også), og fork/exec er slått sammen i `CreateProcess`.

Når prosesser kjører er de i sin "egen lille boble". Prosesser som eies
av administrator (root) eller som eies av samme bruker har lov til å
sende signaler til hverandre. Eksempler på slike signaler kan være:

  - *SIGHUP*: "Hangup of controlling terminal". Om en prosess mottar
    dette signalet, betyr det at moderprosessen har avsluttet.

  - *SIGINT*: Et interrupt har ankommet fra tastaturet.

  - *SIGILL*: Dette signalet kommer av at prosessen prøvde å kjøre en
    ulovlig (illegal) instruksjon.

  - *SIGKILL*: Dette signalet er ett signal som sier at prosessen skal
    drepes. Dette signalet kan ikke fanges av en prosess fordi
    operativsystemet dreper prosessen umiddelbart.

  - *SIGTERM*: Dette signalet ber prosessen om å avslutte. Men,
    prosessen kan velge å ignorere det.

  - *SIGSEGV*: Dette signalet er også kjent som en segmentation fault.
    Det indikerer at prosessen prøvde å aksessere en del av minne der
    den ikke har tilgang.

  - *SIGUSR1* + *SIGUSR2*: Brukerdefinerte signaler. Kan brukes til hva
    som helst.

Som vi ser så er det en rekke ulike signaler som kan bli sendt. Dersom
det ikke er definert hva som skal skje når et signal blir mottatt, så
vil prosessen for de fleste signaler avsluttes. De fleste signaler
(SIGKILL er ett av unntakene) kan overstyres av de som lager programmet
som prosessen kjører. (DEMO: `signal1.c` `signal2.c`. SIGUSR1 til dd.)

To typer prosesser:

  - Brukerprosesser (vanlige brukerprogram)

  - Service prosesser (daemons/services i Unix/Linux, kalles bare system
    services i Windows), *de prosessene som kan kjøre uten at en bruker
    er innlogget* (men de er ikke like batch prosesser fordi batch
    prosesser som regel er kortvarige prosesser som skal kjøres uten
    interaktivitet, mens service prosesser er langvarige prosesser som
    ikke har mye å gjøre, men skal være der hele tiden).

Vi husker `fork` demoene fra sist, parallell i windows er
`win32CreateProcess.c`

MERK: en context switch er skifte mellom prosesser og tar mye tid, mens
en *mode switch/transition* er når en prosess benytter seg av
operativsystemet via systemkall (evn et interrupt/exception forekommer).
Selv om det da er OSet som kjører, regnes det som prosessen kjører i
kernel mode, se DEMO `perfmon` (Performance monitor - Add -
ProcessorMedPilOpp - Add privileged og user time, flytt musepekern/flytt
hele vinduet og se hvordan privileged grafen går opp for å håndtere
interrupts siden det grafiske systemet til Windows OSet kjører i kernel
mode).

##### Process Creation

1.  System initialization

2.  Execution of a process creation system call by a running process

3.  A user request to create a new process

4.  Initiation of a batch job

—–

1.  mange prosesser startes ved oppstart, de fleste av disse kalles
    services, f.eks. en prosess som kjører periodiske jobber (`cron` i
    Unix/Linux, `Task Scheduler` i Windows)

2.  en prosess starter gjerne en annen prosess (`fork/execve` eller
    `CreateProcess`) hvis det er oppgaver den skal gjøre som kan skilles
    ut (f.eks. en prosess som trenger kjøre en subkommando. Eller ett
    shell.)

3.  brukere starter prosesser via kommandoer eller museklikk

4.  på batch-systemer starter prosesser også når nye batchjobber lastes

##### Process Termination

1.  Normal exit (voluntary)

2.  Error exit (voluntary)

3.  Fatal error (involuntary)

4.  Killed by another (involuntary)

—–

1.  naturlig ferdig

2.  program velger å avslutte fordi f.eks. en fil som skal åpnes finnes
    ikke

3.  program avsluttes pga av en bug, f.eks. dele på 0 eller feil
    minneaksess

4.  prosessen får et signal om å avslutte (`kill` i Unix/Linux,
    `TerminateProcess` i Windows), dette signalet kommer fra
    operativsystemet, men kan være iverksatt av en annen prosess. En
    prosess får som regel lov til å sende signal til en annen prosess
    hvis prosessen har samme eier (eller har eier tilsv
    administrator/root)

##### Process States

![image](/home/erikhje/office/kurs/opsys/04-procsandthreads/tex/../../resources/MOS_3e/02-02.pdf)

Overgang 1 skyldes i all hovedsak at en prosess venter på I/O.

Overgang 4 skyldes i all hovedsak at en prosess ikke lenger venter på
I/O.

Overgang 2 og 3 skyldes scheduleren som bestemmer hvilken prosess som
skal kjøres på CPUn.

DEMO: se PROCESS STATE CODES i `man ps` (Ready og Running er ’R’,
Blocked er ’D’ eller ’S’).

(i `top` shift-O og w enter for å sortere på status, deretter shift-r
for å reversere å få de kjørende prosesser øverst, de med status ’R’)

### Implementation

##### Simplified OS View

![image](/home/erikhje/office/kurs/opsys/04-procsandthreads/tex/../../resources/MOS_3e/02-03.pdf)

Dette bildet er viktig å tenke og reflektere litt over, det er slik et
OS egentlig fungerer rent teoretisk, scheduleren sørger for å dele CPUn
mellom prosessene. Det eneste som trengs er en mekanisme for at
scheduleren får kjøre selv også, mellom hvert prosessbytte.

##### Process Control Block (PCB)

![image](/home/erikhje/office/kurs/opsys/04-procsandthreads/tex/../../resources/MOS_3e/02-04.pdf)

Operativsystemet har en tabell med oversikt over alle prosesser (prosess
tabellen), hvert entry i denne tabellen representerer en prosess og en
slik entry kalles ofte en *process control block (PCB)*.

I PCBn finnes all informasjon om en prosess, figuren viser typisk hvilke
entries vi finner i en PCB knyttet til prosesshåndtering,
minnehåndtering og filhåndtering.

I praksis finnes ikke prosesstabellen som en tabell-datastruktur, men vi
kan hente ut et snapshot med `top -b -n 1 > proctabell.txt`.

##### Process Switch/Context Switch

![image](/home/erikhje/office/kurs/opsys/04-procsandthreads/tex/../../resources/MOS_3e/02-05.pdf)

Husk figuren “Simplified OS View”. Her er det som skjer når det byttes
mellom prosesser. Et prosessbytte vil i de aller fleste operativsystemer
bli trigget av ett interrupt. Typisk er det interrupt som kommer fra en
hardware- timer.

Side det er snakk om interrupt, vil prosessbyttet starte slik som alle
interruptrutiner blir gjort, ved at instruksjonspekeren legges på
stacken, før den flyttes til starten av interruptrutinen. (Steg 1-2 i
figuren over).

Selve interruptrutinen vet at den skal lagre unna den nåværende
prosessor- statusen (registre etc.) på stacken, før den eventuelt setter
opp en midlertidig stack som den kan bruke under prosessbyttet (Sted 3-4
i figuren).

Når den forrige prosessen er trygt lagret, kjører typisk
operativsystemet sin scheduler. Dette er en funksjon som avgjør hvilken
prosess som skal være den neste til å kjøre (Trinn 5 - 7). Når det er
avgjort hva som er den neste prosessen til å kjøre, vil denne prosessen
startes opp. Det som skjer da er å flytte stackpekeren til denne
prosessens stack, og starte å laste inn de lagrede registrene etc. fra
denne, før programpekeren tilslutt flyttes til der den gitte prosessen
var i kjøringen av sin kode.

*Det viktigste å merke seg er at ved prosessbytte (også kalt context
switch) så lagres prosessen unna i sin PCB og en ny prosess hentes inn
fra sin PCB. En context switch tar typisk noen mikrosekunder*.

##### Two Types of Processes

  - CPU-bound  
    computing (scientific apps, multimedia) *Remember: hyperthreading
    doesn’t help CPU-bound processes*

  - I/O-bound  
    not much to do, mostly wait for I/O

Demo: `time regn.bash io.bash`

Hyperthreading gjør at det ser ut som vi har fire CPU’er men vi har
fortsatt bare to ALU’er dermed kan bare to prosesser regne om gangen.
Men har vi mange I/O-bound prosesser så får vi et mer effektivt system
siden vi kan kontekstsvitsje mye raskere mellom dem med hyperthreadede
CPU-kjerner.

## Threads

### Usage

##### Word Processor with Three Threads

![image](/home/erikhje/office/kurs/opsys/04-procsandthreads/tex/../../resources/MOS_3e/02-07.pdf)

Tråder kalles ofte lett-vekts prosesser og har samme hensikt som
prosesser: muligheten til å skape parallellitet. Årsakene til at vi har
tråder i tillegg til prosesser er:

  - trådene deler samme adresserom siden de tilhører samme prosess

  - å lage en tråd er 10-100 ganger så raskt som å lage en prosess

  - (en prosess med flere tråder kan utnytte multiprosessorsystemer)

Eksempelet i figuren er en tekstbehandler med tre tråder:

1.  En tråd tar imot input fra brukeren

2.  En tråd reformaterer dokumentet så fort det trengs

3.  En tråd tar periodisk automatisk backup

Det hadde ikke vært holdbart om brukeren måtte vente på reformatering
eller automatisk backup hver gang det skulle skje.

##### Multithreaded Web Server

![image](/home/erikhje/office/kurs/opsys/04-procsandthreads/tex/../../resources/MOS_3e/02-08.pdf)

En webserver bruker flertrådmodellen for å håndtere innkommende
forespørsler om websider. En “hovedtråd” (dispatcher thread) tar imot
henvendelser og starter en working thread for hver henvendelse, working
thread’ene må rett som det her hente noe fra disk og dermed blokkere på
I/O, som gjør at andre tråder bør kjøre. Dermed blir det en effektiv
webserver.

DEMO: `pstree -p` (trådene har klammeparenteser, prosessene
hakeparenteser). Tilsvarende i Windows med `procexp`

##### Code for Multithreaded Web Server

![image](/home/erikhje/office/kurs/opsys/04-procsandthreads/tex/../../resources/MOS_3e/02-09.pdf)

Webserveren kan kodes omtrent slik, med dispatcher tråden til venstre og
working thread til høyre, merk at kode er lik for alle working threads.

*Hvis vi har tilgjengelig en ikke-blokkerende read systemkall, så kan vi
bruke endelig-tilstands maskiner istedet for tråder, men dette er veldig
vanskelig å programmere.*

##### Three Ways to Construct a Server

![image](/home/erikhje/office/kurs/opsys/04-procsandthreads/tex/../../resources/MOS_3e/02-10.pdf)

Med endelig-tilstands maskiner måtte altså tilstanden lagres for hver
gang man kjør et ikke-blokkerende systemkall, og man måtte håndtere
signaler for når dette systemkallet var fullført, det er enklere med
tråder.

### Model

##### Thread Model

![image](/home/erikhje/office/kurs/opsys/04-procsandthreads/tex/../../resources/MOS_3e/02-11.pdf)

Merk forskjellen mellom prosesser og tråder: her er det altså til
venstre tre en-tråd prosesser med hvert sitt adresserom, mens til høyre
en prosess med tre tråder som deler ett adresserom.

Scheduling vil da foregå på to nivåer: en mellom prosesser og en på et
lavere nivå mellom trådene i prosessen. Dette er den klassiske modellen,
i praksis gjøres det litt annerledes som vi snart skal se i diskusjonen
om kernel-level vs user-level tråder.

##### Thread Items

![image](/home/erikhje/office/kurs/opsys/04-procsandthreads/tex/../../resources/MOS_3e/02-12.pdf)

Stacken til en tråd inneholder da lokale variable og returadresser for
funksjoner i tråden.

Merk: det er ingen beskyttelse mellom tråder siden de deler samme
adresserom, en tråd kan overskrive minne til en annen tråd i samme
prosess. Men dette er jo naturlig siden trådene er laget av samme
prosess og da skal samarbeide.

##### Each Thread Has It’s Own Stack

![image](/home/erikhje/office/kurs/opsys/04-procsandthreads/tex/../../resources/MOS_3e/02-13.pdf)

Tenk over hvorfor: tråder er innenfor samme prosess og har derfor samme
globale variable, men tråder gjør gjerne forskjellige oppgaver og det de
gjør forskjellig er å bruke forskjellig funksjoner noe som innebærer at
de har egne lokale variable og returadresser som nettopp er den
informasjonen som ivaretas på en stack: derav har tråder egen stack.

### POSIX

##### Pthread Function Calls

![image](/home/erikhje/office/kurs/opsys/04-procsandthreads/tex/../../resources/MOS_3e/02-14.pdf)

POSIX standard biblioteket for tråder er Pthread.

Merk det spesielle kallet `pthread_yield`: dette finnes fordi tråder
samarbeider og programmereren kan bestemme at en tråd skal gi fra seg
CPUn i bestemte kodesnutter. Mellom prosesser finnes ikke dette siden
prosesser antas å ville ha så mye CPU som mulig.

![image](/home/erikhje/office/kurs/opsys/04-procsandthreads/tex/../../resources/MOS_3e/02-15.pdf)

DEMO av denne koden:

`thread0.c` (hvorfor blir utskriften forskjellig hver gang?)

### User-level vs Kernel-level

##### User-level vs Kernel-level

![image](/home/erikhje/office/kurs/opsys/04-procsandthreads/tex/../../resources/MOS_3e/02-16.pdf)

Skål tråder være synlig for OSet? eller skal vi la tråder bare være en
ren applikasjonsgreie i userspace som OSet ikke bekymrer seg for?

Ved user-level tråder kan vi bruke tråder på et OS som ikke støtter
tråder. Hver prosess må da ha sitt eget *run-time-system* som har en
tråd-tabell tilsvarende OSet sin prosesstabell run-time-systemet er da
inne i bilde ved scheduling av tråder: når en tråd frivillig har gitt
opp CPUn. User-level tråder har følgende fordeler/ulemper:

  - \(+\) svitsje mellom tråder er kanskje 10x raskere enn ved
    kernel-level fordi man unngår overgang til kernel-level, dvs man
    unngår mode switch/transition)

  - \(+\) prosesser kan ha sin egen schedulingsalgoritme (samt benytte
    tråder på OSer som ikke støtter det som nevnt over)

  - \(-\) *ved blokkerende I/O kall blokkeres hele prosessen*

  - \(-\) ved hver page fault blokkeres prosessen

  - \(-\) tråder må gi opp CPU’n frivillig siden det ikke er noen
    naturlig klokkeinterrupt på user-level

  - \(-\) tråder benyttes som regel mest for applikasjoner hvor det er
    mye blokkering basert på I/O

Det siste argumentet er tungveiende og gjør at user-level tråder er
uvanlig.

Ved kernel-level tråder (som er det vanlige) betraktes trådene i all
hovedsak som prosesser og OSet har det run-time-systemet som vi måtte ha
i hver prosess ved user-level tråder. OSet har da trådtabellen (og hver
tråd har en TrådID akkurat som prosesser har en PID). Fordeler og
ulemper er:

  - \(+\) når en tråd blokkerer kan OSet kjøre en annen tråd fra samme
    prosess (dvs ingen blokkering av andre tråder i samme prosess)

  - \(-\) tar lengre tid å skifte mellom tråder

  - \(-\) tar lengre tid å opprette/slette tråder (derav forsøker en del
    systemer å resirkulere tråder, dvs merke de som ’not-runnable’ uten
    å slette datastrukturen)

POSIX biblioteket Pthread som vi nettopp nevnte kan benyttes til både
user-level og kernel-level tråder men som regel kjører vi det alltid på
et OS som støtter kernel-level tråder (slik som Windows og Unix/Linux)
og da bruker vi automatisk kernel-level tråder.

Til slutt: bildet er bittelitt mer komplisert enn bare prosesser og
tråder, og skillet mellom dem er mere uklart i praksis selv om
prinsippene er slik vi nettopp har gjennomgått, se i boka side 920-922
for hvordan dette er på Windows og les fjerde avsnitt på side 744 “In
2000, ...” for systemkallet `clone` på Linux.

## Theory questions

1.  Hva er en prosess? Hva består en prosess av?

2.  Beskriv kort tre viktige tilstander en prosess kan befinne seg i.

3.  Hva er en prosesskontrollblokk (PCB) og hva inneholder den?

4.  Lag en liste over egenskaper som deles av tråder i en prosess og
    egenskaper som er unike for hver tråd.

5.  Hva skjer om vi får et blokkerende I/O-kall i en tråd på user level?

6.  Forklar hvilke (og hvor mange) trådtabeller som finnes når:  
    \- tråder kjøres på user level?  
    \- tråder kjøres på kernel level?

## Lab exercises

1)  Skriv en kommandolinje som starter med kommandoen `ps aux` og
    deretter bruker `awk` til bare å skrive ut prosesstatus (kolonne 8)
    og prosessnavn (kolonne 11). (Hint: se eksemplene på bruk av `awk` i
    kapittel 1 i dette kompendiet.)

2)  <span id="procdia" label="procdia">\[procdia\]</span> **(OBLIG) Lage
    nye prosesser og enkel synkronisering av disse.**  
    Lag ett C-program som starter seks prosesser i henhold til følgende
    tidsskjema (`S` betyr start, `T` betyr terminer/exit):
    
        Prosess-
        nummer  
          ^
        5 |           S--------T
        4 |  S--------T
        3 |        S-----T
        2 S--------T
        1 |  S-----T
        0 S--T
          +-----------------------> tid i sekunder
          0  1  2  3  4  5  6  7
    
    Det eneste hver prosess skal gjøre er å kjøre følgende funksjon:
    
        void process(int number, int time) {
          printf("Prosess %d kjører\n", number);
          sleep(time);
          printf("Prosess %d kjørte i %d sekunder\n", number, time);
        }
    
    Bruk systemkallet `waitpid` for å synkronisere (dvs vente med å
    starte en prosess til en annen er terminert).
    
    TIPS:  
    De neste oppgavene inneholder eksempel på hvilke headerfiler som
    trenger å bli inkludert, samt eksempel på hvordan waitpid brukes.

3)  **To prosesser som skal inkrementere en global variabel.**  
    Kjør følgende program:
    
        #include <stdio.h>     /* printf */
        #include <stdlib.h>    /* exit */
        #include <unistd.h>    /* fork */
        #include <sys/wait.h>  /* waitpid */
        #include <sys/types.h> /* pid_t */
        
        int g_ant = 0;         /* global declaration */
        
        void writeloop(char *text) {
         long i = 0;
         while (g_ant < 30) { 
         /* print and increment g_ant only each 100000 iteration */
          if (++i % 100000 == 0) /* % is the same as modulo */
           printf("%s: %d\n", text, ++g_ant);
         }
        }
        
        /* Note: the following code does not do proper error checking
           of return values from functions, so this is not robust code, 
           but coded this way to be easy to read */
        
        int main(void)
        {
         pid_t pid;
        
         pid = fork();
         if (pid == 0) {        /* child */
          writeloop("Child");
          exit(0);
         }
         writeloop("Parent");   /* parent */
         waitpid(pid, NULL, 0);
         return 0;
        }
    
    Hvordan telles variabelen `g_ant`? Forklar kort hva som skjer.
    
    NB\! Disse to oppgavene (om prosesser og tråder som teller en global
    variabel) har en sammenheng. Fokuser derfor svaret på de deler som
    er ulike mellom disse oppgavene.

4)  **To tråder som skal inkrementere en global variabel.**  
    Kjør følgende program (merk: når du kompilerer programmer som
    benytter pthreads-tråder så må du angi pthreads-library når du
    kompilerer: `gcc -Wall -pthread -o prog prog.c`):
    
        #include <stdio.h>   /* printf */
        #include <stdlib.h>  /* exit */
        #include <pthread.h> /* pthread_t pthread_create pthread_join */
        
        int g_ant = 0;         /* global declaration */
        
        void *writeloop(void *arg) {
         long i = 0;
         while (g_ant < 30) {
          if (++i % 1000000 == 0)
           printf("%s: %d\n", (char*) arg, ++g_ant);
         }
         exit(0);
        }
        
        int main(void)
        {
         pthread_t tid;
         pthread_create(&tid, NULL, writeloop, "2nd thread");
         writeloop("1st thread");
         pthread_join(tid, NULL);
         return 0;
        }
    
    Hvordan telles variabelen `g_ant`? Forklar kort hva som skjer.
    
    NB\! Disse to oppgavene (om prosesser og tråder som teller en global
    variabel) har en sammenheng. Fokuser derfor svaret på de deler som
    er ulike mellom disse oppgavene.

# Prosesskommunikasjon, samtidighet og synkronisering

## Outcome

##### Today’s Learning Outcome

  - Parallell programming

  - Performance

  - Security

  - Conceptual framework

## Introduction

##### Three Issues

1.  How can one process/thread pass information to another?

2.  How can multiple processes/threads avoid getting in each others way?

3.  How can we make sure multiple processes/threads run in the proper
    sequence (with respect to each other)?

*Most of what chapter 2.3 states about processes also applies to threads
and vice versa.*

—–

Merk: vi skal altså se på mekanismer for å håndtere disse
problemstillingene og disse samme mekanismene kan anvendes som regel
enten på tråder eller på prosesser.

*Det du lærer i dette temaet er viktig i to sammenhenger: 1. Dette er
noen av de vanskeligste problemene operativsystemet må løse (altså
hvordan funker et operativsystem), 2. Dette er problemstillinger du vil
møte på i en lang rekke andre sammenhenger som programmerer. Med andre
ord, disse problemene finnes både i operativsystemet og i
applikasjonene.*

Men la oss bare nevne kort litt terminologi først.

### Atomicity

##### Atomicity/Atomic Operation

  - *Indivisible*

  - Certain low-level operations cannot be intercepted, they must be
    performed as one uninteruptable instruction, these are called atomic
    operations

—–

Atomisk betyr altså udelelig.

### Deadlock

##### Deadlock

When no processes/threads can proceed because everyone is waiting for an
event that only one of them can trigger.

—–

Deadlock (vranglås) betyr at vi har en låst situasjon hvor alle venter
på hverandre, tenk et trafikkryss hvor alle har vikeplikt for hverandre
og alle bare står og venter på hverandre.

### Starvation

##### Starvation

A process that is ready to run but is never given the CPU due to
scheduling mechanisms and/or priority (“an overlooked process”).

—–

### Race Conditions

##### Race Conditions

![image](/home/erikhje/office/kurs/opsys/05-ipc-concurrency/tex/../../resources/MOS_3e/02-21.pdf)

Problemet med delte variable i intern minnet: variablene `in` og `out`.

Prosess A og B ønsker begge å skrive ut mens en daemonprosess jobber i
bakgrunnen med å foreta selve utskriftene slik at A og B bare behøver
legge sine utskriftsjobber i køen. `out` indikerer hvilken jobb
daemonprosessen skal printe ut som neste jobb. `in` indikerer neste
ledige plass i køen (dvs der hvor en prosess kan legge sin
utskriftsjobb).

Følgende skjer:

1.  A leser `in` lik 7

2.  Scheduleren skifter til B

3.  B leser `in` lik 7, lagrer sin utskriftsjobb i plass 7,
    inkrementerer `in`, og fortsetter med andre instruksjoner inntil den
    blir avbrutt

4.  A får etterhvert tilbake CPUn, lagrer sin utskriftsjobb i plass 7
    (*og skriver dermed over B’s utskriftsjobb\!*), inkrementerer `in`,
    og fortsetter med andre instruksjoner inntil den blir avbrutt

*Situations like this, where two or more processes are reading or
writing some shared data and the final result depends on who runs
precisely when are called **race conditions***.

(debugging av slike programmer er et mareritt...)

##### A Common Example 1/2

What really happens when you run the code `i++;` ?

    register1 = i;
    register1 = register1 + 1;
    i = register1;

*Assume `i=5` is a global variable, let’s look at what can happen when
thread T1 does `i++;` and thread T2 does `i–;`*

—–

`i` er en variabel som befinner seg i minne, før CPUn kan gjøre noe med
den må den leses av og plasseres i et register. Så behandles den, før
den skrives tilbake til minne.

##### A Common Example 2/2

    T1: register1 = i;             [register1 = 5]
    T1: register1 = register1 + 1; [register1 = 6]
    
    <scheduler switches thread>
    
    T2: register2 = i;             [register2 = 5]
    T2: register2 = register2 - 1; [register2 = 4]
    
    <scheduler switches thread>
    
    T1: i = register1;             [i = 6]
    
    <scheduler switches thread>
    
    T2: i = register2;             [i = 4]

—–

`i` ender opp med verdien 4, den skulle blitt 5, men kunne altså blitt
både 4, 5 og 6 avhengig av rekkefølgen trådene T1 og T2 får kjøre.

Demo: `incdec.c`

### Critical Region/ Mutual Exclusion

##### Critical Region/Mutual Exclusion

![image](/home/erikhje/office/kurs/opsys/05-ipc-concurrency/tex/../../resources/MOS_3e/02-22.pdf)

Hvordan unngå race conditions? Jo vi må sørge for at bare en prosess kan
benytte en delt variabel om gangen. Vi kan løse dette ved å dele inn
koden vår i *kritiske sektorer* (der hvor felles variable behandles og
race conditions kan oppstå).

##### Four Conditions

1.  No two processes may be simultaneously inside their critical
    regions.

2.  No assumptions may be made about speeds or the number of CPUs.

3.  No process running outside its critical region may block other
    processes.

4.  No process should have to wait forever to enter its critical region.

—–

En mulig løsning er å disable interrupts når man går inn i kritisk
sektor, men dette er skummelt av to årsaker:

1.  OSet kan disable interrupts, men en brukerprosess kan ikke få lov
    til det siden vi da blir avhengig av at den slår på igjen interrupts

2.  disabling interrupts påvirker bare en CPU så dette vil ikke funke på
    fler-CPUr, mao brudd på betingelse 2 over

OSet benytter seg av av og til av disabling av interrupt selv når det
skal oppdatere egne variable, men dette gjøres i stor grad bare på
embedded systemer hvor det er single-CPU.

I praksis låser man minnebussen istedet for å disable interrupts når man
ønsker eksklusiv tilgang til minne i en kort periode.

## “Theory Solutions”

### Non-Solutions

##### Solution with Strict Alternation?

![image](/home/erikhje/office/kurs/opsys/05-ipc-concurrency/tex/../../resources/MOS_3e/02-23.pdf)

Vi kan løse problemet med eksklusiv tilgang til kritisk sektor ved å
bruke en variabel `turn` (0 eller 1) som sier hvem sin tur det er til å
gå inn i kritisk sektor. Den som det ikke er sin tur må vente og
konstant teste innholdet i variabelen turn inntil den endrer verdi, det
kalles *busy waiting* og variabelen `turn` kalles en *spin lock*.

Alternativet til busy waiting er å blokkere (og dermed context
switch’e), og valget mellom busy waiting eller å blokkere er relevant
på multiprosessor/multicore CPU’er hvor man ofte kaller denne
problemstillingen *spin or switch*. Dvs busy waiting/spinning er helt ok
hvis ventetiden er kort siden blokkering medfører context switch som
også er kostbart.

Dette fungerer mtp ekslusiv tilgang til kritisk sektor, men det er et
problem: Det er streng alternering, dvs de to prosessene må få hver sin
tur. Hvis den ene prosessen er mye raskere enn den andre, kan den bli
utestengt fra kritisk sektor fordi den andre er treg i ikke-kritisk
sektor, og dette er brudd på betingelse 3.

Mao, hvis vi går tilbake til eksempelet med spooling av filer for
printing, så kan ikke en prosess printe to filer etter hverandre ...

##### Solution with Array Instead?

    /* Process 0 */ 
    
    while(TRUE) {
       interested[0]=TRUE; 
       while(interested[other]==TRUE) /* loop */;
       critical_region();  
       interested[0]=FALSE; 
       noncritical_region();
    }

La oss løse problemet med streng alternering ved å ikke kreve streng
alternering, vi bytter ut `turn` variabelen med et to-dimensjonalt array
hvor hver prosess kan ha sitt element hvor de sier om de er interessert
i å gå inn i kritisk sektor eller ei. Denne løsningen garanterer også
eksklusiv tilgang (betingelse 1). La oss se på et tilfelle (TAVLE):

1.  prosess 0 setter sin interesse til TRUE

2.  scheduleren flytter CPUn til prosess 1

3.  prosess 1 setter sin interesse til TRUE og går inn i spinlock

4.  scheduleren flytter CPUn til prosess 0

5.  prosess 0 går inn i spinlock

6.  oisann ...

Vi får en deadlock, to prosesser som venter på hverandre, dette kommer
vi tilbake til senere i emnet.

Men kanksje vi skulle prøve å kombinere disse? dvs `turn` og
`interested` slik at vi kan unngå streng alternering men også unngå
deadlock?

### Peterson’s

##### Peterson’s Solution

![image](/home/erikhje/office/kurs/opsys/05-ipc-concurrency/tex/../../resources/MOS_3e/02-24.pdf)

Peterson’s løsning fra 1981 (som er en forenkling av Dekker’s løsning
fra 1965) er en løsning som ikke krever streng alternering, dvs dette er
en løsning som oppfyller de fire betingelsene nevnt tidligere (altså en
godkjent løsning).

Hvis både prosess 0 og prosess 1 kaller `enter_region` samtidig så vil
ved worst-case f.eks. prosess 0 rekke å sette `turn` lik 0, før prosess
1 får CPUn og skriver over `turn` med 1. Da vil prosess 0 gå inn i
kritisk sektor (virker kanskje litt unaturlig siden `turn` er lik 1 men
det slik det funker) mens prosess 1 må vente inntil prosess 0 har kjørt
`leave_region`.

##### Peterson’s (fra Silberschatz)

![image](/home/erikhje/office/kurs/opsys/05-ipc-concurrency/tex/../../resources/MOS_3e/02-24-1.png)

Peterson’s er egentlig litt lettere å forklare hvis vi ser på den slik
som enkelte andre lærebøker gjør (e.g. Silberschatz et al.) hvor vi
bytter bare om behandlingen av `turn` variabelen som vist i figuren.

Vi kan prøve forklare det slik:

1.  begge har satt interested til TRUE (begge sier “jeg er interessert”)

2.  prosess 0 setter turn til 1 (“jeg vil la kameraten min kjøre først”)

3.  scheduleren flytter CPUn til prosess 1

4.  prosess 1 setter turn til 0 (“jeg vil la kameraten min kjøre først”)
    og går inn i spinlock (“jeg venter til det blir min tur eller
    kameraten min ikke er interessert lenger”)

5.  scheduleren flytter CPUn til prosess 0

6.  prosess 0 går inn i kritisk sektor (“det er blitt min tur jo, da
    gidder jeg ikke vente”)

7.  prosess 0 blir ferdig med kritisk sektor og setter sin interesse til
    FALSE dermed kjører prosess 1

og prosess 1 kan kjøre flere ganger (“printe flere filer etter
hverandre”) den siden prosess 0 må sette sin interesse til TRUE for at
den skal bli med i alterneringen.

Dermed har vi en løsning som funker.

## “Practical Solutions”

### HW: TSL

##### Critical Region with TSL

![image](/home/erikhje/office/kurs/opsys/05-ipc-concurrency/tex/../../resources/MOS_3e/02-25.pdf)

Et problem som kan forekomme med software-løsninger som Peterson’s er at
måten moderne datamaskinarkitekturer realiserer maskinkode på gjør at
instruksjonene ikke nødvendigvis er de samme enhetene som de vi
behandler i software, dermed bør vi tilstrebe en hardware-støttet
løsning istedet.

Vi kan gjøre det enklere enn Peterson’s løsning ved å bruke
hardware-støtte (dvs bruke en instruksjon som finnes i
instruksjonssettet til datamaskinarkitekturen), siden det vi egentlig
ønsker oss er bare en lock variabel som vi kan sette/”låse” i en
atomisk operasjon (dvs uavbrutt operasjon, se tilbake på eksempelet med
register1 og register2).

Et alternativ til Peterson’s er å bruke den hardware-støttede
instruksjon `TSL` (Test and Set Lock) istedet. *Husk at minne kan være
delt, mens registerinnholdet er spesifikt for hver prosess/tråd*.
Nøkkelen med `TSL` er at den er atomisk, dvs lesing av innholdet i en
variabel i minne, kopiering av denne til et register, og overskriving av
denne i minne, disse tre operasjonene vil alltid skje som en uavbrutt
operasjon (fordi minnebussen låses i hardware under denne
instruksjonen). Og dermed kan enter\_region og leave\_region
implementeres som vist i figuren.

    +---------> | 
    |           v
    |   +---------------+
    |   |               |
    |   | REGISTER=LOCK |
    |   |     LOCK=1    |
    |   |               |
    |   +---------------+
    |           |
    |Ja, busy   |
    |waiting    |
    |           v
    |           +
    |         +   +
    |       +       +
    |     +           +
    +---+ REGISTER!=0?  +
          +           +
            +       +
              +   +
                +
                | Nei, enter 
                | critical region
                v

### HW: XCHG

##### Critical Region with XCHG

![image](/home/erikhje/office/kurs/opsys/05-ipc-concurrency/tex/../../resources/MOS_3e/02-26.pdf)

XCHG er bare en alternativ måte å bruke en lock variabel på ift TSL,
hensikten er den samme.

### Busy Waiting Problem

##### Priority Inversion Problem

Consider two processes H (high priority) and L (low priority).

The scheduling rules are such that H is run whenever its in ready state.

While L in its critical section, H becomes ready (e.g. I/O completed).

*H starts busy waiting and loops forever ...*

—–

Dette kan være en konsekvens av å velge busy waiting som strategi, dvs
uante konsekvenser som denne kan forekomme. For et eksempel fra
virkeligheten se [What really happened on Mars
?](https://cs.unc.edu/~anderson/teach/comp790/papers/mars_pathfinder_long_version.html)
(se på andre avsnitt i “The Failure”).

Busy waiting bør som regel unngås siden det er bortkastet CPU tid, la
oss heller se på muligheten for å blokkere en prosess istedet for å la
den bedrive busy waiting.

### “Sleep and Wakeup”

![image](/home/erikhje/office/kurs/opsys/05-ipc-concurrency/tex/../../resources/MOS_3e/02-27)

![image](/home/erikhje/office/kurs/opsys/05-ipc-concurrency/tex/../../resources/MOS_3e/02-27-1.png)

Istedet for busy waiting ønsker vi ofte bare å blokkere prosesser
(*switch* istedet for *spin*), dette kan vi se for oss at kan gjøres med
et systemkall `sleep()` hvor blokkeringen fjernes ved at en annen
prosess sender en `wakeup(PID)`.

Producer-Consumer er et litt generalisert problem som likner på
printer-spooler problemet vi så på tidligere. To prosesser deler et
buffer (hvor nøkkelen er den delte variablen `count`) hvor den ene
prosessen (producern) legger til enheter og den andre (consumern)
fjerner enheter. Hvis bufferet er full må producern vente, og hvis det
er tomt så må consumern vente. Tenk på enhetene som utskriftsjobber,
producern som en MSWord-tråd og consumern som en printerdaemon.

Figuren over viser nok et tilfelle av race condition ved at tilgang til
count-variabelen er ikke beskyttet med noe kritisk sektor. Hvis `count`
er 0 og consumern kjører og det blir context switch (schedulern flytter
CPUn til producern) der som det er markert i figuren, vil producern
etterhvert sende et wakeup signal som consumern ikke vil ta imot fordi
den har ikke kjørt `sleep()` enda (consumer vil sove, og snart vil
producer sove også). Dette er altså en *race condition*, siden
variabelen `count` kan endres mellom Time-Of-Check (if-statementet) og
Time-Of-Use (funksjonskallet `sleep()`).

(det er også mange andre race conditions her siden ingen kritisk sektor
er beskyttet: det kan forekomme at `count` har feil verdi hvis det skjer
en context switch mellom `insert_item` og den påfølgende
inkrementeringen av `count`, eller innenfor inkrementeringen av `count`
som i eksempelet med register1 og register2, og tilsvarende i consumer.)

Dette kan løses ved å innføre en variabel som tar vare på et
wakeup-waiting-bit som sleep sjekker før den blokkerer, men dette er en
lite generell løsning (dvs, løsningen funker ikke for mer enn to
prosesser, den *skalerer ikke* til flere prosesser). Hva hvis vi bruker
en spesiell type variabel som teller antall wakeup-waiting-bit stedet?
Jo, det er løsningen og det kalles et *semafor*. La oss se på hva som er
så spesielt med det.

### OS: Semaphores

##### Semaphore

“A special kind of an `int`”:

  - Counts *up* and *down* atomically

  - If a process/thread does a *down* on a semaphore which is 0, it is
    blocked (placed in a waiting queue)

  - If a process/thread does an *up* on a semaphore which is 0, one of
    the processes/threads is removed from the waiting queue (becomes
    unblocked)

Semaforer har to tilhørende funksjoner `down` og `up`:

``` 
   down virker slik:              up virker slik:

            |
            v
            +
          +   +
        +       +                        |
      +           +   nei                v
    +     S > 0     +----+          +---------+
      +           +      |          |         |
        +       +    +-------+      | S = S+1 |
          +   +      | Sleep |      |         |
            +        +-------+      +---------+
            |            |               |
         ja |<-----------+               v
            | 
            v
       +---------+
       |         |
       | S = S-1 |        
       |         | 
       +---------+
```

(Merk: avhengig av definisjonen av semafor så kan en semafor ha en
negativ verdi, den negative verdien vil da indikere hvor mange
prosesser/tråder som sover på den.)

Hvis en eller flere prosesser/tråder sover på en semafor, så vil en av
de vekkes og fullføre sin down operasjon. Etter en up kan semaforen
stadig være 0, men det er en færre prosess som sover (dvs blokkerer) på
den.

![image](/home/erikhje/office/kurs/opsys/05-ipc-concurrency/tex/../../resources/MOS_3e/02-28.pdf)

I figuren er Producer-Consumer løst med semaforer. Her brukes en binær
semafor til å kontrollere tilgang til kritisk sektor. I dette
kodeeksempelet kalles den variabelen `mutex` (Merk: en binær semafor er
IKKE det samme som en mutex, men de kan brukes ofte på samme måte, vi
kommer straks tilbake til forskjellen mellom de). `mutex` brukes altså
til eksklusiv tilgang til kritisk sektor, mens `empty` og `full` brukes
til *synkronisering*, dvs både til å holde rede på antall ledige plasser
i bufferet og blokkere (consumern blokkerer når `full` er lik 0, mens
producern blokkerer når `empty` er lik 0, mao vi må ha to semaphorer
siden et semaphore blokkerer bare når det blir 0, ikke når det blir 100
:).

Virker helt kurant ikke sant? vel, hva hvis du bytter om rekkefølgen på
de to `down`-kallene i producern, da kan producern blokkere hvis
bufferet er fullt mens mutex’n er 0, og neste gang consumern skal
aksessere bufferet vil den gjøre `down` på mutex’n og blokkere den og,
og vi har en deadlock ...

(mao, det negative med semaforer/mutex’er er at det er vanskelig å kode
riktig med dem).

DEMO: `1-en-producer-consumer-semafor.c`  
(vis koden, kjør, kommenter bort random wait på hvert sitt sted for å se
at det funker selv om producer er mye raskere enn consumer eller
motsatt))

I POSIX er altså en down på en semafor `sem_wait` mens en up er
`sem_post`.

### OS: Mutex

##### Mutex Implementation with TSL

![image](/home/erikhje/office/kurs/opsys/05-ipc-concurrency/tex/../../resources/MOS_3e/02-29.pdf)

Mutex kan implementeres i user mode som vist i figuren. Nøkkelen her er
`thread_yield`. Husk at en tråd som gjør busy waiting i userspace vil
låse hele prosessen fordi en tråd i userspace må gi fra seg CPUn
frivillig, det finnes ikke noe klokkeinterrupt for tråder i userspace.

Mutex er altså en enkel lock-variabel som enten er LOCKED (`1` evn `!0`)
eller UNLOCKED (`0`).

Merk: *noen lærebøker og tekster skiller ikke mellom mutex og binær
semafor, men i de flestes oppfatning er forskjellen den at en mutex har
en eier (dvs den som låser mutex’n må låse den opp), mens en semafor har
ingen eier (det behøver ikke være sammen prosess/tråd som gjør down og
up på en binær semafor).*

##### Mutexes in Pthread

![image](/home/erikhje/office/kurs/opsys/05-ipc-concurrency/tex/../../resources/MOS_3e/02-30.pdf)

DEMO: `2-en-producer-consumer-semafor-og-mutex.c`  
(vis koden, kjør, kommenter bort random wait på hvert sitt sted for å se
at det funker selv om producer er mye raskere enn consumer eller
motsatt, *denne koden viser at en mutex og en binær semafor kan som
regel ha samme funksjon*)

### OS: Condition Variable

##### Condition Variables in Pthread

![image](/home/erikhje/office/kurs/opsys/05-ipc-concurrency/tex/../../resources/MOS_3e/02-31.pdf)

Betingelsesvariable er et alternativ til semaforer, dvs det er en type
variabel som gjør at en tråd kan vente basert på en betingelse, og den
kan sende signal til andre som venter på variabelen. Brukes som regel
alltid sammen med en mutex, la oss se på et eksempel.

Merk: *det spesielle med en condition variable (betingelsesvariabel) er
at den har ingen verdi slik som en semafor. Den er kun en variabel som
kan ta imot et signal.*

![image](/home/erikhje/office/kurs/opsys/05-ipc-concurrency/tex/../../resources/MOS_3e/02-32.pdf)

Producer-Consumer solved with mutex and condition variable.

DEMO: `3-en-producer-consumer-mutex-og-condvar.c`  
(vis koden (Merk: *her er tellesemaforene erstattet med en bufferindex
variabel som behandles i kritisk sektor siden den testes etter mutex er
låst*), kjør, kommenter bort random wait på hvert sitt sted for å se at
det funker selv om producer er mye raskere enn consumer eller motsatt)

### ProgLang: Monitor

##### A Monitor

![image](/home/erikhje/office/kurs/opsys/05-ipc-concurrency/tex/../../resources/MOS_3e/02-33.pdf)

Siden programmering med semaforer kan være krevende, *kan monitor* være
et alternativ. En monitor er høyere nivå, dvs det er en konstruksjon som
finnes *i enkelte programmeringsspråk og dermed overlater vi til
kompilatoren* og sørge for mye av det vanskeligste med å få håndtering
av kritiske sektorer riktig.

Hovedpoenget er at prosedyrer (dvs de kritiske sektorene som regel),
variable og datastrukturer plasseres i en monitor. Variablene og
datastrukturene i en monitor kan bare håndteres via monitorens
prosedyrer. Prosesserer kaller da prosedyrene i monitoren *men bare en
prosess kan være aktiv i monitoren om gangen*.

I tillegg til gjensidig eksklusjon trengs mulighet for at en prosess kan
blokkere basert på betingelser som vi har sett før. Derav har man også
betingelsesvariable (condition variables) tilgjengelig.

![image](/home/erikhje/office/kurs/opsys/05-ipc-concurrency/tex/../../resources/MOS_3e/02-34.pdf)

Producer Consumer i Pidgin Pascal, et pseudokodespråk.

![image](/home/erikhje/office/kurs/opsys/05-ipc-concurrency/tex/../../resources/MOS_3e/02-35.pdf)

Producer Consumer løst i java, merk hvor monitoren er\!

DEMO: `ProducerConsumer.java`  
(vis koden, kjør)

DEMO: løs `incdec.c` med både semafor og med mutex

### LowLevel: Barriers

Av og til er vi nødt til å sørge for at det vi ønsker å gjøre skjer i
rett rekkefølge. Som vi vet, så kan prosessoren gjøre instruksjoner
"out-of-order", noe som betyr at rekkefølgen en instruksjon blir kjørt
kan endres. Senere skal vi og kikke på filsystemer som implementerer noe
som kalles "journalling". Dette er og avhengig av at data blir skrevet
til disk for at det skal bli utført korrekt.

Når vi definerer en "barrier" i koden vår, sier vi til maskinvaren at
alt som er definert at skal kjøre før barrier-en skal kjøres helt ferdig
før vi kjører det som er definert etter. Da har vi faste merker i
programmet vårt, der vi kan være helt sikre på hva som allerede er
gjort, og hva som skal skje etterpå.

En "barrier" kan og brukes til å synkronisere en gruppe prosesser/tråder
som alle jobber uavhengig av hverandre, men der det er viktig at de alle
venter på hverandre ved gitte punkt.

##### Barriers

![image](/home/erikhje/office/kurs/opsys/05-ipc-concurrency/tex/../../resources/MOS_3e/02-37.pdf)

## Theory questions

1.  I læreboka figur 2.21 finnes et eksempel med to prosesser som ønsker
    utskrift. Hvilken prosess sørger for å hente utskriftfilene fra
    spooler directory? Hvorfor kan det gå galt med denne løsningen?

2.  Gitt at du har flere CPUer, når er det ok å gjøre *busy waiting* i
    stedet for blokkering (blokkering \(=\) tvinge frem en context
    switch)?

3.  De fleste CPU-er har en assemblyinstruksjon som heter `TSL` eller
    `XCHG`. Forklar hvordan en av disse virker, og hva som er gjør at
    den er nyttig ved synkronisering av prosesser.

4.  Gitt følgende trådkode:
    
        void *consumer(void *arg)
        { int i; 
          for (i=0;i<5;i++){         
            pthread_mutex_lock(&mutex);
            if (state == EMPTY) 
              pthread_cond_wait(&signalS, &mutex);
        
            -----
        
            pthread_cond_signal(&signalS);
            pthread_mutex_unlock(&mutex);
          }
          pthread_exit(NULL);
        }
    
    Forklar hva `wait`- og `signal`-operasjonene i ovenstående
    programlinjer har som funksjon. Hva er hensikten med variabelen
    `mutex` og hvorfor forekommer den i `wait`-operasjonen?

5.  Gitt følgende program:
    
        01 int g_ant = 0;         /* global declaration */
        02
        03 void *writeloop(void *arg) {
        04  while (g_ant < 10) {
        05    g_ant++;
        06    usleep(rand()%10);
        07    printf("%d\n", g_ant);
        08  }
        09  exit(0);
        10 }
        11
        12 int main(void)
        13 {
        14  pthread_t tid;
        15  pthread_create(&tid, NULL, writeloop, NULL);
        16  writeloop(NULL);
        17  pthread_join(tid, NULL);
        18  return 0;
        19 }
    
    Forklar hvorfor utskriften fra dette programmet ikke nødvendigvis
    blir slik vi gjerne intuitivt tenker at den skal bli med en while
    løkke som teller fra 1 til 10:
    
        1
        2
        3
        4
        5
        6
        7
        8
        9
        10
    
    Legg inn de synkroniserings-mekanismene du ønsker (semforer, mutexer
    eller betingelsesvariable) for å garantere at utskriften blir slik
    vi ønsker. Forklar hvorfor du plasserer de slik du gjør. (du behøver
    ikke skrive opp igjen hele programmet, bare svar ved å angi mellom
    hvilke kodelinjer du vil innføre ny kode, og hvorfor.)

6.  Deres overordnede har ett program der en har en array (input) som
    inneholder en rekke verdier som det skal regnes litt på, og en annen
    array (output) der alle resultatene skal lagres. Dette programmet
    inneholder også en funksjon (startCalculating) som foretar
    beregningene, og denne funksjonen bruker ca 1 sekund hver gang den
    skal regne ut en verdi. En annen funksjon i programmet (work) er
    ansvarlig for å gå gjennom alle verdiene og gi de til
    startCalculating en etter en. Denne funksjonen ser ca slik ut:
    
        // Noen globale variable
        int next = 0;
        int input[2000];
        int output[2000];
        
        // Selve funksjonen work
        void work() {
          int myWork, result;
          // Iterer over alle verdiene i arrayet
          while(next < 2000) {
            // Hent verdi fra input
            myWork = input[next];
            // Beregn ny verdi
            result = startCalculating(myWork);
            // Lagre ny verdi i output
            output[next] = result;
            // Tell opp next slik at en går til neste verdi.
            next++;
          }
        }
    
    1.  Gitt den informasjonen som er vist lengre opp; hvor lang tid vil
        dette programmet trenge for å kjøre ferdig?
    
    2.  Deres overordnede har hørt at en kan øke hastigheten på
        beregninger ved å benytte tråder, og har dermed gitt deg
        oppgaven med å gjøre funksjonen "work()" istand til å bli kjørt
        av mange tråder parallelt. Benytt en valgfri
        synkronisering-mekanisme, og skriv den nye funksjonen "work()"
        slik at dette kan oppnås. List også opp eventuelle linjer som må
        legges til globalt (f.eks deklareringen av en mutex), og linjer
        som må legges til i main før trådene startes.
    
    3.  Gitt at programmet, etter at work() er gjort trygg i (b), blir
        kjørt med 8 parallelle tråder på en datamaskin som har 8
        prosessorer (CPU’s) tilgjengelig. Hvor lang tid ca vil
        programmet da ta for å bli ferdig?

## Lab exercises

1)  **(OBLIG) Lage nye tråder og enkel semafor-synkronisering av
    disse.**  
    Skriv om programmet fra forrige ukes lab (oppgave
    [\[procdia\]](#procdia) “Lage nye prosesser og enkel synkronisering
    av disse”) til å våre trådbasert i stedet for prosessbasert, og bruk
    semaforer istedet for prosessventing for å synkronisere de. Det kan
    hende du da vil møte på begrensningen i å overføre argumenter til en
    trådfunksjonen siden en trådfunksjonen under pthreads vil at
    argumentet sitt skal være en void-peker (dvs, en peker som kan peke
    til hva-som-helst). Slik du kan løse dette er at du definerer en
    struct med argumentene dine, oppretter og allokerer plass til den i
    main, og bare sender med en peker til den i det tråden opprettes:
    
        struct threadargs {
          int id;         /* thread number */
          int sec;        /* how many seconds to sleep */
          int signal[6];  /* which threads to signal when done */
        };
        .
        .
        .
        void *tfunc(void *arg) {
          struct threadargs *targs=arg;
        .
        .
        .
        int main(void)
        {
          int i,j;
          struct threadargs *targs[6];
          /* allocate memory for threadargs and zero out semaphore signals */
          for (i=0;i<6;i++) { 
            targs[i] = (struct threadargs*) malloc(sizeof(struct threadargs));
            for (j=0;j<6;j++) {
              targs[i]->signal[j]=0;
            }
          }
        .
        .
        .
    
    Allokerer `malloc` plass i data-området eller stack-området?

2)  **(OBLIG) Flere Producere og Consumere.**  
    Ta utgangspunkt i en av producer-consumer eksemplene fra
    forelesningen. Disse eksemplene hadde bare en producer og en
    consumer. Skriv om koden slik at programmet tar tallet \(N\) som
    kommandolinjeargument og det fører til at det blir \(N\) producere
    og \(N\) consumere (som alle sammen fortsatt jobber mot det samme
    bufferet). Utskriften fra programmet må vise hvilken tråd som har
    kjørt, f.eks.
    
        $ ./flere-producer-consumer-semafor 20
        main started
        (Producer5, idx is 1) =
        (Producer11, idx is 2) ==
        (Consumer14, idx is 1) =
        (Producer10, idx is 2) ==
        (Producer10, idx is 3) ===
        (Consumer7, idx is 2) ==
        (Producer15, idx is 3) ===

# Scheduling

## Outcome

##### Today’s Learning Outcome

  - Performance

  - Conceptual framework

## Introduction

##### Process Behaviour

![image](/home/erikhje/office/kurs/opsys/06-scheduling/tex/../../resources/MOS_3e/02-38.pdf)

(Scheduling var viktig før, og er viktig nå, men har sjeldent vært
viktig på PCer siden det stort sett bare er en bruker på de)

Skal CPU kjøre prosess A, B eller C? Scheduleren bestemmer.

``` 
 A
   \

 B - CPU

   /
 C
```

I utgangspunktet betyr ikke scheduling så mye på vanlige PC-er fordi de
er så raske at de begrenses mest av hvor fort brukeren klarer å gi CPUen
oppgaver. Men hvis brukeren benytter PC-en til videoprosessering, eller
å kjøre en lang rekke virtuelle maskiner får CPU-en fort nok å gjøre.

Hva scheduleren overordnet må sørge for:

  - Velge riktig prosess (en interaktiv brukerprosess er som regel
    viktigere enn en periodisk statistikkinnsamlingsprosess)

  - Ikke skifte prosess for ofte siden context switch er kostbart

Prosesser er enten (se figuren):

  - CPU-bound  
    krever mye CPU-kraft (f.eks. videoprosesserings-prosesser)

  - I/O-bound  
    bruker mest tid på å vente på I/O (de fleste vanlige prosesser)

*Merk: poenget er lengden på CPU-burst, IKKE lengden på I/O-wait, dvs
I/O-bound prosesser er ikke I/O-bound fordi I/O tar så lang tid, men
fordi de har lite å gjøre mellom hver I/O request.*

##### Many Schedulers

  - I/O block writes, Printer jobs, Routers, ...

  - CPU scheduling
    
      - Long-term/Admission scheduler
    
      - Medium-term/Memory scheduler
    
      - *Short-term/CPU scheduler*
    
      - (Dispatcher)

Long-term/Admission scheduler bestemmer om en prosess får komme inn i
ready-køen, dvs den kan stoppe et system fra å bli for fullt, “beklager,
jeg har ikke plass til flere prosesser nå...”.

Medium-term/Memory scheduler kjører litt oftere enn long-term og
vurderer om noen prosesser skal tas ut av (swappes ut midlertidig) eller
tas inn igjen i køen.

Short-term/CPU scheduler kjører oftest (derav “Short-term”) og er den vi
skal prate mest om, dvs den som bestemmer hvilke prosesser som slipper
til når på CPU(ene).

Når Short-term/CPU scheduler har gjort sin beslutning, er det
Dispatcheren som gjør jobben med å bytte ut kjørende prosess men den som
skal ta over.

##### Preemptive vs Nonpreemptive

  - *Preemptive scheduling/multitasking* means that the OS can take the
    CPU away from a running process

  - *Nonpreemptive/Cooperative scheduling/multitasking* means that the
    process has to voluntarily give up the CPU

<http://en.wikipedia.org/wiki/Preemption_(computing)>

##### When to Schedule?

  - When a process is created

  - When a process exits

  - When a process blocks on I/O

  - When an I/O interrupt occurs

  - At each single or every \(k\)th clock interrupt, commonly:
    
      - *preemptive* scheduling uses clock interrupts
    
      - *nonpreemptive* does not

—–

Når en prosess skapes, skal den nye kjøres eller den som skapte
prosessen (parent)? Scheduleren bestemmer.

Preempt betyr å avbryte, som regel sier vi at vi har preemptive
scheduling hvis en prosess som har CPUn ikke får lov til å beholde
CPU’en til den er ferdigkjørt (eller selv blokkerer på I/O eller gir
fra seg CPU’en frivillig). Dette betyr vanligvis at scheduleren bruker
klokkeinterrupt for å avbryte kjørende prosesser. Men vi sier også av og
til at alle tilfeller som fører til at scheduleren avbryter prosesser er
preemptive scheduling (som vi snart skal se for f.eks. Shortest
Remaining Time Next algortimen).

##### Scheduling Goals

![image](/home/erikhje/office/kurs/opsys/06-scheduling/tex/../../resources/MOS_3e/02-39.pdf)

I forhold til throughput og turnaround: det er avveininger, hvis man
prioriterer korte prosesser for høy throughput så vil en lang prosess
aldri slippe til og føre til at man får uendelig høy
gjennomsnitts-turnaroundtid.

I forhold til proportionality: hvis en bruker klikker på et ikon for å
sende en fax forventer hun at det tar tid, men klikker hun på krysset i
hjørne av vindu forventer hun at det skal lukkes med en gang.

Ift real-time systems, så gjelder predictability (forutsigbarhet) mest
for soft real-time, dvs multimedia, og det er viktigere at lyd spilles
av riktig enn at bilde ikke hakker (øyet vårt godtar litt dårlig bilde,
men øre vårt godtar ikke hakkete lyd).

## Batch Systems

### FCFS (FIFO)

##### First-Come First-Served, Same Arrival Time

![image](/home/erikhje/office/kurs/opsys/06-scheduling/tex/../img/fcfs-lik-ankomst.png)

(også kalt First-In First-Out, FIFO)

Dette er en non-preemptive schedulingsalgoritme.

Vi antar at vi har en *ready kø*, en kø med prosesser klare til å
kjøres.

Prosesser plasseres i alltid bakerst i ready køen og schedulern velger
den første i køen hver gang og lar den kjøre ferdig (dette er da en
nonpreemptive algoritme).

Enkel å forstå, enkel å implementere.

Langt unna optimal i mange tilfeller, f.eks. hvis det er en CPU-bound
prosess og mange I/O-bound prosesser som ønsker gjøre mange I/O-requests
så vil disse ta veldig lang tid siden den CPU-bound prosessen alltid vil
kjøre imellom. F.eks. hvis prosess A trenger å gjøre 1000 separate
diskreads, mens prosess B er CPU-bound med 1 sek CPU-arbeid mellom hver
I/O request, så vil A ta 1000 sek å fullføre (i motsetning til en
scheduler med preemption hvert 10. msec, som ville fullført A på 10
sek).

*FCFS er fair i betydning at den som venter lengst får slippe til, men
det er spesielt fordelaktig for lange CPU-bound prosesser.*

Gjennomsnittlig turnaround tid FCFS-SAT (Same Arrival Time):

\[\frac{8+12+16+20}{4}=\mathbf{14}\]

##### First-Come First-Served, Different Arrival Time

![image](/home/erikhje/office/kurs/opsys/06-scheduling/tex/../img/fcfs-ulik-ankomst.png)

Gjennomsnittlig turnaround tid FCFS-DAT (Different Arrival Time):

\[\frac{3+7+9+12+12}{5}=\frac{43}{5}=\mathbf{8.6}\]

### SJF/SPN

##### Shortest Job First, Same Arrival Time

![image](/home/erikhje/office/kurs/opsys/06-scheduling/tex/../img/spn-lik-ankomst.png)

(i interaktive systemer så kalles Shortest Job First for Shortest
Process Next).

En nonpreemptive algoritme *som antar at kjøretiden pr prosess er kjent
på forhånd*.

Denne er optimal mtp turnaround tid *gitt at det ikke ankommer nye
prosesser*.

*Problematisk hvis de store prosessene alltid må vente til slutt*. Hvis
nye korte prosesser ankommer hele tiden, vil de store aldri slippe til
... (starvation)

Gjennomsnittlig turnaround tid SJF-SAT:

\[\frac{4+8+12+20}{4}=\mathbf{11}\]

##### Shortest Job First, Different Arrival Time

![image](/home/erikhje/office/kurs/opsys/06-scheduling/tex/../img/spn-ulik-ankomst.png)

Gjennomsnittlig turnaround tid SJF-DAT:

\[\frac{3+7+11+14+3}{5}=\frac{38}{5}=\mathbf{7.6}\]

### SRTN

##### Shortest Remaing Time Next

![image](/home/erikhje/office/kurs/opsys/06-scheduling/tex/../img/srt.png)

En preemptive verjson av SJF. Scheduleren velger alltid den prosess som
har igjen kortest kjøretid. Samme ulempe som SJF (lange prosesser kan
ende opp vente lenge). Dvs her kjører scheduleren hver gang en ny
prosess ankommer køen og scheduleren har makt til å avbryte en lang
prosess med en ny kortere en. Men scheduleren gjør denne vurderingen
altså bare hver gang prosesser avsluttes eller nye prosesser ankommer
køen.

Gjennomsnittlig turnaround tid SRTN-DAT (SRTN-SAT er lik SJF-SAT):

\[\frac{3+13+4+14+2}{5}=\frac{36}{5}=\mathbf{7.2}\]

(men husk: SJF og SRTN antar at man kjenner kjøretid til prosessen, og
det gjør man ikke alltid)

## Interactive Systems

### RR

##### Round-Robin Scheduling, Quantum 1

![image](/home/erikhje/office/kurs/opsys/06-scheduling/tex/../img/rr-tidskvantum-1.png)

Round-Robin er en preemptive versjon av First-Come First-Served.

Prosessene får kjøre i et tidskvantum hver og legges så bakerst i køen.

Eneste interessante spm er hvor stort tidskvantum skal være, hvis
context switch tar 1msec og tidskvantum 4msec så er 20% av CPUn
bortkastet på overhead, men hvis tidskvantum er 100msec så kan brukere
oppleve altfor lang responstid hvis det er mange samtidige prosesser
fordi alle får altfor lang tid om gangen.

Gjennomsnittlig turnaround tid RR1-DAT:

\[\frac{4+17+13+14+7}{5}=\frac{55}{5}=\mathbf{11}\]

Demo: Linux, les om jiffies på `man 7 time` og se at en jiffie typisk er
2.5ms som default, men Linux sin Completely Fair Scheduler bruker ikke
jiffies, se “4. SOME FEATURES OF CFS” på  
<https://www.kernel.org/doc/Documentation/scheduler/sched-design-CFS.txt>
og sjekket vi `/proc/sys/kernel/sched_min_granularity_ns` ser vi den er
noen ms (og litt forskjellig på Ubuntu desktop og server). Vi kan også
se på `/proc/sys/kernel/sched_rr_timeslice_ms` og se at det er snakk om
noen ms hvis RR (round robin) benyttes istedet for CFS.

Demo: Windows, `clockres` gir “jiffie” intervallet, 2x for desktop, 12x
for server, kan endres med  
`SystemPropertiesAdvanced`, Advanced, Performance, Advanced og se hva
som skjer med i  
`hklm:\System\CurrentControlSet\control\PriorityControl`

##### Round-Robin Scheduling, Quantum 4

![image](/home/erikhje/office/kurs/opsys/06-scheduling/tex/../img/rr-tidskvantum-4.png)

Gjennomsnittlig turnaround tid RR4-DAT:

\[\frac{3+17+7+14+9}{5}=\frac{50}{5}=\mathbf{10}\]

### Priority

##### Priority Scheduling with Multiple Queues

![image](/home/erikhje/office/kurs/opsys/06-scheduling/tex/../../resources/MOS_3e/02-42.pdf)

Det er også vanlig å gi prosesser prioritet og kanskje prioritetsklasser
som vist i figuren. En video som spilles av må kjøre jevnt, mens en mail
som kan sendes kan bruke 1 sek ekstra slik at videoavspillingen
prioriteres.

Prioritet må som regel være dynamisk (hvis ikke vil lavt prioriterte
prosesser kanksje aldri slippe til, starvation ...).

I/O-bound prosesser bør gis høy prioritet (f.eks. bør de få prioritet
\(1/f\) hvor \(f\) er andel av siste kvantum brukt.)

Round-robin benyttes gjerne innenfor hver klasse.

### SPN with Aging

##### Shortest Process Next with Aging

How to estimate the next use of a specific process (e.g. command-line
commands)?

\[\bar{T}_{n}=(1-a)T_{n}+a\bar{T}_{n-1}\]

\[(\mbox{Def. } \bar{T}_{0}=(1-a)T_{0})\]

Tanenbaum har ikke med denne definisjonen av \(T_{0}\) så i læreboken
blir siste ledd i utregningen litt annerledes enn eksempelet nedenfor,
men det er ubetydelig i praktisk sammenheng (dvs med definisjonen vi tar
med her blir vektingen mer fornuftig, vi slipper at de to eldste leddene
har likt vekt, men det innebærer at summen av vektene ikke blir 1, men
dette betyr som sagt svært lite i praksis).

Merk: SJF=SJN=SPN, så denne kalles av og til Shortest Estimated Process
Next, siden det spesielle med denne er bare hvordan remaining time
estimeres. Og her er det aging kommer inn.

Vi har et sett med kjøretider  
\(T_{0}T_{1}T_{2}T_{3}\)  
mellom disse gjør vi estimater:  
\(T_{0}\bar{T}_{0}T_{1}\bar{T}_{1}T_{2}\bar{T}_{2}T_{3}\bar{T}_{3}\)  
hvor \(\bar{T}_{n}\) er estimatet som gjøres umiddelbart etter siste
registrerte kjøretid \(T_{n}\).

\(T_{n}\) representerer da siste nøyaktige *måling*, mens
\(\bar{T}_{n}\) representerer historikken (et *estimat*).

Med \(a=1/2\), vil estimatet for 4. gangs kjøring bli:

\[\bar{T}_{2}=\frac{1}{2}T_{2}+\frac{1}{2}\bar{T}_{1}=\frac{1}{2}T_{2}+\frac{1}{4}T_{1}+\frac{1}{8}T_{0}\]

Eksempel: Agingalgoritmen med \(a=\frac{1}{2}\) brukes til å predikere
(spå) kjøretider. Fire tidligere kjøringer fra den eldste til den siste
er 20ms, 15ms, 40ms og 10ms. Hva blir estimatet for neste kjøring?

Her er altså \(T_{0}=20, T_{1}=16, T_{2}=40, T_{3}=10\) og vi skal finne
estimatet for neste kjøring \(\bar{T}_{3}\):

\[\bar{T}_{3}=\frac{1}{2}T_{3}+\frac{1}{2}\bar{T}_{2}\]

vi setter inn for \(\bar{T}_{2}\) og deretter for \(\bar{T}_{1}\) og
\(\bar{T}_{0}\) slik at vi får

\[\bar{T}_{3}=\frac{1}{2}T_{3}+\frac{1}{4}T_{2}+\frac{1}{8}T_{1}+\frac{1}{16}T_{0}\]

som da gir oss

\[\bar{T}_{3}=\frac{10}{2}+\frac{40}{4}+\frac{16}{8}+\frac{20}{16} =
    5+10+2+1.25 = \mathbf{18.25}\]

### Guaranteed

##### Guaranteed Scheduling

  - With n processes each process get \(1/n\) of CPU time

  - Must keep track of CPUtime for each process

  - Always run the process with the least consumed CPUtime in relation
    to its guaranteed time

### Lottery

##### Lottery Scheduling

  - Basically achieves the same as guaranteed scheduling, but much
    simpler

  - Each process gets one (or more if higher priority) lottery ticket

  - Sceduling by a lottery draw

### Fair-Share

##### Fair-Share Scheduling

  - Guaranteed scheduling, but with respect to users

  - If user A have process a1, a2, a3 and a4, and user B have only
    process b1, the scheduling might be:  
    *a1 b1 a2 b1 a3 b1 ...*

## Real-Time Systems

##### Scheduling in Real-Time Systems

  - \(m\) periodic events, event \(i\) occurs with period \(P_{i}\) and
    requires \(C_{i}\) CPU time for each event, the load can only be
    handled if \[\sum_{i=1}^{m}\frac{C_{i}}{P_{i}}\leq 1\]

—–

3 prosesser, periodiske hendelser hvert 100, 200 og 500 msec. Hver
hendelse krever henholdsvis 50, 30 og 100 msec. Er dette mulig å
schedule’e ?

\[\frac{50}{100}+\frac{30}{200}+\frac{100}{500}=0.5+0.15+0.2=0.85\]

Ja, dette lar seg schedule’e slik at real-time systemet vil funke siden
\(0.85\leq 1\).

## Thread Scheduling

##### Thread Scheduling

![image](/home/erikhje/office/kurs/opsys/06-scheduling/tex/../../resources/MOS_3e/02-43.pdf)

Husk at her får vi scheduling på to nivåer ved user-level tråder, først
må prosessen schedules og innenfor denne prosessen schedules trådene.
Hvis en tråd på user-level gjør et blokkerende I/O kall blokkeres hele
prosessen.

Figuren viser situasjonen hvis vi har round-robin scheduling med
tidskvantum 50msec, men hver tråd har bare 5msec med jobb å gjøre, figur
a viser situasjonen for user-level, mens figur b viser situasjonen for
kernel level.

Det er viktig å huske at å svitsje mellom tråder er mye ganger raskere
enn å gjøre en full context switch, siden vi fremdeles er i den samme
prosessen (og dermed i samme adresserom, som vi skal se hva er når vi
tar for oss virtuelt minne) og dermed ikke trenger å bytte adresseerom.

MEN user-level scheduling kan være nyttig for applikasjonstilpasning:
f.eks. kan en user-level scheduler prioritere at dispatcher-tråden
alltid får kjøre når den er klar i webserver eksempelet vi så på ved
introen til tråder. En kernel scheduler vil ikke nødvendigvis være klar
over slike applikasjonsspesifikke forhold.

## Multiproc

##### How to Make Faster Computers

  - According to Einstein’s special theory of relativity, no electrical
    signal can propagate faster than the speed of light, which is about
    30 cm/nsec in vacuum and about 20 cm/nsec in copper wire or optical
    fiber.

  - *How does this relate to the speed of a computer?*

En prosessor med

  - 1GHz klokke kan signalet teoretisk maksimalt forplante seg 200mm pr
    klokkeperiode, og tilsvarende:

  - 10GHz - 20mm

  - 100GHz - 2mm

  - 1THz - 0.2mm

Jo mindre kretsene blir, jo større varmeutvikling og desto vanskeligere
å få bort varmen.

### Affinity

##### Processor Affinity/CPU Pinning

  - A process/thread can be restricted to run on only one or a set of
    CPUs

Et problem er at prosesser/tråder kanksje ikke bør hoppe random mellom
CPU’r siden det er mye prosess/trådspesifikk caching av data knyttet til
en CPU der tråden har kjørt. Scheduling som tar høyde for dette kalles
*affinity scheduling*. Dvs scheduling som forsøker å la en prosess/tråd
kjøre på samme CPU som den kjørte sist i håp om at det er igjen mye
relevant data for den prosess/tråden i den CPU’n sin cache.

Demo: `taskset -c 0 ./regn.bash`  
`sudo htop`, a for set affinity

Scheduleren gjør naturlig affinity så det er normalt lite behov for oss
å kunstig manipulere dette, men det kan være spesielle situasjoner som
f.eks. noe programvare som har lisenskostnader pr antall CPU’er i bruk
(Oralce-databaser kanskje).

### Gang-/Co-scheduling

##### Communication Between Two Threads

![image](/home/erikhje/office/kurs/opsys/06-scheduling/tex/../../resources/MOS_3e/08-14.pdf)

Mange tråder kommuniserer mye, derav trengs en løsning med både time og
space sharing.

Figuren viser hvilket problem som kan oppstå når tråden \(A_{0}\) og
\(A_{1}\) skal kommunisere, en toveiskommunikasjon kan ta 200msec noe
som er uholdbart.

Løsningen kalles *Gang scheduling*.

##### Gang-/Co-Scheduling

![image](/home/erikhje/office/kurs/opsys/06-scheduling/tex/../../resources/MOS_3e/08-15.pdf)

Gang scheduling

1.  Grupper av relaterte tråder schedules som en enhet (en gjeng).

2.  Alle medlemmene i gjengen kjører samtidig på ulike tidsdelte CPU’r.

3.  Alle medlemmene i gjengen starter og ender sine tidsluker sammen

Alle CPU’r schedules synkront.

Figuren viser et eksempelt på dette med 6 CPU’r og 5 prosesser med ulikt
antall tråder.

Poenget er altså at alle tråder i samme prosess bør kjøre samtidig i
samme tidsluke.

Dette praktiseres delvis i hypervisore som vmware, xen og linux/kvm, men
man er forsiktig fordi det kan også redusere ytelsen. Men hvis man vet
det er mye kommunisering mellom tråder ved bruk av spinlocks så er
gang/co-scheduling en fordel.

## OS Implementation

##### Current Use of Scheduling

<http://en.wikipedia.org/wiki/Scheduling_(computing)>

see Windows and GNU/Linux paragraphs, then see

<http://en.wikipedia.org/wiki/Multilevel_feedback_queue>

Merk at en multilevel feedback kø er en kombinasjon av FIFO (FCFS),
prioritetskøer, pre-emption, med round-robin i laveste nivå kø.

Demo: Windows, `perfmon`, se alle chrome-trådenes dynamiske prioritet

## Theory questions

Hva mener vi når vi sier at schedulingen er non-preemptive? Hvilke
systemer bruker preemptive og non-preemptive scheduling?

Hva ligger i begrepet “starvation” ifb med Shortest Job First (SJF)
scheduling algoritmen?

Anta Round Robin scheduling. Vurder konsekvensene ved korte og lange
tidskvantum.

Nevn noen fordeler vi har ved prioritert scheduling.

Anta ett sett med fire prosesser med ankomst-tid og burst/CPU-tid (i ms)
gitt i tabell:

| Prosess | Burst/CPU-tid (ms) | Ankomst-tid |
| :------ | :----------------- | :---------- |
| P1      | 7                  | 0           |
| P2      | 4                  | 2           |
| P3      | 1                  | 4           |
| P4      | 4                  | 5           |

Tegn et Ganttskjema (tidstabell) som illustrerer eksekveringen av
prosessene for hver av følgende schedulingsalgoritmer:

First-Come First-Served (FCFS)

Shortest Job First (SJF)

Shortest Remaining Time Next (SRTN)

Round Robin med tidskvantum på 2ms

Hva blir gjennomsnittlig turnaroundtid for hver av
schedulingsalgoritmene?

## Lab exercises

1)  **(OBLIG) Dining philosophers.**  
    Dining philosophers er et klassisk problem innen operativsystemer,
    og er beskrevet i læreboka i kapittel 2.5.1. Første gang man leser
    om det virker det kanskje som et snodig teoretisk problem, men det
    adresserer en del sentrale problemstillinger:
    
      - Kritisk sektor/gjensidig ekslusjon  
        Filosofene må dele gafler/spisepinner.
    
      - Deadlock (vranglås)  
        Siden gaflene/spisepinnene er ekslusive ressurser og filosofene
        må ha to av dem for å spise, innebærer det en fare for deadlock.
    
      - Starvation (utsulting)  
        Alle filosofene må få mulighet til å spise.
    
      - Paralellitet  
        Det må være mulig for flest mulig filosofer å spise om gangen.
    
      - Rettferdighet  
        Alle filosofene bør få mulighet til å spise like mye.
    
    I denne omgang er vi primært opptatt av de to første. Programmer et
    forslag til løsning av dining philosophers, bruk gjerne tråder som
    filosofer. *Du kan gjerne ta utgangspunkt i figur 2-47 (s170) i
    læreboka og implementere den koden komplett i C.*

# Virtuelt minne, paging og segmentering

## Introduction

Repeter tallsystemer med [The deal with numbers: hexadecimal, binary and
decimals - bin 0x0A
(LiveOverflow)](https://www.youtube.com/watch?v=mT1V7IL2FHY)

##### Question

*What is the multi-gigabyte file `pagefile.sys` hidden in my root
directory?*

—–

`Get-ChildItem C:\ -force`

##### In the Ideal World

“Hello, Im a process, I would like memory that is

  - *Private*

  - *Infinitely large*

  - *Infinitely fast*

  - *Nonvolatile* (does not loose data when powered off)

  - *(Inexpensive)*”

—–

Vi kan desverre ikke tilby dette, men vi kan tilby minnehierarkiet fra
første forelesning (HW Review - Memory).

### Free space

##### Memory Management, Bitmaps/Linked Lists

![image](/home/erikhje/office/kurs/opsys/07-memman-vm-paging-seg/tex/../../resources/MOS_3e/03-06.pdf)

En bitmap er en datastruktur der en har N bits som representerer N page
frames, og hvert enkelt bit sier om en gitt page frame er ledig eller
opptatt. Det benyttes i mange sammenhenger for å holde rede på ledig
plass i minne eller på disk, fordi en bit i en bitmap representerer
gjerne en page på 4KB og dermed blir aldri bitmap’n nevneverdig stor.

Det er veldig tregt å søke gjennom en bitmap etter en gitt størrelse
ledig plass. Se bare for deg hvordan ett slikt søk må foregå for å f.eks
finne ett ledig hull på 2MB.

## Virtual Memory.

##### The Memory Management Unit

![image](/home/erikhje/office/kurs/opsys/07-memman-vm-paging-seg/tex/../../resources/MOS_3e/03-08.pdf)

MMU oversetter *virtuelle adresser* til *fysiske adresser*.

Det store poenget er at vi kan kjøre programmer som bare delvis er i
minne, fordi vi innfører et virtuelt adresserom i tillegg til det
fysiske adresserommet.

Virtuelt adresserom kan vises med `vmmap.exe` og `procexp.exe` på
Windows og Ubuntu System Monitor. Kjør `hello-stop.c`.

Merk og inndelingen av minnet i kernel space / user space :  
<http://duartes.org/gustavo/blog/post/anatomy-of-a-program-in-memory>

Merk: 64-bits adresser betyr i praksis 48-bit i dag på X86-64, for å
unngå kompleksiteten av å implementere en full 64-bit adressebuss. På
windows (tom. Windows 8) implementerer 44-bit adresserom som gir oss en
kernel/user space inndeling på 8TB/8TB. Linux og Windows fra 8.1 og
nyere bruker bruker 48-bit som gir kernel/user space inndeling på
128TB/128TB.

Det er sansynlig at adressebusser på 64 bit kommer til å komme når det
blir mer vanlig med mange TB minne i datamaskiner.

Prosesser som ser hvert sitt virtuelle adresserom (ofte kalt memory
map), og disse virtuelle adresserommene benytter mange eller få page’r i
fysisk minne.

### Paging

##### Virtual and Physical Addresses

![image](/home/erikhje/office/kurs/opsys/07-memman-vm-paging-seg/tex/../../resources/MOS_3e/03-09.pdf)

Eksempel for 16-bits adresser (64KB) og 32KB fysisk minne (dvs 15-bits
fysiske adresser).

Det virtuelle adresserommet er delt inn i *page’er* (av og til kalt
*virtuelle page’er*. De tilsvarende enhetene i det fysiske minne kalles
*page frames*. Hvis et program forsøker adressere en page som ikke er
mappet til en page frame, blir det et interrupt som kalles en *page
fault* (dette er et interrupt av type *exception*, dvs i samme kategori
som det interrupt vi får hvis vi forsøker dele på null i et program).
Operativsystemet skriver da en lite-brukt page frame tilbake til disk og
laster inn den ønskede page frame istedet. Overføringer til og fra disk
skjer alltid i hele page’er.

(NB\! Interrupt kategoriseres litt ulikt, vi forsøker å følge
kategoriseringen Intel benytter med hardware interrupt, exceptions, og
trap (software interrupt)).

*Det å holde page tabellen oppdatert (dvs administrere page tabellen) og
å frakte page’er mellom disk og RAM, kalles ofte paging*.

##### Internal Operation of the MMU

![image](/home/erikhje/office/kurs/opsys/07-memman-vm-paging-seg/tex/../../resources/MOS_3e/03-10.pdf)

Figuren viser hvorfor vi må ha en page størrelse \(2^n\). Fysisk adresse
blir page frame nummer med påhekta offset (hekta på som de minst
signifikante bit’ene).

Virtuelt minne/virtuelt adresserom finnes altså IKKE, det er en
abstraksjon, et adresserom som prosessene forholder seg til. Vi kan
tenke på at som regel (dvs ved de fleste typer implementasjoner) så er
virtuelt minne representert via page tabellen.

##### A Page Table Entry

![image](/home/erikhje/office/kurs/opsys/07-memman-vm-paging-seg/tex/../../resources/MOS_3e/03-11.pdf)

Present/Absent bit, Protection bits, Modified bit (*Dirty bit*),
Referenced bit, Caching disabled bit (viktig ved memory-mapped I/O)

32 bit entries er vanlig.

  - mapping virtuell til fysisk adresse må være kjapt (hvis Page table
    er i RAM, vil være henting av instruks medføre en ekstra
    RAM-referanse som medfører av programmet vil bruke dobbelt så lang
    tid i praksis...)

  - page tabellen må ikke være for stor

### TLB

##### Translation Lookaside Buffer (TLB)

![image](/home/erikhje/office/kurs/opsys/07-memman-vm-paging-seg/tex/../../resources/MOS_3e/03-12.pdf)

    -+  Virtual (logical) address
    C|  +-------------+
    P|->|pagenr|offset|
    U|  +-------------+
    -+     |
           |      pagenr framenr
           |     +--------------+
           |  +->|      |       |
           |  +->|      |       |
           |  +->| Translation  |TLB hit
           +--+->| Lookaside    |------+
              +->| Buffer       |      |
              +->|      |       |      |              
              +->|      |       |      |              +--------+
              +->|      |       |      |              |        |
              |  +--------------+      |              |        |
              |                        | Physical     |Physical|
              |TLB miss                v address      |memory  |
              |                    +--------------+   | (RAM)  |
              |                    |framenr|offset|-->|        |
              |                    +--------------+   |        |
              |     +-----+            ^              |        |
              |     |Page |            |              +--------+
              +---->|Table|------------+
                    |     |
                    +-----+

TLB er en type cache som cacher page-table entries slik at man ikke
trenger gå til page-tabellen for hver gang en adresseoversetting trengs.
TLB er en hardware device som kan søke på alle sine entries paralellt,
og inneholder typisk de siste N brukte oversettelsene fra page til
page-frame.

Ulike prosessorarkitekturer implementerer TLB forskjellig, og det er mer
regelen enn unntaket at det er flere TLB-er på en prosessor. En TLB kan
være organisert på flere nivå (L1, L2, L3 etc.), og ved systemer med
flere prosessorkjerner er det ikke uvanlig at det er TLB-er knyttet til
hver enkelt prosessor. Da har kanskje hver kjerne individuelle L1 TLB
mens L2 TLB er felles for alle kjernene på en sokkel. Selve strukturen
for TLB varierer fra arkitektur til arkitektur, men en kan anta at den
finnes, har flere nivå, og at hver prosessorkjerne har eksklusiv tilgang
til deler av TLB-en.

Hva slags typisk situasjon viser figuren? (svar: kanksje en for løkke
som har sin kode i page 19-21, med data i 129 og 130, med data-index’er
i 140 og stack i 860 og 861)

Ved programeksekvering vil et stort antall minnereferanser være til et
fåtall page’er.

Eksempel:

Anta 10ns for å aksessere TLB, 100ns for å aksessere minnet.

Det å hente en byte hvis page er i TLB blir da 10ns + 100ns = 110ns.

Ved en TLB miss får vi 10ns + 100ns + 100ns = 210ns (altså et tillegg på
100ns siden vi må via page table)

Ved 98% hit rate blir effektiv aksesstid:

0.98\(\times\)110ns + 0.02\(\times\)210ns = **112ns**

En TLB har typisk 64 entries.

HVA VIL DET EGENTLIG SI AT TLB ER EN TYPE CACHE?  
se fjerde avsnitt på <http://en.wikipedia.org/wiki/CPU_cache>  
(se forhold til L1,L2,L3 og delen om “Example: the K8”)

##### Page Fault terminology

  - TLB miss / Soft miss  
    Page Table Entry (PTE) is not TLB.

  - Minor page fault / Soft miss / Soft (page) fault  
    Page is in memory but not marked as present in PTE (e.g. a shared
    page brought into memory by another process)

  - Major page fault / Hard miss / Hard (page) fault  
    Page is not in memory, I/O required.

DEMO:

    \time -v gimp
    \time -v gimp
    sync ; echo 3 | sudo tee /proc/sys/vm/drop_caches
    \time -v gimp

### Multilevel PT

##### Multilevel Page Tables

![image](/home/erikhje/office/kurs/opsys/07-memman-vm-paging-seg/tex/../../resources/MOS_3e/03-13.pdf)

Gjør at ikke alle page tabellene må være i minnet samtidig.

``` 
Binary address (0x00403004):
0000 0000 0100 0000 0011 0000 0000 0100
10bit-nivå1| 10bit-nivå2| 12bit-offset -----------------+
                                                        v
                                      20bit-framenr|12bit-offset -> Fysisk
             +------+                         ^                     minne
10bit-nivå1  |Ytre  |  10bit-nivå2            |                     (RAM)
er index     |Page  |  er index i    +------+ |
i ytre Page  |Table |  Page Table 1  |Page  | |
Table        |      |         |      |Table | |
  |         2|      |         |      | 1    | |
  +-------->1|      |-+       +---->3|      |-+
            0|      | |             2|      |  
             +------+ |             1|      |
                      |             0|      |
                      +------------->+------+
                Adresse
                til Page   
                Table 1    
```

### Inverted PT

##### Inverted Page Table

![image](/home/erikhje/office/kurs/opsys/07-memman-vm-paging-seg/tex/../../resources/MOS_3e/03-14.pdf)

Ved invertert page table har page tabellen en entry pr page frame
istedet for en entry pr page (page frames pr prosess søkes da opp i page
tabellen basert på PID gjerne). Gjør det vanskelig å foreta mapping
virtuell til fysisk adresse. *Løses i praksis med TLB\!* Løses også med
en ekstra hash tabell for page tabellen istedet for lineært søk direkte
i page tabellen.

Trenger invertert page table eller multi-level page tabel fordi:

  - Ved vanlig page table og 32-bits system, 4KB page størrelse blir det
    1M entries som gir en 4MB page table størrelse per prosess.

  - Ved vanlig page table og 64-bits system, 4KB page størrelse blir det
    \(\frac{2^{48}}{2^{12}}=2^{36}\) entries, hver entry på 8 byte gir
    ca 512TB page table størrelse per prosess\!

## Segmentation

"As of x86-64, segmentation is considered obsolete and is no longer
supported, except in legacy mode." (Tanenbaum s. 248)

## Theory questions

1.  Hva brukes en bitmap til i forbindelse med minnehåndtering i et
    page-basert minne?

2.  Hvilket felter finnes i en pagetable entry?

3.  Hvilke prinsipper kan vi benytte for å redusere størrelsen på en
    pagetable i internminnet?

4.  Affinity scheduling (“CPU pinning”) reduserer antall cache misser.
    Reduseres også antall TLB misser? Reduseres også antall page faults?
    Begrunn svaret.

5.  Hvor stor blir en bitmap i et page-inndelt minnesystem med
    pagestørrelse 4KB og internminne på 512MB?

6.  Tanenbaum oppgave 3.5  
    For hver av disse minneadressene (desimal, altså oppgitt i 10-talls
    systemet), regn ut hva som vil være det virtuelle page-nummeret og
    hva som vil bli offset inn i page-en ved en page-størrelse på 4K og
    8K: 20000, 32769, 60000.

7.  Tanenbaum oppgave 3.11  
    Anta at du har en maskin som har 48-bits virtuelle adresser og
    32-bit fysiske adresser:
    
    1)  Hvis en page er 4K, hvor mange oppføringer (entries) vil det
        være i page tabellen hvis den bare har ett nivå? Forklar.
    
    2)  Anta at maskinen har en TLB med plass til 32 oppføringer. Hvor
        effektiv vil denne TLB-en være om det kjøres ett lite program
        (der all koden får plass i en page) som har en for-løkke som går
        gjennom ett svært int-array som går over flere tusen pages?
        Forklar.

## Lab exercises

1)  **(OBLIG) Prosesser og tråder.**  
    Lag et script `myprocinfo.bash` som gir brukeren følgende meny med
    tilhørende funksjonalitet:
    
    ``` 
      1 - Hvem er jeg og hva er navnet på dette scriptet?
      2 - Hvor lenge er det siden siste boot?
      3 - Hva var gjennomsnittlig load siste minutt?
      4 - Hvor mange prosesser og tråder finnes?
      5 - Hvor mange context switch'er fant sted siste sekund?
      6 - Hvor mange interrupts fant sted siste sekund?
      9 - Avslutt dette scriptet
    
    Velg en funksjon:
    ```
    
    Hint: bruk en `switch case` for å håndtere menyen, og hvert menyvalg
    bør føre til en `echo` hvor svaret settes inn via `$()`, punkt 2 og
    3 finner du via `uptime` (eller fra `/proc/{uptime,loadavg}`) og for
    punkt 5 og 6 kan du hente ut info fra `/proc/stat`, se
    <http://www.linuxhowtos.org/System/procstat.htm>

2)  Skriv om `myprocinfo.bash` scriptet til å bruke `whiptail` til å
    vise meny. Hint: `man whiptail` og  
    <span><http://stackoverflow.com/questions/1562666/bash-scripts-whiptail-file-select></span>  
    <span><https://en.wikibooks.org/wiki/Bash_Shell_Scripting/Whiptail></span>.

# Page replacement algoritmer, design og implementering

## Page Replace

### Optimal

##### Optimal Page Replacement Algorithm

  - Label all pages with the number of instructions that will be
    executed before that page is referenced

  - Evict the page with the highest label

—–

Se Virtual Memory på Wikipedia, figur, Hva skjer når RAM blir fyllt opp?

Page faults fremtvinger valget av hvilken page frame som skal erstattes
(hvis minnet er fullt eller prosessen har nådd sine max page frames den
får lov å bruke).

Denne optimale algoritmen kan simuleres slik at de foreslåtte reelle
algoritmen kan sammenlikne seg med den. Hvis man klarer en algoritme som
er innenfor 1-2% av den optimale er ikke det så verst...

### Random

##### Random Replacement Algorithm

  - Evict a random page\!

### FIFO

##### First-In First-Out (FIFO)

  - First-in first-out

Første page som er lagt inn i minnet, er også den første som fjernes når
minnet blir fullt. Dette gir muligheten for at en page som fjernes
gjerne er en som er mye brukt, og dermed på legges rett tilbake.

### Second-chance

##### Second-Chance Page Replacement Algorithm

![image](/home/erikhje/office/kurs/opsys/08-memman-pagereplace-design-impl/tex/../../resources/MOS_3e/03-15.pdf)

Er FIFO men benytter Referenced bit’n i tillegg for å fjerne svakheten
ved FIFO. Pages som nylig er blitt brukt (lest/skrevet til) vil ikke bli
fjernet fra minnet.

### NRU

##### Not Recently Used (NRU)

  - Separate page frames into four classes
    
    0.  not referenced, not modified
    
    1.  not referenced, modified
    
    2.  referenced, not modified
    
    3.  referenced, modified

  - Evict a random page from lowest class

M-bitet nullstilles ikke ved hvert klokketikk. Dette er en veldig enkel
algoritme, men kanskje god nok. En veldig grov tilnærming til Least
Recently Used.

### LRU

##### LRU

Least recently used would replace the page that is the longest time
since it is last used, under the assumption that a recently used frame
is likely to be used again. It is difficult to implement however, but
two alternatives might be:

  - Linked lists

  - Hardware counters

LRU er en god ide, men er i praksis vanskelig å implementere. Antakelsen
om at en page det er lenge siden er brukt ikke skal brukes i nær fremtid
er som oftest en korrekt antakelse. Likeså er antakelsen om at en page
som nylig er brukt snart skal brukes igjen.

En "enkel" måte å implementere LRU vil være å vedlikeholde en linket
liste der vi legger en page fremst hver gang den blir aksessert. Når vi
da har behov for å bytte ut en page, så kan vi se hvilken page som
ligger bakerst i lista, og da er det den som det er lengst siden vi har
brukt.  
Utfordringen med en slik strategi er at det å vedlikeholde en slik liste
tar litt tid, og dersom denne skal oppdateres ved hver minneaksess vil
det fort bli uholdbart. Maskinvarestøttet implementering av en slik
liste er komplisert, og dermed ikke noe Intel/AMD har gjort.

En maskinvarestøttet implementering som teoretisk er mulig er å ha en
64-bit teller som teller opp hvert klokkeslag. Når så en minnereferanse
skjer, så kan vi oppdatere ett passende felt i paging-tabellen til å
inneholde verdien av denne telleren. Når en page-replace skal skje, kan
vi da simpelt nok bare lete etter den page-en som har lavest verdi i
dette feltet.

[LRU Page-Replacement
Algorithm](http://cs.uttyler.edu/Faculty/Rainwater/COSC3355/Animations/lrupagereplacement.htm)

### Strategier

*Demand paging* betyr at page frames allokeres ved behov istedet for at
noen allokeres på forhånd. Hvis page frames allokeres før prosessen
kjøres kalles det *prepaging*. Det settet med page’r som en prosess
holder på å bruke kalles *working set*. Hvis det ikke er nok
tilgjengelig minne for en prosess til å ha hele prosessens working set i
fysisk minne, så blir det veldig hyppige page fault, og programmet er i
en tilstand vi kaller *thrashing*.

Sjekk gjerne Microsoft teknologiene Superfetch og ReadyBoost.

    # Linux
    top
    # Windows
    vmmap
    refresh
    # høyreklikk på powershell prosessen
    empty-ws
    ls # i powershell
    refresh

### Working set

##### Size of Working Set

![image](/home/erikhje/office/kurs/opsys/08-memman-pagereplace-design-impl/tex/../../resources/MOS_3e/03-19.pdf)

Merk: på Unix/Linux kalles working set for *Resident Set Size (RSS)*.

\(w(k,t)\) er størrelsen på working set i ett gitt tidspkt \(t\) ut fra
de \(k\) siste minneaddresseringene. En viktig egenskap ved working set
er at det endres sakte med tiden. Som regel måler man ikke working set
ut fra antall minneaddresseringer tilbake i tid, men bruker CPU-tid
istedet.

##### Remember Why Cache Works

  - Locality of reference
    
      - *spatial locality*
    
      - *temporal locality*

Cache gjør at programmer kjører raskere fordi instruksjoner gjenbrukes
ofte (samlokalisert i tid) mens data ofte aksesseres blokkvis
(samlokalisert i rom, har du lest en byte skal du sikkert snart ha
byte’n ved siden av den også). Dette er logikken bak working set også:
kode og data gjenbrukes i veldig stor grad, og noen deler brukes mye
oftere enn andre.

##### Working Set Page Replacement Algorithm

![image](/home/erikhje/office/kurs/opsys/08-memman-pagereplace-design-impl/tex/../../resources/MOS_3e/03-20.pdf)

Periodisk klokkeinterrupt setter alle R-bit til 0. Current virtual time
er CPU-tid for denne prosessen.

Ved page fault oppdateres altså current virtual time for alle som har
sin R-bit satt for indikere at disse var nylig brukt, dvs en del av
working set i det page fault’n forekom.

Hvis du lurer på hvilke av disse algoritmene evn brukes ifb CPU cache,
så kikk gjerne på [What cache invalidation algorithms are used in
actual CPU
caches?](http://stackoverflow.com/questions/22597324/what-cache-invalidation-algorithms-are-used-in-actual-cpu-caches)

## Design Issues

##### Local or Global Page Replacement

![image](/home/erikhje/office/kurs/opsys/08-memman-pagereplace-design-impl/tex/../../resources/MOS_3e/03-23.pdf)

Globale algoritmer funker generelt bedre. Lokale algoritmer sliter hvis
working set endrer seg mye over tid, hvis det øker kan man få thrashing
selv om det totalt sett er ledige page’r, hvis det minker kan det hende
det sløses med minne. Uansett må det styres hvor mange page frames hver
prosess skal kunne benyttet. En grei måte å gjøre det på er å se på
*page fault frequence*.

### Page faults

##### Page Fault Rate

![image](/home/erikhje/office/kurs/opsys/08-memman-pagereplace-design-impl/tex/../../resources/MOS_3e/03-24.pdf)

A betyr for mange page faults, mens B betyr for få, dvs kanksje
prosessen har fått allokert unødvendig mye minne.

Hvordan funker dette i praksis på f.eks. Windows?
<http://channel9.msdn.com/Events/TechEd/NorthAmerica/2011/WCL406>
(Paging 23:30-34:00; SuperFetch 49:00-50:28; WorkingSet, Local Page
Replacement og LRU med aging 05:27-07:28; Copy On Write 19:30-20:32).

##### Last kontroll

Av og til er det lureste å swappe en hel prosess til disk for å frigjøre
alle den page frames. Altså god gammeldags swapping er fortsatt nyttig.
Dette må sees i sammenheng med i hvilken grad prosessene i minnet er
CPU-bound eller I/O-bound slik at man forsøker utnytte systemet optimalt
til enhver tid.

##### Page størrelse

Stor page størrelse kan føre til mye *intern fragmentering*, mens for
liten page størrelse gir store page tabeller.

(Husk: forskjell på intern fragmentering, ekstern fragmentering og data
fragmentering.)

Overhead ved paging kan beregnes vha gjennomsnittlig prosess størrelse
\(s\), page størrelse \(p\) og hver page entry trenger \(e\) bytes. I
tillegg blir det gjennomsnittlig intern fragmentering pr prosess
\(p/2\):

overhead \(= se/p + p/2\)

deriverer mhp \(p\) og setter lik 0:

\(-se/p^{2}+1/2=0\)

gir

\(p=\sqrt{2se}\)

som ved s=1MB og e=8bytes (som er typiske størrelser) gir p=4K.

### Sharing

##### Sharing Program Pages

![image](/home/erikhje/office/kurs/opsys/08-memman-pagereplace-design-impl/tex/../../resources/MOS_3e/03-26.pdf)

I praksis skjer deling av page’r svært ofte, f.eks. ved fork av en ny
prosess lages ingen ny kopi før en page modifiseres, dette er et viktig
prinsipp som kalles *copy on write*.

##### Shared Library

![image](/home/erikhje/office/kurs/opsys/08-memman-pagereplace-design-impl/tex/../../resources/MOS_3e/03-27.pdf)

Det er svært viktig å benyttet delte biblioteker fremfor å statisk
kompilere inn alt i hver enkelt executable fil. Det sparer enormt mye
diskplass og har en stor sikkerhetsmessig fordel at et bibliotek kan
oppgrades uten at alle programmene som linker til det må det. Delte
bibliotek er et særtilfelle av det mer generelle *memory-mapped filer*.

KODEEKSEMPEL (legg inn scanf for å stoppe):

[Memory Mapped Files](https://beej.us/guide/bgipc/html/multi/mmap.html)

## Implementation

### OS involvement

##### OS Involvement in Paging

  - Process creation  
    Create (allocate memory and possible also swap area) the page table
    dependent on the size of the program.

  - Process execution  
    Reset MMU, flush TLB.

  - Page fault  
    Which virtual address caused the page fault? swap page to disk and
    read needed page into the page frame.

  - Process exit  
    Release page table and page frames.

### PF handling

##### Page Fault Handling

1.  Interrupt to kernel, general registers stored away.

2.  OS finds out which virtual page is needed, checks that address is
    valid, and access (protection bits) is ok.

3.  If page frame is dirty, the page is scheduled for transfer to disk,
    process suspended until write completed.

4.  The wanted page is transferred from disk (process suspended while
    this happens).

5.  Page table is updated.

6.  Instruction causing the page fault is restarted.

7.  The process is scheduled, registers restored and return to user
    space execution.

Viktig å få med seg *If page frame is dirty...*.

## Summary

##### Har vi kontroll?

![image](/home/erikhje/office/kurs/opsys/08-memman-pagereplace-design-impl/tex/../img/virtualmemory.pdf)

En oppsummerende foil: er vi sikre på at vi skjønner dette bilde? hvis
ja, så har vi kontroll\!

Og hvorfor har vi en stor pagefile.sys igjen?

<http://support.microsoft.com/kb/2160852>  
(skumles alt)

## Theory questions

1.  Hva menes med page replacement? Beskriv hva som skjer ved denne
    aktiviteten.

2.  Hva menes med *working set* og *thrashing* i forbindelse med
    minnehåndtering?

3.  Hva er hensikten med et swapområde?

4.  Tanenbaum oppgave 3.31  
    A computer has 4 page frames. The time of loading, time of last
    access, and the R and M bits for each page are (the times are in
    clock ticks):
    
    | Page | Loaded | Last ref. | R | M |
    | :--: | :----: | :-------: | :-: | :-: |
    |  0   |  126   |    280    | 1 | 0 |
    |  1   |  230   |    265    | 0 | 1 |
    |  2   |  140   |    270    | 0 | 0 |
    |  3   |  110   |    285    | 1 | 1 |
    

    1)  Which page will NRU replace?
    
    2)  Which page will FIFO replace?
    
    3)  Which page will LRU replace?
    
    4)  Which page will second chance replace?

## Lab exercises

**Hjelp til lesbarhet: byte konvertering.**  
Skriv et script `human-readable-byte.bash` som leser et tall, antall
bytes, fra `STDIN`, konverter tallet til KB, MB, GB eller TB avhengig av
størrelsen på tallet, og skriver den nye størrelsen i heltall. Scriptet
bør oppføre seg omtrent slik:

    $ echo 25000000 | bash human-readable-bytes.bash 
    23MB
    $ echo 250 | bash human-readable-bytes.bash 
    250B

**Hjelp til lesbarhet: ns konvertering.**  
Skriv et script `human-readable-ns.bash` som leser et tall, antall ns
(nanosekunder), fra `STDIN`, konverter tallet til us (microsekunder), ms
(millisekunder) eller sek (sekunder) avhengig av størrelsen på tallet,
og skriver den nye størrelsen i heltall. Scriptet bør oppføre seg
omtrent slik:

    $ echo 25000000 | bash human-readable-ns.bash 
    25ms
    $ echo 250 | bash human-readable-ns.bash 
    250ns

**(OBLIG) Page faults** Anta at nettleseren chrome kjører. Skriv et bash
script `chromemajPF.bash` som skriver ut antall major page faults hver
av chrome sine prosesser har forårsaket. I tillegg skal scriptet skrive
ut en melding om en av chrome sine prosesser har forårsaket mer enn 1000
major page faults. Du kan finne antall major page faults for en PID med

    $ ps --no-headers -o maj_flt 4468
      113

og du kan finne chrome sine PIDs med

    $ pgrep chrome
    4377
    4390
    4468
    4479

Scriptet ditt bør oppføre seg slik:

    $ ./chromemajPF.bash
    Chrome 4377 har forårsaket   1160 major page faults (mer enn 1000!)
    Chrome 4390 har forårsaket     41 major page faults
    Chrome 4468 har forårsaket    113 major page faults
    Chrome 4479 har forårsaket     31 major page faults

**(OBLIG) En prosess sin bruk av virtuelt og fysisk minne.**  
Skriv et script `procmi.bash` som tar et sett med prosess-ID’er som
kommandolinjeargument og for hver av disse prosessene skriver til en fil
`PID-dato.meminfo` følgende info:

Total bruk av virtuelt minne (\(VmSize\))

Mengde virtuelt minne som er privat (\(VmData+VmStk+VmExe\))

Mengde virtuelt minne som er shared (\(VmLib\))

Total bruk av fysisk minne (\(VmRSS\))

Mengde fysisk minne som benyttes til page table (\(VmPTE\), som da
gjerne er multi-level, slik at det vil variere mye fra prosess til
prosess)

Hint: `man proc` og les om filen `/proc/[pid]/status`, samt se i filen
`/proc/meminfo`.  
Eksempel kjøring:

    $ bash procmi.bash 1 1985 17579
    $ ls 17579*
    17579-20100216-15:26:31.meminfo
    $ cat 17579-20100216-15\:26\:31.meminfo 
    
    ******** Minne info om prosess med PID 17579 ********
    Total bruk av virtuelt minne (VmSize):   3444KB
    Mengde privat virtuelt minne (VmData+VmStk+VmExe):   516KB
    Mengde shared virtuelt minne (VmLib):   1596KB
    Total bruk av fysisk minne (VmRSS):   1112KB
    Mengde fysisk minne som benyttes til page table (VmPTE):   32KB
    
    $ 

# Filsystemimplementasjon, EXTFS

## Files & Dirs

##### File Structure

![image](/home/erikhje/office/kurs/opsys/09-fs-impl-ext/tex/../../resources/MOS_3e/04-02.pdf)

Filer har historisk sett vært oppbygd på tre ulike måter:

1)  er en ustrukturert sekvens av bytes, det viktigste er at
    operativsystemet gir blaffen i filtyper, en fil er bare en fil. Det
    er applikasjonen som må håndtere filtyper. All windows og UNIX/Linux
    idag funker slik.

2)  benyttes ikke lenger, ble benyttet for å ha records lik
    “hullkort-bilder”

3)  benyttes på mange stormaskiner fortsatt (på IBM stormaskiner så
    kalles disse filene ett datasett)

Det vi i dag ser på som filer kan grovt deles inn i følgende typer
filer:

1.  Regular files: brukerdata, ascii eller binære (ascii benytter bare 7
    av 8 bit pr tegn, Unicode (UTF-8, UTF-16) bruker typisk 1-4 bytes.)

2.  Directories: Kataloger/mapper for å organisere andre filer.

3.  Symbolske linker (UNIX/Linux) (Symlinks/Shortcuts in Windows)

4.  Character devices (UNIX/Linux): serielle I/O enheter

5.  Block devices (UNIX/Linux): disker (alt som kan adresseres blokkvis)

Vi skiller også mellom sekvensiell og random aksess. Tape er ett
eksempel på medie der du bare leser en fil sekvensiellt. Typisk i dagens
datamaskiner med harddisker og SSD-er er mulighetene for random aksess.

##### Executable Files

![image](/home/erikhje/office/kurs/opsys/09-fs-impl-ext/tex/../../resources/MOS_3e/04-03.pdf)

(a) viser et typisk exe filformat (Dataområdet er initialiserte globale
variable, BSS er ikke initialiserte globale variable, Symboltabellen
brukes for debugging, entry point angir start for koden).

(b) viser et gammeldags arkiv (fra tiden da `ar` ble benyttet for
biblioteker i motsetning til nå hvor `tar` brukes til alt).

DEMO:

`file` (`/usr/share/file/magic`)

### Attributes

##### File Attributes

![image](/home/erikhje/office/kurs/opsys/09-fs-impl-ext/tex/../../resources/MOS_3e/04-04.pdf)

Filattributter = metadata

DEMO:

`ls -ltra`, `lsattr / chattr +i`, `getfattr`, `stat`, `getfacl`

### Operations

##### File Operations

1.  Create

2.  Delete

3.  Open

4.  Close

5.  Read

6.  Write

<!-- end list -->

7.  Append

8.  Seek

9.  Get attributes

10. Set attributes

11. Rename

Dette er de mest vanlige systemkallene knyttet til filer, men eksakt
hvilke som finnes varierer litt mellom de forskjellige
operativsystemene.

*Hvorfor gjør vi egentlig en open? dvs trenger vi å gjøre open?* Nei,
egentlig ikke, men det gjør alle påfølgende fil aksesser så mye raskere
hvis filattributtene er lastet og sjekket på forhånd, og at man får
henter inn diskaddresser i minne før man starter filaksessen.

    #include <sys/types.h>                         /* include necessary header files */
    #include <fcntl.h>
    #include <stdlib.h>
    #include <unistd.h>
    
    int main(int argc, char *argv[]);              /* ANSI prototype */
    
    #define BUF_SIZE 4096                          /* use a buffer size of 4096 bytes */
    #define OUTPUT_MODE 0700                       /* protection bits for output file */
    
    int main(int argc, char *argv[])
    {
      int in_fd, out_fd, rd_count, wt_count;
      char buffer[BUF_SIZE];
    
      if (argc != 3) exit(1);                      /* syntax error if argc is not 3 */
    
      /* Open the input file and create the output file */
      in_fd = open(argv[1], O_RDONLY);             /* open the source file */
      if (in_fd < 0) exit(2);                      /* if it cannot be opened, exit */
      out_fd = creat(argv[2], OUTPUT_MODE);        /* create the destination file */
      if (out_fd < 0) exit(3);                     /* if it cannot be created, exit */
    
      while (1) { /* Copy loop */
        rd_count = read(in_fd, buffer, BUF_SIZE);  /* read a block of data */
        if (rd_count <= 0) break;                  /* if EOF or error, exit loop */
        wt_count = write(out_fd, buffer, rd_count);/* write data */
        if (wt_count <= 0) exit(4);                /* wt count <= 0 is an error */
      }
    
      /* Close the files */
      close(in_fd);
      close(out_fd);
      if (rd_count == 0)                           /* no error on last read */
        exit(0);
      else
        exit(5);                                   /* error on last read */
    }

Merk: `in_fd` og `out_fd` er fildeskriptorer som skapes med systemkallet
`open`.

sjekk `/usr/include/bits/fcntl.h` for å se hva `O_RDONLY` betyr (greit å
vite når vi skal se inni `fdinfo` også)

merk at dette ikke er direkte systemkall men de tilhørende
C-biblioteksfunksjonene

DEMO (med sleep):

    ./cp-read-write 10MBfile a
    ls -l /proc/$(pidof cp-read-write)/fd
    cat /proc/$(pidof cp-read-write)/fdinfo/{3,4}

EXTRA DEMO:

La oss samtidig se hva som egentlig skjer når det benyttes pipes, vi så
nettopp at alle prosesser har standard fildeskriptorer 0 (STDIN), 1
(STDOUT) og 2 (STDERR), se hva som skjer med disse når vi lager en pipe:

    cat | wc -l
    ls -l /proc/$(pidof cat)/fd  # Merk rettigheten som er satt med 'w'
    ls -l /proc/$(pidof wc)/fd   # og 'r' her

##### Directories

![image](/home/erikhje/office/kurs/opsys/09-fs-impl-ext/tex/../../resources/MOS_3e/04-07.pdf)

Alle kataloger har entries for . og .. (Merk for / (rota) betyr de det
samme).

Hver prosess har en CWD (Current working directory)

DEMO:

`ls -l /proc/$(pidof bash)/`

##### Directory Operations

1.  Create

2.  Delete

3.  Opendir

4.  Closedir

<!-- end list -->

5.  Readdir

6.  Rename

7.  Link

8.  Unlink

Kommer tilbake til linking senere i shared files.

## File System Implementation

### Layout

##### File System Layout

![image](/home/erikhje/office/kurs/opsys/09-fs-impl-ext/tex/../../resources/MOS_3e/04-09.pdf)

I all hovedsak er et filsystem bare en datastruktur, samt håndteringen
av denne. Men det er en veldig interessant datastruktur siden den er i
bruk så og si hele tiden.

Se <http://en.wikipedia.org/wiki/Partition_table> og spesielt den
pågående overgangen innen teknologiene for Firmware/Partisjonstabell
(fra BIOS/MBR til UEFI/GUID).

##### File System Layout: Linux ext2

![image](/home/erikhje/office/kurs/opsys/09-fs-impl-ext/tex/../../resources/MOS_3e/10-31.pdf)

ext2 filsystemet deler inn i blokkgrupper og forsøker å dele directories
utover gruppene og la filer i samme directories være i samme gruppe (for
å minske disk arm forflytting, dvs øke ytelsen)

DEMO:

`sudo debugfs /dev/sda1` (gjør at man ser på filsystemdetaljer
read-only)  
`show_super_stats` (“Show superblock statistics”, viser innholdet i
superblokken som er kopiert inn som start på hver gruppe, og viser alle
gruppene)

Størrelse på partisjon bør kunne regnes ut fra:  
Blokkgrupper \(\times\) Blokker pr gruppe \(\times\) Blokkstørrelse  
(kontroller med å se på `/dev/sda1` med `df -h`)

Men før vi ser mere detaljer om ext filsystemet, la oss gå tilbake til
teorien et øyeblikk:

Det mest interessante spørsmålet er kanskje: *Hvilke diskblokker
tilhører hvilke filer?*

### Allocation

#### Contiguous

##### Contiguous Disk Space Allocation

![image](/home/erikhje/office/kurs/opsys/09-fs-impl-ext/tex/../../resources/MOS_3e/04-10.pdf)

Dette er altså “sammenhengende allokering”, filer lagres som *en*
sammenhengende sekvens av blokker. (Dette funker jo ikke noe særlig i
praksis i read-write filsystemer, men prinsippet tilstrebes i vanlige
filsystemer *for deler av filer*, dvs en stor fil vil i de fleste
filsystemer forsøkes lagret mest mulig samlet.)

Enkelt og rask sekvensiell lesing (trenger bare vite startblokk og
filstørrelse), men fører til mye fragmentering og det er jo umulig å
vite filstørrelser på forhånd (hvor stor er DV-fila du skal hente fra
kamera?), men brukes selvfølgelig ved read-only filsystemer hvor
filstørrelsene er kjente (f.eks. CD og DVD).

(“History repeats itself” sammenhengende lagring var mye benyttet på
magnetbånd for lenge siden, og er kommet tilbake for CDr/DVDr).

#### Linked list

##### Linked List Allocation

![image](/home/erikhje/office/kurs/opsys/09-fs-impl-ext/tex/../../resources/MOS_3e/04-11.pdf)

Problematisk med random aksess, samt at mengden data i en blokk ikke
lenger er \(2^{n}\) siden deler av blokka brukes til metadata (peker til
neste blokk). Så, la oss i stedet ta disse adressene (pekerne) og legge
de i en tabell i minnet istedet:

#### FAT

##### File Allocation Table

![image](/home/erikhje/office/kurs/opsys/09-fs-impl-ext/tex/../../resources/MOS_3e/04-12.pdf)

Filene befinner seg i fysiske blokker:

A: 4-7-2-10-12

B: 6-3-11-14

———-

FAT løser problemet med sammenhengende lister og er jo mye brukt
(FAT32).

Problemet er at størrelsen på tabellen avhenger av størrelsen på disken.
F.eks. med en 500GB disk eller partisjon, 2KB blokkstørrelse og 4B pr
entry blir tabellen \((500GB/2KB)\times 4B=1000MB\) som må være i minne
for at oppslag skal være effektive.

I tillegg er random aksess tregt siden man hele tiden må søke gjennom
blokk-kjeden (f.eks. 4-7-2-10-12 for A over) beskrevet i FAT.

#### I-node

##### Example I-node

![image](/home/erikhje/office/kurs/opsys/09-fs-impl-ext/tex/../../resources/MOS_3e/04-13.pdf)

Løser FAT-problemet med “en FAT pr åpen fil”. Trenger bare å ha i-nodene
tilsvarende de åpne filene i minne om gangen.

Hvis filen er større enn tilgjengelig antall blokkaddresser benyttes
siste blokkaddresse som en peker til et sett nye blokkaddresser (som
igjen kan benytte sin siste blokkaddresse som peker til enda et sett med
blokkaddresser osv).

### Ext2fs

##### Linux (ext2fs) I-node (128 bytes)

![image](/home/erikhje/office/kurs/opsys/09-fs-impl-ext/tex/../../resources/MOS_3e/10-33.pdf)

DEMO:

`stat a`

Blocks er antall blocks allokert for filen (IKKE hvor mange er i bruk).

IO Block er “optimal blocksize for I/O”.

##### UNIX System V File System

![image](/home/erikhje/office/kurs/opsys/09-fs-impl-ext/tex/../../resources/MOS_3e/04-34.pdf)

De 12 første diskaddressene er lagret i i-noden. For store filer må
single, double eller triple indirect block benyttes.

Anta diskblokkstørrelse på 1KB (blkstr), 4bytes diskblokkaddresser
(blkaddr).

\(12 \times 1KB = 12KB\) går i i-noden.

\(256 \times 1KB + 12KB = 268KB\) med single indirect.

\(256 \times 256 \times 1KB + 12KB = (2^{16}+12)KB\) (64MB) med double
indirect.

\(256 \times 256 \times 256 \times 1KB + 12KB = (2^{24}+12)KB\) (16GB)
med triple indirect.

  - Max partisjonsstørrelse  
    En partisjon har en 32-bits variabel “block count” som gjør at max
    partisjonsstørrelse blir 16TB (4KB blokkstørrelse:
    \(2^{12}\times 2^{32}\)) (1EB i ext4).

  - Max filstørrelse  
    Med 4KB blokkstørrelse kan man oppnå filstørrelser på 4TB
    (\(2^{10^{3}}\times 2^{2}\)KB\(=2^{32}\)KB\(=2^{42}\)B\(=4\)TB)
    (16TB i ext4), men siden i-noden inneholder en 32-bits variabel
    `i_blocks` som oppgir antall 512B-sektorer er max fil str i praksis
    2TB, se “Note:” i  
    <http://www.nongnu.org/ext2-doc/ext2.html#DEF-BLOCKS>

##### Implementing Directories

![image](/home/erikhje/office/kurs/opsys/09-fs-impl-ext/tex/../../resources/MOS_3e/04-14.pdf)

En haug med filer er ikke mye nyttig, vi må kunne strukturere dem mer
enn bare å ha dem i en stor haug.

Det finnes primært to typer directories:

1.  filnavn, addresse og alle attributter i selve directory entrien

2.  bare filnavn og peker til i-node

##### Two Ways of Handling Long File Names

![image](/home/erikhje/office/kurs/opsys/09-fs-impl-ext/tex/../../resources/MOS_3e/04-15.pdf)

To måter å håndtere lange variable filnavn på:

1.  in-line, enkelt men gir variable størrelse på dir entry (f.eks. kan
    et entry lettere ligge i skille mellom to pages og generere page
    fault)

2.  in-a-head, gir standard entry størrelse men litt mer komplisert med
    peker til filnavnet

Generelt lineært søk ikke noe problem, men store directories håndteres
med “caching og hashing”.

##### A Linux Directory with Three Files

![image](/home/erikhje/office/kurs/opsys/09-fs-impl-ext/tex/../../resources/MOS_3e/10-32.pdf)

NB\! Feil i figuren, i-node nr til voluminous skal være 42, ikke 19.

Entry size trengs hvis det er gjort noe padding (f.eks. når er fil er
slettet som i eksempel, det er dette pilene illustrerer).

ext2 benytter altså in-line filnavn men ingen attributter, alt finnes i
i-noden.

DEMO:

    sudo debugfs /dev/sda1
    stat / # finner blokknr som root dir er i, bruker den i skip:
    sudo dd if=/dev/sda1 bs=4k count=1 skip=1283 | xxd

(vi finner igjen directory navn og ser at de har variabellengde entries)

##### Key Issues in File Lookup

1.  How to get to *root directory* (`/`, `C:\`)?

2.  How are *files connected to directories*?

3.  How are *data blocks connected to files*?

4.  *Are directories implemented as files?*

—–

La oss svare på disse fire spørsmålene i EXT, FAT og NTFS.

##### Lookup: `/usr/ast/mbox`

![image](/home/erikhje/office/kurs/opsys/09-fs-impl-ext/tex/../../resources/MOS_3e/04-35.pdf)

    # ext3-image is dd of ext3 memory stick with all zeros
    # except dir structure /home/erikh/{a.txt,cf3.msi}
    # 
    ##### Find root dir
    #
    # root dir is always in inode nr 2, and this is in block group 0, and 
    # the GDT gives the datablock of the inode table (which we cant read):
    fsstat -f ext ext3-image
    #
    ##### Look in root dir's inode
    #
    # root dirs attributes:
    istat -f ext ext3-image 2
    #
    ##### Look in root dir's datablocks
    #
    # hexdump of inode 2 (root's) datablock:
    dd if=ext3-image bs=1k count=1 skip=510 | xxd
    # find inode of /home:
    ifind -f ext -n /home ext3-image
    #
    ##### Look in home dir's inode
    #
    # home dirs attributes:
    istat -f ext ext3-image 27889   # hex 6CF1, little endian?
    #
    ##### Look in home dir's datablocks
    #
    # hexdump of inode 27889 (home's) datablock:
    dd if=ext3-image bs=1k count=1 skip=117249 | xxd
    # find inode of erikh:
    ifind -f ext -n /home/erikh ext3-image
    #
    ##### Look in erikh dir's inode
    #
    # erikh dirs attributes:
    istat -f ext ext3-image 27890
    #
    ##### Look in erikh dir's datablocks
    #
    # hexdump of inode 27889 (erikh's) datablock:
    dd if=ext3-image bs=1k count=1 skip=117250 | xxd
    # find inode of cf3.msi:
    ifind -f ext -n /home/erikh/cf3.msi ext3-image
    #
    ##### Look in cf3.msi inode
    #
    # cf3.msi attributes gives me its datablocks:
    istat -f ext ext3-image 27891 | less

##### UNIX `read` System Call

`n=read(fd,buffer,nbytes);`

![image](/home/erikhje/office/kurs/opsys/09-fs-impl-ext/tex/../../resources/MOS_3e/10-34.pdf)

DEMO fra `/prod/PID/fdinfo`

### Links

##### Shared File

![image](/home/erikhje/office/kurs/opsys/09-fs-impl-ext/tex/../../resources/MOS_3e/04-16.pdf)

Ved hjelp av en link fra en av B’s kataloger til en av C’s filer er
trestrukturen nå blitt en rettet asyklisk graf (DAG - Directed Acyclic
Graph).

Både B og C kan nå dele (f.eks. editere) den samme filen.

##### Shared File

![image](/home/erikhje/office/kurs/opsys/09-fs-impl-ext/tex/../../resources/MOS_3e/04-17.pdf)

Figuren viser problemet som kan oppstå ved at C ikke får slettet sin
egen fil fordi C har gitt B tilgang til å hardlinke til den.

Symbolsk link (symlink) er en fil som ikke har noen datablokk men
i-noden inneholder stien (path) til en annen fil.

Hard link er en directory entry som peker til en eksisterende i-node.

Generelt benyttes mest symlinker siden de er lettere å forstå, ulempen
er noe ekstra overhead (jmf bruk av CNAME records i DNS istedet for
multiple A records).

DEMO:

    cat a
    ls -l a # se linkteller lik 1
    ln -s a sym
    ls -l a # ingen endring i linkteller
    ln a hard
    ls -l a # linkteller er blitt 2
    cat sym; cat hard # begge gir lik utskrift
    stat a (debugfs) # merk inode nr
    stat sym (debugfs) # merk inode nr
    stat hard (debugfs) # hvilken inode nr har denne?
    rm a
    cat sym; cat hard # bare hard gir utskrift
    stat hard (debugfs) # ingen endring, filen finnes fortsatt

God forklaring på tilsvarende konsepter i Windows/NTFS:
<http://devtidbits.com/2009/09/07/windows-file-junctions-symbolic-links-and-hard-links/>

### LFS

##### Log-Structured File Systems (LFS)

  - More CPU and memory causes mosts reads from memory cache, thus disk
    access are typically small writes

  - LFS is a way of *structuring the entire disk as a log* with the
    purpose of buffering all small writes into larger periodic segment
    writes appending the log

  - Each segment contains a segment summary, i-nodes, directories and
    data-blocks, finding i-node’s is a bit more difficult, but doable

  - An *LFS cleaner* removes redundant files and releases segments

En diskskriving på 50 \(\mu\)s er ofte foranlediget av 10ms søketid og
4ms rotasjons-delay, mao diskeffektivitet på under 1%. *Små
write-operasjoner er kostbare*.

LFS forbedrer disk ytelse med en faktor 10 ved mange små writes.

LFS er ikke mye brukt i praksis, men tanken om å skape robusthet ved å
være klar over hva som skal gjøres fremover i tid med filsystemet, er
blitt videreført inn i eksisterende filsystemer iform av “journalling”.

### JFS

##### Journaling File Systems

Example “removing a file”:

1.  Remove the file from its directory.

2.  Release the i-node to the pool of free i-nodes.

3.  Return all the disk blocks to the pool of free disk blocks.

*In the absence of system crashes, the order in which these steps are
taken does not matter; in the presence of crashes, it does.*

f.eks. hvis bare nr 2 har rukket blitt utført, kan denne i-noden
gjenbrukes og siden nr 1 ikke er blitt utført kan da en directory entry
peke på feil fil

eller

f.eks. hvis bare nr 3 har rukket blitt utført, kan to filer ende opp med
å dele de samme datablokkene.

##### Journaling File Systems

What is the difference between:

*Add newly freed blocks from i-node K to the end of the free list*

and

*Search the list of free blocks and add newly freed blocks from i-node K
to it if they are not already present*

?

##### Journaling File Systems

To make journalling work, the logged operations must be *idempotent*
(convergent).

A Journalling file system keep a log of the operations it is going to
do, so they can be redone in case of a system crash.

—–

Vi ønsker altså operasjoner \(f\) som er slik at

\(f(\mbox{feil})=\mbox{riktig}\) og \(f(\mbox{riktig})=\mbox{riktig}\)

### VFS

##### Virtual File Systems

![image](/home/erikhje/office/kurs/opsys/09-fs-impl-ext/tex/../../resources/MOS_3e/04-18.pdf)

Et OS må håndtere mange filsystemer.

I Windows gjøres dette (inspirert fra VMS) ved at man bruker
forskjellige stasjoner, hvor hver stasjon har en bokstav som navn,
f.eks. en minnepinne med FAT32 filsystemet blir montert/mappet som E:
mens selve operativsystemet er i NTFS på C:

Windows forsøker ikke integrere alle filsystemer inn i en helhet, det
gjør UNIX/Linux.

I UNIX/Linux bruker man ikke stasjoner med bokstaver men monterer/mapper
opp alle filsystemer i samme directory-struktur.

UNIX/Linux gjør dette via VFS som tar imot POSIX systemkall og
oversetter de til det som filsystemet håndterer

##### Virtual File Systems: read

![image](/home/erikhje/office/kurs/opsys/09-fs-impl-ext/tex/../../resources/MOS_3e/04-19.pdf)

Uansett hva filsystemer har av datastrukturer og metadata mappes det til
v-noder.

## Theory questions

1.  Nevn minst tre eksempler på filattributter/filmetadata.

2.  Hva menes med ekstern og intern fragmentering i forbindelse med en
    fil?

3.  Hvorfor kan filsystemet bli skadet dersom datamaskinen får en krasj
    når det ikke er et journalførende filsystem?

4.  Tanenbaum oppgave 4.11  
    One way to use contiguous allocation of the disk and not to suffer
    from holes is to compact the disk every time a file is removed.
    Since all files are contiguous, copying a file requires a seek and
    rotational delay to read the file, followed by the transfer at full
    speed. Writing the file back requires the same work. Assuming a seek
    time of 5 msec, a rotational delay of 4 msec, a transfer rate of 8
    MB/sec, and an average file size of 8 KB, how long does it take to
    read a file into main memory and then write it back to the disk at a
    new location? Using these numbers, how long would it take to compact
    half of a 16-GB disk?

5.  Tanenbaum oppgave 4.12  
    In light of the answer to the previous question, does compacting the
    disk ever make sense?

6.  Tanenbaum oppgave 4.40  
    How many disk operations are needed to fetch the i-node for the
    file  
    `/usr/ast/courses/os/handout.t`? Assume that the i-node for the root
    directory is in memory, but nothing else along the path is in
    memory. Also assume that all directories fit in one disk block.

7.  Skriv en kommandolinje i bash som teller antall PDF-filer (filer som
    heter noe med pdf) i din hjemmekatalog (home directory).

## Lab exercises

**(OBLIG) Informasjon om deler av filsystemet.**  
Skriv et script `fsinfo.bash` som tar en directory som argument og
skriver ut

Hvor stor del av partisjonen directorien befinner seg på som er full

Hvor mange filer finnes i directorien (inkl subdirectorier),
gjennomsnittlig filstørrelse, og full path til den største filen

Hvilken fil (IKKE ta med directories) har flest hardlinker til seg selv

Eksempel kjøring:

    $ bash fsinfo.bash /opsys/
    Partisjonen /opsys/ befinner seg på er 91% full
    Det finnes 1137 filer.
    Den største er /opsys/00-X/Jan2007.doc som er 14110208 (14M) stor.
    Gjennomsnittlig filstørrelse er ca 186208 bytes.
    Filen /opsys/06-filesystems/lab/c har flest hardlinks, den har 3.
    $

**Regulære uttrykk med grep.**  
Bruk `grep` til å finne i `/usr/share/dict/words` (eller en tilsvarende
ordliste du har tilgjengelig) alle ord som:

starter med en `q`

slutter med en `q`

inneholder minst to `q`’er

inneholder minst to `o`’er og starter ikke med `w` eller `a`

er 7 eller 8 tegn lange

**Regulære uttrykk anvendt på filsystemet.**  
Skriv et script `fnamecheck.bash` som tar en directory som argument og
skriver ut alle filnavn (inkl alle subdirectories) som inneholder norske
tegn og/eller mellomrom i filnavnet.

(litt moro til slutt) Les og prøv tipsene på  
<span><http://www.linuxjournal.com/content/tech-tip-use-gxmessage-displaying-gui-messages-scipts></span>

# Filsystemhåndtering og ytelse, FAT og NTFS

## File System Management and Performance

##### How Big are Files?

![image](/home/erikhje/office/kurs/opsys/10-fs-man-perf-fat-ntfs/tex/../../resources/MOS_3e/04-20.pdf)

For store blokker kaster bort plass, for små blokker kaster bort tid.

4KB blokkstørrelse dekker de fleste filer, men et annet viktig poeng fra
artikkelen bak disse dataene er at 93% av 4KB blokkene ble benyttet av
de 10% største filene, dvs mange store filer gir lite intern
fragmentering og dermed god plassutnyttelse.

UANSETT: beste blokkstørrelse vil alltid avhenge av
anvendelsen/omgivelsene, siden det er stor forskjell på diskbruk i
forskjellige miljøer og sammenhenger.

### Blocksize

##### Rate/Space Trade-Off

![image](/home/erikhje/office/kurs/opsys/10-fs-man-perf-fat-ntfs/tex/../../resources/MOS_3e/04-21.pdf)

Gitt en disk med 1MB pr spor, rotasjonstid 8,33 msec (7200rpm),
gjennomsnittlig seek time (forflytning mellom spor) 5 msec og
blokkstørrelse 4KB blir da aksesstiden for en blokk:

\(5+(8,33/2)+(4KB/1MB)\times 8,33 = 9,20\)

Mao nesten helt bestemt kun av seek time og rotasjon. *Når vi først har
funnet en blokk er det fint om det er mye data der.*

Hvis vi vet at vi vil ha en del store filer, kanskje det lønner seg å
øke blokkstørrelsen til 64KB siden det er billig med mye diskplass for
tiden. Vi må dog huske på at filer får allokert hele blokker. Dersom en
fil ikke fyller hele blokkene, vil resten ikke blitt brukt, og intern
fragmentering oppstår.

##### Free Blocks

![image](/home/erikhje/office/kurs/opsys/10-fs-man-perf-fat-ntfs/tex/../../resources/MOS_3e/04-22.pdf)

Med 1KB blokkstørrelse og 32 bits (4 bytes) blokkaddresser, er det plass
til 256 blokkaddresser i hver blokk (1KB/4bytes). En av de 256 brukes
som peker til neste. En 16GB disk/partisjon trenger en freelist på max
65793:

Ant. diskblokker: \(2^{4}\) (16) \(\times2^{30}\) (1GB) \(/2^{10}\)
(1KB) \(=2^{24}\)

Ant. til freelist: \(2^{24}/255=65793\) (mao freelist kan oppta ca
*64MB*)

Bitmap løsningen krever til \(2^{24}\) diskblokker \(2^{24}\) bit som er
*2MB* (\(2^{21}\)bytes), fordi bitmap krever kun en bit pr blokk mens
freelist må ha en 32 bits addresse pr blokk.

Freelist kan kreve mindre plass ved å lagre sammenhengende sekvenser av
blokker istedet for hver enkelt blokkaddresse for ledige blokker, men
det er ikke nødvendigvis plassbesparende (tenk over hvordan det vil se
ut for en veldig fragmentert disk).

### Consistency

##### File System Consistency

![image](/home/erikhje/office/kurs/opsys/10-fs-man-perf-fat-ntfs/tex/../../resources/MOS_3e/04-27.pdf)

Ikke noe stort problem lenger pga journalling, men kan fortsatt dukke
opp, konsistenthet sjekkes med `fsck` på Linux og `scandisk` på windows.

Kan sjekke konsistenthet på

1.  blokker: tabell over freelist-forekomster og fil-tilhørighet for
    alle blokker
    
    1.  er ok.
    
    2.  2 er missing block, kaster bort plass, så den legges til
        freelist.
    
    3.  blokk 4 er registrert to ganger i free list, heller ikke noe
        problem, bare fjern den ene entrien i freelist.
    
    4.  blokk 5 tilhører to filer (\!), ikke bra, lag en kopi og gi den
        til den ene filen, uansett er minst en av filene tukla til

2.  filer: sammenlikn antall directory entries for hver fil med
    linktelleren i filens i-node
    
    1.  hvis flest antall i linktelleren, litt plass-sløsing, linkteller
        oppdateres
    
    2.  hvis flest directory entries, ILLE (fordi man sletter en fil i
        det dens linkteller blir 0, så dermed kan en fil slettes selv om
        en direntry fortsatt peker til den), linkteller oppdateres

### Caching

##### Caching

![image](/home/erikhje/office/kurs/opsys/10-fs-man-perf-fat-ntfs/tex/../../resources/MOS_3e/04-28.pdf)

Figuren viser hvordan buffer cache’n (jmf figur om VFS på slutten av
forrige kapittel) er organisert og benytter LRU-prinsippet (jmf Page
Replacement algoritmene).

Buffer cache er altså en generell cache som legger seg rett over disken
og bare bryr seg om blokker som benyttes ofte (på samme måte som page
replacement algoritmene i kapittel 3). Buffer cachen bruker gjerne LRU
(Least Recently Used) men tar i tillegg høyde for sannsynligeten for at
blokken trengs igjen snart (f.eks.en nesten full datablokk trengs nok
igjen snart), og i hvilken grad en blokk er kritisk for filsystemets
konsistenthet (f.eks. en modifisert i-node).

Windows benyttet opprinnelig *write-through caches* (dirty blocks
skrives alltid umiddelbart tilbake til disk) som er det som gjorde at
disketter alltid var mye lettere å håndtere på windows enn på UNIX/Linux
(som måtte kjøre `sync` først)

Block Read Ahead (OSet ser på access patterns for å finne filer som er
sekvensiell i aksess og dermed kan OSet gjøre block read ahead)

### Cyl. Groups

##### Cylinder Groups/Block Groups

![image](/home/erikhje/office/kurs/opsys/10-fs-man-perf-fat-ntfs/tex/../../resources/MOS_3e/04-29.pdf)

Cylinder groups er ideen bak block groups som vi har studert på ext
filsystemet på linux.

DEMO:

kopier og slett store filer på minnepinne og se på egenskapene i windows
(med defragmenteringsverktøyet)

## FAT12/16/32

##### FAT12/16/32 Overview

  - Probably the most widely used

  - A very simple file system (simple is good\! KISS\!)

  - No i-nodes with the files or i-node pointer in directory entry, all
    information in the directory entry and the FAT

  - Blocks are called clusters

TAVLE:

``` 
 ---------------------------------------------------------------------
| VolumeID | Reserved sectors | FAT1 | FAT2 | Files and directories...
 ---------------------------------------------------------------------
```

VolumeID gir blant annet info om Bytes pr sector, Sectors pr cluster,
Number of FATs (alltid 2), Sectors pr FAT, Første cluster til Root
directory (som regel rett etter FAT2)

  - Max partisjonsstørrelse  
    FAT32 bruker en 32-bits sector-count (i VolumeID, dvs i boot
    sektor), og siden industri-standard sektorstørrelse er 512 byte,
    gjør dette at max partisjonsstørrelse blir 2TB
    (\(2^{9}\)B\(\times 2^{32}=2^{41}=2\)TB).

  - Max filstørrelse  
    Hver direntry bruker et 32-bits felt for å lagre filstørrelse,
    dermed er max filstørrelse 4GB.

(merk: dette er de teoretiske størrelsene, det kan hende verktøyet man
bruker for å lage en partisjon har bestemt seg for at det skal sette
ytterligere begrensninger).

DEMO:

`sudo dd if=/dev/sdb1 bs=512 count=1 skip=0 | xxd` og finn ihht figur 4
<http://www.pjrc.com/tech/8051/ide/fat32.html>

bruk <http://www.statman.info/conversions/hexadecimal.html> for å vise
at 200 blir 512 og at 10 blir 16, derav blokkstørrelse 8KB

vis <http://technet.microsoft.com/en-us/library/cc776720.aspx> for å
bekrefte

### Dir. entry

##### The Directory Entry

![image](/home/erikhje/office/kurs/opsys/10-fs-man-perf-fat-ntfs/tex/../../resources/MOS_3e/04-31.pdf)

Directory entry inneholder altså noe av det samme som i-noder gjør.

Lange filnavn skjer ved at det gis korte filnavn hvor byte 7 og 8 er  1
el. høyere inntil unikt og flere directory entries benyttes til å holde
lange filnavn (siden rootdir er begrenset til 512 entries (i FAT16) bør
ikke lange filnavn forekomme i rootdir).

MERK: Ingen representasjon av EIER av fila, mao ingen sikkerhet\!

### The FAT

##### The FAT

![image](/home/erikhje/office/kurs/opsys/10-fs-man-perf-fat-ntfs/tex/../../resources/MOS_3e/04-12.pdf)

Vi husker denne fra tidligere. Filer knyttes altså mot datablokker
(clusters) gjennom directory entrien og deretter FAT-tabellen istedet
for alle adressene i i-noden (og tilhørende single/double/triple
indirect adresseblokker).

Nice description of FAT filesystem is  
<http://www.win.tue.nl/~aeb/linux/fs/fat/fat.html>

    # vfat-image is dd of vfat memory stick with all zeros
    # except dir structure /home/erikh/{a.txt,cf3.msi}
    #
    ##### Find root dir
    #
    # in the VolumeID bytes 11 and 12 gives sector size (litte endian)
    dd if=vfat-image bs=1 count=2 skip=11 | xxd
    # hex 200 is decimal 512
    # and byte 13 is number of sectors per cluster (cluster=block)
    dd if=vfat-image bs=1 count=1 skip=13 | xxd
    # fsstat formats all this VolumeID (boot sector) info for us: 
    fsstat -f fat vfat-image
    # 248 blocks in FAT because: 248x512B/16b~64K (x2KB=128MB)
    # note: in FAT16 root dir is in a known location so no info
    # in VolumeID about this (this is different in FAT32) 
    #
    ##### Look in root dir
    #
    # note: root dir is not part of FAT
    # root dir says home starts in FAT entry 4 (bytes 26-27 says 0300)
    dd if=vfat-image bs=512 count=1 skip=497
    #
    ##### Look in FAT for additional entries (datablocks)
    #
    # FAT entry 4 says no more FAT entries (ffff)
    dd if=vfat-image bs=512 count=1 skip=1 | xxd
    #
    ##### Look in datablock for directory content
    #
    # FAT entry 4 is sector 529+4 which says erikh is FAT entry 5
    # (remember that first two FAT entries are not in use)
    dd if=vfat-image bs=512 count=1 skip=533 | xxd
    #
    ##### Look in FAT for additional entries (datablocks)
    #
    # FAT entry 5 says no more FAT entries (ffff)
    dd if=vfat-image bs=512 count=1 skip=1 | xxd
    #
    ##### Look in datablock for directory content
    #
    # FAT entry 5 is sector 529+8 which says cf3.msi is FAT entry 6
    # and a.txt is FAT entry 3254 (0cb6)
    dd if=vfat-image bs=512 count=1 skip=537 | xxd
    #
    ##### Look in FAT for additional entries (datablocks)
    #
    # FAT entry 6 says next is entry 7, 8, 9, etc
    dd if=vfat-image bs=512 count=1 skip=1 | xxd
    # (we do not look in datablocks since cf3.msi a binary file)
    # FAT entry 3254 says no more FAT entries (ffff)
    # (3254 DIV (512B/16b=256) = 12
    dd if=vfat-image bs=512 count=1 skip=13 | xxd
    #
    ##### Look in datablock for file content
    #
    # FAT entry 3254 is sector 529+((3254-2)*4)=13537
    dd if=vfat-image bs=512 count=1 skip=13537 | xxd

##### Partition Sizes

![image](/home/erikhje/office/kurs/opsys/10-fs-man-perf-fat-ntfs/tex/../../resources/MOS_3e/04-32.pdf)

## NTFS

##### NTFS

  - Advanced file system with many features (compression and encryption)

  - Solves the problem with lack of security in MS-DOS/FAT

TAVLE:

    +------------------------------------------
    | Boot sector | MFT | Systemfiler | Data...
    +------------------------------------------

  - Max partisjonsstørrelse  
    \(2^{64}\)B\(=16\)EB

  - Max filstørrelse  
    \(2^{64}\)B\(=16\)EB

### MFT

##### Master File Table (MFT)

![image](/home/erikhje/office/kurs/opsys/10-fs-man-perf-fat-ntfs/tex/../../resources/MOS_3e/11-41.pdf)

MFT er en sekvens med 1KB records.

En MFT record representerer en fil eller directory.

Hver MFT record er en sekvens med (attribute header, value) par hvor
attribute header sier hva slags attribute det er og hvor stor den er.

Den største attribute er som regel datastrømmen (som det kan være flere
av, men det er lite brukt).

Hvis datastrømmen får plass i MFT record’n vil det si at hele fila er i
MFT recorden, det kalles en *resident* eller *immediate* attribute/fil.

Demo: kjør PowerShell som Administrator: `ntfsinfo C:`

### Records

##### Attributes in MFT records

![image](/home/erikhje/office/kurs/opsys/10-fs-man-perf-fat-ntfs/tex/../../resources/MOS_3e/11-42.pdf)

NTFS definerer 13 attributes.

Standard info er alt som trengs av POSIX bl.a.

Attribute list sier hvilke andre MFT records denne filen benytter.

##### A MFT Record for a Small File

![image](/home/erikhje/office/kurs/opsys/10-fs-man-perf-fat-ntfs/tex/../../resources/MOS_3e/11-43.pdf)

Diskblokker allokeres sammenhengende hvis mulig, og beskrives med
(startblokk, lengde) par etterhverandre i MFT data attributtet.

##### MFT Records for a Big File

![image](/home/erikhje/office/kurs/opsys/10-fs-man-perf-fat-ntfs/tex/../../resources/MOS_3e/11-44.pdf)

En stor fil benytter mange MFT records for å liste opp alle diskblokker
den benytter.

##### MFT Record for a Small Directory

![image](/home/erikhje/office/kurs/opsys/10-fs-man-perf-fat-ntfs/tex/../../resources/MOS_3e/11-45.pdf)

Små directories er i all hovedsak bare et sett med (filnavn,MFTindex,++)
i en MFT record. Store directories derimot benytter datablokker og
organiseres som et B+ tree for at søket skal være mer effektivt.

    # ntfs-image is dd of ntfs memory stick with all zeros
    # except dir structure /home/erikh/{a.txt,cf3.msi}
    #
    ##### Find root dir
    #
    # the boot sector gives the first cluster (block) of the MFT file, 
    # and root dir is always in MFT entry nr 5:
    fsstat -f ntfs ntfs-image 
    #
    ##### Look in root dir's MFT entry 
    #
    # root dirs attributes, we want datablocks of $INDEX_ALLOCATION
    istat -f ntfs ntfs-image 5
    #
    ##### Look in root dir's datablocks
    #
    # root dirs datablocks gives me MFT nr of subdir home
    dd if=ntfs-image bs=4k count=1 skip=3981 | xxd 
    # search for MFT entry of home to confirm:
    ifind -f ntfs -n home ntfs-image
    #
    ##### Look in home dir's MFT entry 
    #
    # home dirs attributes, we want datablocks of $INDEX_ALLOCATION
    istat -f ntfs ntfs-image 64
    #
    ##### Look in home dir's MFT entry again: RESIDENT FILE (no datablocks) 
    #
    # no, wait, there's only the resident $INDEX_ROOT attribute
    icat -f ntfs ntfs-image 64-144-2 | xxd
    # we can also verify this be looking in the datablocks of the MFT file
    istat -f ntfs ntfs-image 0
    # and since blocksize is 4KB and each entry is 1KB, entry 64 is approx
    dd if=ntfs-image bs=4k count=1 skip=20 | xxd
    # which gives me MFT nr of subdir erikh
    ifind -f ntfs -n home/erikh ntfs-image
    #
    ##### Look in erikh dir's MFT entry 
    #
    # erikh dirs attributes, we want datablocks of $INDEX_ALLOCATION:
    istat -f ntfs ntfs-image 65
    #
    ##### Look in erikh dir's MFT entry again: RESIDENT FILE (no datablocks) 
    #
    # there's only the resident $INDEX_ROOT attribute here as well:
    icat -f ntfs ntfs-image 65-144-2 | xxd
    # which gives me the MFT nr of files a.txt and cf3.msi:
    ifind -f ntfs -n home/erikh/a.txt ntfs-image
    ifind -f ntfs -n home/erikh/cf3.msi ntfs-image 
    #
    ##### Look in a.txt MFT entry (RESIDENT FILE) 
    #
    # a.txt is a very small file which fits in the MFT 
    # (resident $DATA attribute, called "an immediate file")
    istat -f ntfs ntfs-image 66
    icat -f ntfs ntfs-image 66-128-2
    #
    ##### Look in cf3.msi MFT entry 
    #
    # cf3.msi attributes gives me its datablocks (stored as 'data runs'):
    istat -f ntfs ntfs-image 35

## Comparison

##### Comparison

<http://en.wikipedia.org/wiki/Comparison_of_file_systems>

|               |  **Ext3**  |    **FAT**    |    **NTFS** |
| :------------ | :--------: | :-----------: | ----------: |
| Max file size |    2TB     |      4GB      |        16EB |
| Max vol size  |    16TB    |      2TB      |        16EB |
| Attr location |   I-node   |   Dir entry   |   MFT entry |
| Allocation    |   Table    | “Linked-list” |       Table |
| Owner/perms   |    Yes     |      No       |         Yes |
| Speed         | B dir tree |   Table dir   | B+ dir tree |

## Storage and Testing

##### IOPS (Input/output Operations Per Second)

  - Many storage layers:  
    <span><https://www.usenix.org/legacy/event/wiov11/tech/final_files/Hildebrand.pdf></span>

  - How many IOPS do I get??? *hard to answer...*

  - Some ryggrad-rules-of-thumb
    
      - RAM: `500K +`
    
      - SSD: `10K +`
    
      - HDD: `100-200`

##### “Simplest” test: Sequential read

Read directly from block device (if possible): `hdparm -Tt <block
device>`

Read from block device or big file from filesystem:

    sync;echo 3 > /proc/sys/vm/drop_caches
    fio --filename=BIGFILEorBLOCKDEVICE \
     --direct=1 --rw=read \
     --refill_buffers --ioengine=libaio \
     --bs=4k --iodepth=16 --numjobs=4 \
     --runtime=10 --group_reporting \
     --norandommap --ramp_time=5 \
     --name=seqread

##### “Worst” test: Random write

    fio --filename=BIGFILEorBLOCKDEVICE \
     --direct=1 \
     --rw=randwrite --refill_buffers \
     --ioengine=libaio --bs=4k \
     --iodepth=16 --numjobs=4 \
     --runtime=10 --group_reporting \
     --norandommap --ramp_time=5 \
     --name=randomwrite
    # and/or --sync=1, see man 2 open, man fio

##### “Real-life” test: Random read/write mix

<span><http://www.storagereview.com/fio_flexible_i_o_tester_synthetic_benchmark></span>

    sync;echo 3 | tee /proc/sys/vm/drop_caches
    fio --filename=BIGFILE --direct=1 \
     --rw=randrw --refill_buffers --norandommap \
     --randrepeat=0 --ioengine=libaio --bs=8k \
     --rwmixread=70 --iodepth=16 \
     --unified_rw_reporting=1 --numjobs=16 \
     --runtime=60 --group_reporting \
     --name=rwmix

See also Anandtech’s use of `iometer` (for Windows)

    # Lage ramdisk, kan gjøres som tmpfs på de fleste Linux-distroer
    # https://www.kernel.org/doc/Documentation/filesystems/tmpfs.txt
    mkdir mytmpfs
    mount -t tmpfs -o size=2G tmpfs mytmpfs
    # copy some big file (e.g. this is a 1.5GB iso image)
    cp /home/archive/iso-etc/xenial-desktop-amd64.iso mytmpfs/
    # run test
    fio --filename=/home/erikh/tmp/mytmpfs/xenial-desktop-amd64.iso --rw=randwrite --refill_buffers --ioengine=libaio --bs=4k --iodepth=16 --numjobs=4 --runtime=10 --group_reporting --norandommap --ramp_time=5 --name=mytest

## Theory questions

1.  Hvor store diskblokkadresser har vi i windows filsystemet NTFS?

2.  Hva forståes med MFT i NTFS filsystemet?

3.  Når du slår opp på en directory entry for en fil i et FAT filsystem,
    så finner du addressen til første datablokk/cluster. Forklar hvordan
    du finner de resterende datablokkene/clusterene.

4.  Hva beskriver enheten IOPS? Er antall IOPS en disk kan behandle et
    godt tall på denne diskens ytelse?

5.  Hva er grunnen til at man gjerne får høyere ytelsestall om en tester
    en blockdevice direkte enn om man tester mot en fil som ligger på
    denne blockdevicen?

6.  Hvordan kan caching gjøre at lesing fra en harddisk går mye raskere?
    Hvor er cache for filsystem implementert?

7.  Tanenbaum oppgave 4.4  
    Is the `open` system call in UNIX absolutely essential? What would
    the consequences be of not having it?

## Lab exercises

1)  Bare fortsett på forrige ukes oppgaver...

# I/O

## I/O Hardware

##### I/O Hardware Devices

![image](/home/erikhje/office/kurs/opsys/11-io/tex/../../resources/MOS_4e/5-1.pdf)

Det er viktig å forsøke gruppere I/O enheter så godt man kan slik at man
ha færrest mulig grensesnitt mot dem, og standardisere så mye man kan.
To hovedkategorier:

  - Blokkbaserte enheter  
    disk, dvd, minnepinne, alt som kan addresseres som blokker av data,
    f.eks. 512 bytes eller 32kB

  - Characterbaserte enheter  
    mus, skriver, lydkort, nettverkskort, bare en strøm av
    enkelt-characters, ikke addresserbare

Det finnes enheter som ikke passer inn, f.eks. klokker som bare
genererer interrupt med jevne mellomrom.

### Isolated vs Memory-mapped

##### Isolated or Memory-Mapped I/O

![image](/home/erikhje/office/kurs/opsys/11-io/tex/../../resources/MOS_3e/05-02.pdf)

I/O enheter har som regel kontroll-, status- og dataregistre
(databuffere).

Opprinnelig hadde vi bare Isolated I/O som i figur (a), dvs man
kommuniserte med I/O enheter via I/O porter (typisk et 16-bits tall slik
som TCP porter).

Etterhvert begynte man benytte også memory-mapped I/O (b) som vil si at
man istedet bruker en del av minne til å prate mot I/O registerne. Da
slipper man egne instruksjoner for I/O-portene, man bare bruker de samme
instruksjonene som man ellers bruker mot minne. Ulempen er at det spiser
litt av minneområde, samt evn problemer med caching (dvs skal en prosess
lese av et statusregister på en I/O enhet blir det bare tull hvis denne
infoen er cachet).

Intel benytter som ofterst en hybrid modell hvor databufferene er
memory-mapped mens de andre registerne er har isolated I/O (dvs I/O
porter).

### DMA

##### Direct Memory Access

![image](/home/erikhje/office/kurs/opsys/11-io/tex/../../resources/MOS_3e/05-04.pdf)

Vi husker hvordan DMA funker fra tidligere.

### Precise vs Imprecise Interrupts

##### Interrupts

![image](/home/erikhje/office/kurs/opsys/11-io/tex/../../resources/MOS_3e/05-05.pdf)

Og vi husker hvordan interrupt funker fra tidligere.

Men det fakta at vi har pipelining, superskalar prosessor og
mikrooperasjoner (dvs instruksjoner dekomponert i mikrooperasjoner)
kompliserer bildet vårt.

Bla tilbake til figur 1.7 side 22 i tanenbaum.

##### Precise vs Imprecise interrupts

![image](/home/erikhje/office/kurs/opsys/11-io/tex/../../resources/MOS_3e/05-06.pdf)

(a) viser et presist interrupt som er definert ut fra fire kriterier:

1.  PC lagres på et kjent sted

2.  Alle instruksjoner før PC er ferdig

3.  Ingen instruksjoner etter PC er kjørt

4.  Kjent tilstand for PC-instruksjonen

(b) viser et upresist interrupt, dvs poenget her er at det er lite
sammenheng mellom PC og hvilken tilstand instruksjonene rundt PC
befinner seg i.

Upresise interrupt gjør livet vanskelig for operativsystemet (en masse
informasjon lagres på stacken og OSet må finne ut av den), mens presise
interrupt krever en komplisert interruptlogikk og CPUn må logge masse
info løpende for å kunne genere presise interrupt, dette skaper overhead
som reduserer ytelsen.

Arktikturen tilbyr gjerne begge deler, dvs f.eks. muligheten til å sette
et kontrollbit som sørger for at presise interrupts genereres.

Uansett må vi husker at interrupts er kostbare, de spiser mye CPU-tid.

## I/O Software

##### Example: Printing a String

![image](/home/erikhje/office/kurs/opsys/11-io/tex/../../resources/MOS_3e/05-07.pdf)

I/O kan gjøres på tre forskjellige metoder:

1.  Programmert I/O

2.  Interrupt-basert I/O

3.  DMA

(vi husker dette fra første intro-temaet)

### Three ways

Merk: dette er bare en utdyping av de tre metodene for I/O som vi
introduserte i kapittelet om datamaskinarkitektur.

##### Programmed I/O

![image](/home/erikhje/office/kurs/opsys/11-io/tex/../../resources/MOS_3e/05-08.pdf)

Bare les kodekommentarene til høyre.

Problemet er “loop until ready”, dette er busy waiting og er dumt hvis
CPUn har noe annet å gjøre (men ikke nødvendigvis dumt hvis den ikke har
noe annet å gjøre som ved et embedded system, heller ikke dumt hvis
skriveren har et stort buffer).

##### Interrupt-Driven I/O

![image](/home/erikhje/office/kurs/opsys/11-io/tex/../../resources/MOS_3e/05-09.pdf)

(a) er koden som starter utskriften og sørger for utskrift at første
tegn, men (b) er interrupt service procedure som skriver ut resten av
tegnene (et tegn for hvert interrupt). Poenget er at scheduleren kan
kjøre mellom hvert tegn slik at andre prosesser kan kjøre da. Når det
ikke er flere tegn å skrive ut så vil brukerprosessen ikke lenger
blokkeres.

##### I/O Using DMA

![image](/home/erikhje/office/kurs/opsys/11-io/tex/../../resources/MOS_3e/05-10.pdf)

Med DMA så settes også jobben bort på samme måte som ved
interrupt-drevet I/O men forskjellen er at nå belastes ikke CPUn for
hvert tegn, DMA kontrolleren gjør alt selv.

Dvs, ved DMA så blir det bare et interrupt for utskrift av hele bufferet
imotsetning til interrupt-drevet I/O som genererer et interrupt for
hvert tegn som blir skrevet ut.

## I/O Software Layers

##### Drivers location

![image](/home/erikhje/office/kurs/opsys/11-io/tex/../../resources/MOS_3e/05-12.pdf)

##### Layers

![image](/home/erikhje/office/kurs/opsys/11-io/tex/../../resources/MOS_3e/05-17.pdf)

  - Hardware  
    selve enheten (består typisk av kontroller og media) som utfører I/O
    operasjonen.

  - Interrupt handler  
    sørger for å vekke device driver som er blokkert mens I/O
    operasjonen pågår.

  - Device driver  
    skriver til I/O-enhetens registrere for å styre den, samt leser av
    status fra disse registerne, sjekker input parameterne som den får.
    De fleste drivere er spesielle for den type hardware de støtter, dvs
    de er skrevet av leverandøren, mens andre er generelle, f.eks en
    driver for alle SCSI disker.

  - Device-independent software  
    alt som skal gjøres for I/O enheter som ikke er spesifikt for
    bestemte enheter kan samles i uavhengig software i OSet. F.eks.
    navngivning: hva skal enheten hete? /dev/sda eller /dev/hda, skal
    lydkortet knyttes til /dev/sound osv. Aksess kontroll (dvs
    beskyttelse) av enhetene er også uavhengig av type enhet og som
    regel er oppgaver som buffring av data, feilhåndtering, allokering
    og release (spesielt for alle enheter som bare kan brukes av en
    prosess om gangen, som f.eks. en CD-brenner), samt sørge for
    blokkstørrelse-uavhengighet.

  - User process  
    den som gjør I/O forespørsel (som regel via et systemkall),
    formaterer I/O, spooling (f.eks. håndterer en kø av printjobber).

## Disks

### HDD

##### Disk Parameters

![image](/home/erikhje/office/kurs/opsys/11-io/tex/../../resources/MOS_4e/5-18.pdf)

Det som er viktig å være klar over er at med flashminne eller SSD (solid
state drive) lagringsmedia så er det ingen mekanisk lesearm, så
søketiden blir praktisk talt borte. MEN magnetiske disker med lesearm
vil være med oss i minst 10 år til og kanskje enda lenger. Så det er
fortsatt viktig å kjenne til teknologien for magnetiske disker. På
servere i dag benyttes stort sett SAS disker, se

<http://en.wikipedia.org/wiki/List_of_device_bandwidths#Storage>

Ift figuren merk at seek time er 7 ganger bedre, transfer rate er 1300
ganger bedre, mens kapasiteten er 50000 ganger bedre, mao det blir
ganske tydelig at flaskehalsen er seek time.

##### Disk Terminology

![image](/home/erikhje/office/kurs/opsys/11-io/tex/../../resources/MOS_3e/05-19.pdf)

  - Head (lese-/skrive-hode)  
    en disk har et antall plater/platesider som kan leses av et
    lese/skrivehode, eller lese/skrivearm om du vil, en disk har typisk
    mellom 1 og 16.

  - Sylinder and Track (spor)  
    hver plate er delt inn i spor (en runde på plata) typisk noen
    ti-tusener. Alle sporene vertikalt rett ovenfor hverandre danner en
    sylinder.

  - Sektor  
    hvert spor er delt inn i sektorer, typisk mellom ni og noen hundre.

  - Sone  
    siden en disk har plass til flere sektorer jo lengre ut mot kanten
    av platen man er så deles disken inn i soner som vist i figur a.
    Kontrolleren tar seg av å skjule denne fysiske geometrien på disken
    og presenterer heller den virtuelle geometrien vist i figur (b).
    Merk at det er like mange sektorer tilsammen i den virtuelle som i
    den fysiske.

Opprinnelig var max verdiene på IBM PC for sylinder, head og sektor
65535, 16, og 63, mens nå har man droppet å addresse slik og addresserer
heller løpende lineært fra null og opptil så mange sektorer som fins.
Dette kalles LBA (*Logical Block Addressing*).

### SSD

##### Solid-State Drive

![image](/home/erikhje/office/kurs/opsys/11-io/tex/../img/ssd.pdf)

Figuren: “Det viktigste” å vite om SSD disker (i tillegg til å vite at
de ikke har noen mekanisk bevegelige deler) er at de kan bare lese og
skrive page’r (dvs man kan ikke skrive mindre enn en page), og kan bare
slette blokker (dette pga de fysiske egenskapene til dette
lagringsmediet, noe med at det trengs sterkere spenning for å blanke ut
celler og da må dette gjøres på en større gruppe celler om gangen), og
*SSD disker kan ikke overskrive page’r direkte, de må slette innholdet i
en page (og dermed en hel blokk) før de kan skrive en til page*.

SSD disker er som regel laget av NAND flash IC’r som i lese/skrive
hastighet er midt mellom RAM og magnetisk disk (dvs i motsetning til RAM
så mister ikke NAND flash data når strømmen blir borte, samtidig er NAND
flash mye raskere enn magnetisk disk).

gå gjennom <http://www.anandtech.com/printarticle.aspx?i=3531> i pdf
form ihht sidene:

  - tabeller side 7  
    MERK: hovedpoenget med SSD er at random read/write kan bli minst en
    størrelsesorden bedre enn HDD grunnet at det ikke er en mekaniske
    lesearm som skal flyttes lenger

  - øverste figur side 11  
    forskjell på SLC og MLC er altså bare på celle nivå (MLC celler kan
    overskrives minst 10.000 ganger mens SLC celler kan overskrives
    minst 100.000 ganger). Det finnes også celler som kan overskrives
    enda færre ganger i og med at de lagrer flere enn to bit pr celle.

  - avsnitt side 13  
    “the real performance...” (parallell aksess skaper hastighet: det er
    dette som gjør at SSD’r kan bli så mye raskere enn vanlig
    minnepinner) og “Now you can read...” (du kan lese og skrive page’r
    MEN for å overskrive en page MÅ DU SLETTE HELE BLOCK SOM DEN PAGE
    TILHØRER)

  - tabell side 14  
    merk hvordan overskriving skjer på SSD, og at file delete normalt
    ikke kommuniserer til disken, husk det er bare noe som gjøres i
    filsystemet, f.eks. å fjerne en dir-entry

  - figurer side 15-18  
    gå gjennom eksempelet som viser nettopp hvordan dette skjer i
    praksis og hvorfor en overskriving tar i dette tilfelle 26sec
    istedet for 12sec (merk: krysset over page’n som ikke er i bruk
    lenger, dvs “invalid” merke er ikke noe som finnes i virkeligheten
    på SSD’n, den vet ikke hva som kan slette før OSet gir den beskjed
    om at en page skal overskrives) (LES NEDERST SIDE 18 HELE og TOPP
    SIDE 19)

  - figur og avsnitt side 23  
    “The TRIM command forces the block to be cleaned before our final
    write. There’s additional overhead but it happens **after a delete
    and not during a critical write**”

  - øverste figur side 48  
    her ser vi hvordan SSD fullstendig dominerer over HDD ved random
    read ytelse

  - figur og tabell side 49  
    her ser vi hvor stor forskjell det kan være på random write, dvs SSD
    som er optimalisert for sekvensiell read/write, takler dårlig random
    write (NOE SOM ER ILLE FOR OSS VANLIGE BRUKERE FORDI VANLIG PC BRUK
    ER MEST RANDOM READ/WRITE)

  - figure side 58  
    her ser vi hvor stor reell merkbar ytelsesforskjell vi typisk kan
    oppleve ved overgang til en SSD, dette er en boot med påfølgende
    lasting av et sett tunge applikasjoner

##### Disk Actions: HDD vs SSD

![image](/home/erikhje/office/kurs/opsys/11-io/tex/../img/disk-actions.png)

*The TRIM command let’s the SSD do erasures after file deletions instead
of before overwrites*

##### Important SSD Concept

  - Write amplification  
    read-erase-modify-write cycle causes extra writing (also due to wear
    levelling)

  - Wear levelling  
    distribute erasures and re-writes evenly across the media

##### Write Amp. and Wear Levelling

![image](/home/erikhje/office/kurs/opsys/11-io/tex/../img/garbagecollection.png)
<span><http://anandtech.com/storage/showdoc.aspx?i=3631></span>

Vi prater ofte om en “write amplification factor” som bør være så nære 1
som mulig, dvs hvis den er 1.1 betyr det at skriving a 1MB betyr i
gjennomsnitt at 1.1MB blir skrevet til disk i praksis. Merk altså at en
blokk med minst data i seg velges for å skrives sammen med den
innkommende write-request slik at dataene fra denne blokka kombineres
med de nye dataene i en ny blokk og den gamle blokka slettes og legges
til gruppen med ledige blokker.

Merk: wear levelling kan også gjøres i software (ref vår lagdelte IO
modell, dette kan gjøres enten i driveren eller på kontrollern) se siste
paragraf om “special-purpose file systems” på  
<http://en.wikipedia.org/wiki/Wear_levelling>

Merk: Filsystemer som skriver veldig ofte til et bestemt sted på disken
(f.eks. journalling eller FAT-tabellen) høres ut som et mareritt for SSD
disker siden de 10.000 gangene dette stedet på disken kan skrives til
burde bli brukt opp fort, men dette er altså ikke noe problem grunnet
wear levelling.

*Husk at filsystemet forholder seg til et LBA addresserom, dvs
filsystemet bare addresserer blokker lineært fra 0,1,osv mens i praksis
vil altså f.eks. blokk 1 bli flyttet rundt på SSD’n av SSD kontrollern,
men dette er usynlig for filsystemet. SSD kontrollern er snitt mot
filsystemet og presenterer det med samme addresser hver gang. Nok en
gang: ABSTRAKSJON OG LAGDELING LØSER ALLE PROBLEMER* `:)`

##### SSD: OS Consequences

  - Classical defragmentation destroys more than it helps

  - FS-Page/Block/Sector alignment with SSD-Pages

  - Write’s are bad (maybe disable journalling and writing of access
    time?)

  - *Support the TRIM command*  
    <http://en.wikipedia.org/wiki/TRIM_(SSD_command)>

—–

Defragmentering betyr jo å flytte data rundt om på disk for at det skal
være mest mulig sammenhengende lagring, dette har ingen effekt på SSD,
men derimot fører bare til mye ekstra skriving, dvs slitasje (nedkorting
av en SSD’s levetid).

Filsystemet bør fungere med et addresseringssystem som ikke gjør at
skriving til en 4KiB filsystemblokk betyr skriving til to 4KiB
SSD-page’r. Dvs filsystemets addresseringsenheter bør være alignet med
SSD’n sine page’r.

Generelt: ikke skrive så ofte til disk: dvs skru av journalling (skriver
bare til disk hvert 5. sekund) og noatime? eller lite nyttige
kjerringråd? <http://tytso.livejournal.com/tag/ssd>. Mer moderne SSD-er
(2012 og nyere) er gode på wear-levelling, og du kan i praksis skrive
kontinuerlig til de i flere år. For den gjennomsnittlige consumer-ssd
vil dette si at du sannsynligvis bytter SSD-en før den er utslitt.

TRIM er støtte av Linux (siden 2.6.28) og Windows 7 (Android siden 4.3),
og de fleste disker siden 2011:
<http://www.tomshardware.com/forum/252653-32-supporting-trim>

### RAID

![image](/home/erikhje/office/kurs/opsys/11-io/tex/../../resources/MOS_3e/05-20.pdf)

(Merk at figuren i Tanenbaum tar også med RAID nivå 6 men vi har ikke
brukt denne figuren her siden den blir nesten uleselig da :)

Figuren viser RAID nivå 0 til 5, merk paritetsinfo som er lagret der det
er skyggede felter. RAID 6 er samme som RAID 5 men med dobbelt
paritetslagring.

Det brukes også kombinasjoner, f.eks. RAID 10 som er RAID 1 på toppen av
RAID 0.

Se

<http://www.acnc.com/raid>

## Theory questions

1.  Hva er forskjellen på memory mapped og isolated I/O? Angi fordeler
    og ulemper med disse to prinsippene.

2.  På en harddisk, hvor mange bytes finnes som regel i en sektor? Hva
    er en sylinder? Hva er typisk gjennomsnittlig aksesstid for en disk
    i dag? Hva er overføringsraten (ca, i MB/s) mellom diskplate og
    buffer?

3.  Hva oppnår vi med å koble diskene som RAID disker? Hvordan er
    diskene organisert på RAID-level 1. Forklar hvordan diskene er
    organisert på RAID-level 5.

4.  Hvilke fire kriterier definerer et presist interrupt?

5.  Forklar forskjellen mellom HDD og SSD når det gjelder lesing,
    skriving/overskriving og sletting av filer. Hva er poenget med TRIM
    kommandoen?

6.  Hva betyr det at et operativsystem er tilpasset SSD disker (slik som
    f.eks. Windows 7 er).

7.  Hvorfor er det en fordel at data lagres sammenhengende på en
    Harddisk? Hvordan er dette på en SSD?

## Lab exercises

1)  **Virtuell Windows i skyen**. Merk: hvis du ikke er i et NTNU-nett,
    så bruk
    [VPN](https://innsida.ntnu.no/wiki/-/wiki/Norsk/Installere+VPN).
    
    1.  Logg inn på <https://skyhigh.iik.ntnu.no> (NTNU brukernavn og
        passord)
    
    2.  Lag et nøkkelpar:
        
        1.  Compute
        
        2.  Key Pairs
        
        3.  Create Key Pair
        
        4.  Key Pair Name `mykey`
        
        5.  Create Key Pair (**ta godt vare på filen mykey.pem som du da
            får som output, det er din private nøkkel som du trenger
            senere**)
    
    3.  Lag en Windows server i et nettverk:
        
        1.  Orchestration
        
        2.  Stacks
        
        3.  Launch Stack
        
        4.  Template file: `single_windows_instance.yaml` (som du finner
            i iikos-oblig3 repoet)
        
        5.  (Du behøver ikke velge noe på “Environment Source” eller
            “Environment File”)
        
        6.  Stack Name: `Mysil` (eller hva du vil)
        
        7.  Skriv inn NTNU-passordet ditt
        
        8.  Sjekk at key\_name sier `mykey`
        
        9.  Launch
    
    4.  (vent ca 15 minutter)
    
    5.  Brukernavnet ditt er `admin`. Finn ut hva passordet ditt er:
        
        1.  Compute
        
        2.  Instances
        
        3.  Retrieve Password (fra nedtrekksmeny til høyre)
        
        4.  Private Key File: last opp `mykey.pem`
        
        5.  Decrypt Password
        
        6.  Kopier og ta vare på passordet du ser i feltet Password
    
    6.  Noter deg hvilken Floating IP servern din har (Compute,
        Instances), det er denne IP-adressen du skal benytte for å koble
        deg til servern med Remote Desktop.
    
    7.  For å koble deg opp mot den nylagde Windows-maskina så benytt en
        eller annen Remote Desktop klient, f.eks. `mstsc` på Windows,
        `Microsoft Remote Desktop connection` fra App store på Mac,
        eller på Linux `apt install freerdp2-x11` og  
        `xfreerdp /u:Admin +clipboard /w:1366 /h:768 /v:SERVER_IP`
    
    8.  Gå gjerne tilbake til laboppgavene om Windows i første kapittel
        i dette kompendiet og kjør PowerShell-kommandoene der.

2)  Skriv en PowerShell-kommandolinje som skriver ut alle prosesser som
    har working set større enn 10MB. Det skal skrives ut prosessnavn,
    prosess-ID og working set-størrelse, sortert på working
    set-størrelse.

3)  **(OBLIG) Prosesser og tråder.**  
    Lag et script `myprocinfo.ps1` som gir brukeren følgende meny med
    tilhørende funksjonalitet:
    
    ``` 
      1 - Hvem er jeg og hva er navnet på dette scriptet?
      2 - Hvor lenge er det siden siste boot?
      3 - Hvor mange prosesser og tråder finnes?
      4 - Hvor mange context switch'er fant sted siste sekund?
      5 - Hvor stor andel av CPU-tiden ble benyttet i kernelmode og i usermode siste sekund? 
      6 - Hvor mange interrupts fant sted siste sekund?
      9 - Avslutt dette scriptet
    
    Velg en funksjon:
    ```
    
    Hint: bruk en `switch case` for å håndtere menyen, og hvert menyvalg
    bør føre til en `Write-Output` hvor svaret settes inn via `$()`, og
    du vil nok få god bruk av cmdlet’ene `Get-CimClass` og
    `Get-CimInstance`. Du kan søke etter relevante CIM-klasser (CIM er
    forkortelse for Common Information Model) med  
    `Get-CimClass | Where-Object { $_.CimClassName -match "Thread" }` og
    deretter søke gjennom en instans av klassen du fant etter passende
    egenskaper med  
    `Get-CimInstance Win32_Thread | Get-member | Where-Object {$_.Name
    -match "State"}`. Det er ikke alltid så lett å gjette/google seg til
    riktig CIM-klasse, så du kan ta utgangspunkt i at i denne oppgaven
    trenger du å bruke instanser av CIM-klassene
    `Win32_OperatingSystem`, `Win32_Thread`,  
    `Win32_PerfFormattedData_PerfOS_System` og  
    `Win32_PerfFormattedData_Counters_ProcessorInformation`.

# Deadlock

## Intro

### Resources

##### Resources

  - Deadlocks can occur on *hardware resources* or *software resources*

  - *Preemptable* and *Nonpreemptable* resources

—–

Resursser i forbindelse med deadlocks kan blant annet være:

  - Hardware:
    
      - Scanner
    
      - Brenner
    
      - Printer
    
      - Plotter
    
      - CPU
    
      - Minne

  - Software:
    
      - Filer
    
      - OS-tabeller
    
      - database-records

Det at en resurs er preemptable betyr at den kan tas vekk fra en prosess
uten problemer, f.eks. CPU eller Minne. Non-preemtable vil si at
ressursen ikke kan fjernes fra prosessen og gis til en annen uten å
skape problemer. For eksempel om en CD-Brenner blir fjernet fra en
prosess som er midt i en brenning. Det vil resultere i en ødelagt
CD-Plate.

Deadlocks dreier seg om Nonpreemptable ressurser, ofte fler av hver type
(f.eks. to Brennere).

##### Resources

  - Sequence of events required to use a resource:
    
    1.  *Request* the resource
    
    2.  *Use* the resources
    
    3.  *Release* the resource

  - If resource not available
    
      - Process can wait (*we assume this*)
    
      - Process can return error code

—–

### Semaphores

##### Protecting Resources with Semaphores

![image](/home/erikhje/office/kurs/opsys/12-deadlocks/tex/../../resources/MOS_3e/06-01.pdf)

Ressurser kan beskyttes med semaforer, der vi krever at en semafor skal
telles ned før resursen reserveres. Figuren over er et kodeeksempel for
en eller to ressurser.

##### Code with Potential Deadlock

![image](/home/erikhje/office/kurs/opsys/12-deadlocks/tex/../../resources/MOS_3e/06-02.pdf)

Dersom det er behov for mer enn en ressurs, er rekkefølgen disse
reserveres i veldig viktig. I eksemplet over, vil den ene kodesnutten
være trygg, mens den andre vil gi mulighet for å forårsake deadlocks.

Dersom begge prosessene er enige om at ressurs 1 skal reserveres før
ressurs 2 (a), vil vi ende opp med at den ene prosessen må vente litt
dersom den andre har begynt å reservere ressursene

Derimot, dersom prosess A reserverer ressurs 1 først, før det kommer et
context-switch som lar prosess B kjøre før A har reservert ressurs 2, så
åpner vi for muligheten for at B kommer til å reservere 2, og deretter
sitte å vente på at ressurs 1 skal bli ledig. Samtidig sitter A og
holder på 1, og venter på at 2 skal bli ledig. Da har vi en deadlock.
(b)

### Deadlock def

##### Deadlocks

  - *A set of processes is deadlocked if each process in the set is
    waiting for an event that only another process in the set can
    cause.*

  - No processes can run, release resources or wake up: this is called a
    *resource deadlock*.

—–

Vi husker dining philosophers: deadlock hvis alle plukker opp sin
venstre gaffel og venter på at den høyre skal bli ledig (hvis ikke
venting men legger ned igjen, og fortsetter synkront å plukke opp legge
ned alle samtidig har vi en relatert problemstilling: starvation).

### Four conditions

##### Conditions for Resource Deadlocks

1.  *Mutual exclusion*
    
      - Each resource assigned to one process or available.

2.  *Hold and wait*
    
      - Processes holding a resource can ask for other resources.

3.  *No preemption*
    
      - Resources cannot be forcibly taken away, must be released.

4.  *Circular wait*
    
      - Must be a circular chain of two or more processes, each waiting
        for a resource held by the next member of the chain.

(Note: 1-3 leads to possibility of deadlock, 1-4 guarantees deadlock)

—–

### Modelling

##### Deadlock Modelling

![image](/home/erikhje/office/kurs/opsys/12-deadlocks/tex/../../resources/MOS_3e/06-03.pdf)

R er tildelt prosess A.

Prosess B ber om S.

Prosessene C og D er i deadlock, siden hver av de ber om en ressurs som
er reservert av den andre..

##### How Deadlock Can Happen/Be Avoided

![image](/home/erikhje/office/kurs/opsys/12-deadlocks/tex/../../resources/MOS_4e/6-4.pdf)

(a)-(c) kjøres prosessene sekvensielt og ingen deadlock oppstår, men
dette er dumt i praksis siden man ikke har noen parallellitet og en
prosess som blokkerer på I/O ødelegger ytelsen.

(d)-(j) anta round-robin og blokkering på I/O, i (h) blokkerer A, i (i)
blokkerer B, og i (j) blokkerer C og vi har deadlock.

Hvis OSet bestemmer seg for å vente med å kjøre B til A og C er ferdig
unngår vi deadlock, men hvordan kan OSet kjenne til at det er fare for
deadlock? (deadlock avoidance)

## Dealing with Deadlocks

##### Dealing with Deadlocks

1.  Ignore the problem.

2.  Detection and Recovery.

3.  Dynamic avoidance by careful resource allocation.

4.  Prevention, negating one of the four conditions.

### Ostrich alg

##### The Ostrich Algorithm

  - Stick your head in the sand and pretend there is no problem at all.

  - *Common approach\! (with some modifications)*

—–

Vanlig grunnleggende tanke i UNIX og Windows, problemet oppstår så
sjeldent at det er ikke verdt ytelsestapet å innføre mekanismer som
håndterer deadlock.

Moderne Linux og Windows har en del forebyggende tiltak, resource
ordering, dynamiske grenser på kritiske systemtabeller, osv

### Detect/Recover

#### Resource graphs

##### A Resource Graph with a Cycle

![image](/home/erikhje/office/kurs/opsys/12-deadlocks/tex/../../resources/MOS_3e/06-05.pdf)

Hvis vi bare har en instans av hver ressurs kan denne metoden brukes for
å detektere deadlock.

*Vi lar altså deadlock inntreffe og forsøker komme oss ut av det
(recovery)*.

*Vi har deadlock hvis vi finner en syklus i grafen*.

Formelle algoritmer for å sjekke grafer for sykler henter vi fra diskret
matematikken (kanskje fra AlgMet emnet?).

#### Recovery

##### Deadlock Recovery

  - Recovery through Preemption

  - Recovery through Rollback

  - Recovery through Killing Processes

Tre enkle alternativer for å håndtere deadlocks kan være:

1.  Preeemption er altså å ta bort en ressurs fra en prosess med makt.
    Dette er veldig vanskelig i praksis uten å ødelegge noe for
    prosessen.

2.  Prosesser som deadlockes kan restartes fra registrere sjekkpunkt
    (snapshots).

3.  Drep den prosessen som frigir de nødvendige ressurser og som
    samtidig tar minst skade (dvs. ikke den prosessen som oppdaterer en
    database) (f.eks. tre prosesser som alle ønsker printer og plotter,
    men bare en har begge og de to andre er deadlocked).

### Avoidance

#### States

##### A Safe State

![image](/home/erikhje/office/kurs/opsys/12-deadlocks/tex/../../resources/MOS_3e/06-09.pdf)

En tilstand er safe hvis den

1.  *ikke er i deadlock* og

2.  *det finnes en scheduling rekkefølge hvor alle prosessene kan kjøres
    ferdig selv om alle ber om sine maksimale ressurskrav*

##### An Unsafe State

![image](/home/erikhje/office/kurs/opsys/12-deadlocks/tex/../../resources/MOS_3e/06-10.pdf)

A har fått en tilleggsressurs fra (a) til (b), det skulle den ikke fått
fordi (b) representerer en unsafe tilstand\!

Men det er ikke sikkert at det blir deadlock\!

I en safe tilstand vi garantert systemet kunne kjøre ferdig, fra en
unsafe tilstand finnes ingen slik garanti (men det kan hende det går
bra, men vi har altså ingen garanti for det i en unsafe tilstand).

En unsafe tilstand er altså IKKE en deadlock tilstand.

#### Bankers alg single

##### Banker’s Algorithm for a Single Resource

![image](/home/erikhje/office/kurs/opsys/12-deadlocks/tex/../../resources/MOS_3e/06-11.pdf)

Banker’s referer til en bank som gir kreditt til et sett kunder og må
passe på at man ikke går tom for penger i banken.

Banker’s vil sjekke hver tilstand og utsette alle forsøk på å gå til en
unsafe tilstand, dvs hvis en prosess \(P_{1}\) kommer med en
ressursforespørsel som fører systemet over i en unsafe tilstand,
utsettes denne forespørselen til noen andre prosesser har frigitt de
nødvendige ressurser slik at \(P_{1}\)’s ressursforespørsel kan
imøtekommes uten av systemet ender i en unsafe tilstand.

#### Bankers alg mult

##### Banker’s Algorithm for Multiple Resources

![image](/home/erikhje/office/kurs/opsys/12-deadlocks/tex/../../resources/MOS_3e/06-12.pdf)

Banker’s algoritme er identisk med “deadlock detection algorithm” men
forskjellen er at når vi kaller den Banker’s algoritme betyr det at den
benyttes hele tiden til å styre unna unsafe tilstander (dvs styre unna
muligheten for deadlock). Ved “deadlock detection” så lar man prosessene
får sine ressurskrav så fremt det er mulig, og man bare lar deadlock
oppstå, så kjører man “deadlock detection algorithm” av og til for å
sjekke etter deadlock.

En annen forskjell er at ved Banker’s antar vi at alle prosesser ber om
sine maksimale ressurskrav, mens ved “deadlock detection algorithm” ser
vi bare på hva som er ressursforespørslene til prosessene i det
algoritmen kjøres.

Altså: i det en prosess gjør en ressursforespørsel, later systemet som
om denne aksepteres og kjører Banker’s algoritme for å sjekke om
systemet da havner i en unsafe tilstand, hvis så er tilfelle utsettes
ressursforespørselen.

![image](/home/erikhje/office/kurs/opsys/12-deadlocks/tex/../img/stallings.png)

MERK: systemet mottar en forespørsel fra en prosess, så brukes Banker’s
for å sjekke om systemet havner i en unsafe tilstand hvis forespørselen
imøtekommes, hvis så er tilfelle utsettes forespørselen.

Kjør så med Allocation matrix (5 1 1) for \(P_{2}\) og Claim matrix (2 1
1) for \(P_{1}\), (6 3 3) for \(P_{2}\) og (4 2 5) for \(P_{3}\). Dette
er en unsafe tilstand og \(P_{1}\)’s forespørsel kan ikke imøtekommes,
den må utsettes.

### Prevention

##### Deadlock Prevention

*Can we make sure any of the four conditions are not satisfied?*

1.  Mutual exclusion
    
      - Each resource assigned to one process or available.

2.  Hold and wait
    
      - Processes holding a resource can ask for other resources.

3.  No preemption
    
      - Resources cannot be forcibly taken away, must be released.

4.  Circular wait
    
      - Must be a circular chain of two or more processes, each waiting
        for a resource held by the next member of the chain.

—–

Ja, man kan sørge for å fjerne minst ett av alternativene over slik at
deadlocks ikke skjer, og det er det som gjøres i praksis. Bankers er
umulig i praksis siden man ikke vet en prosess sine resursbehov i
forkant, men man har ett par andre teknikker som er nyttige:

#### Mutual exclusion

##### Attacking Mutual Exclusion

  - Can we avoid exclusive access to resources?

  - Yes, for some resources we can do *spooling*.

  - Good principle: *Avoid assigning a resource when that is not
    absolutely necessary, and try to make sure that as few processes as
    possible may actually claim the resource*.

—–

TAVLE:

``` 

P1 -> +----------+      ------
      | Spooler  |     /      \     +---------+
      | Directory| -> | Daemon | -> | Printer |
      |          |     \      /     +---------+
P2 -> +----------+      ------
```

Deadlock kan oppstå hvis P1 og P2 fyller opp spoolerdir uten at noen av
dem blir ferdige med å skrive dit, da venter begge på ledig plass som
bare kan frigjøres av hver av dem siden daemon ikke vil begynne skrive
ut en jobb før hele jobben er i køen (i spoolerdir).

Mao, vi har fjernet deadlock-muligheten knyttet til printern men skapt
en deadlock-mulighet knyttet til diskplass (men diskplass er idag såpass
tilgjengelig at en spoolerdir neppe vil fylles opp, og dermed bør
sannsynligheten for deadlock være minimal).

#### Hold & wait

##### Attacking Hold and Wait

  - Can we require all processes to state all their resource needs in
    advance?

  - No, not possible and not practical.

  - (and if we could, we could suddenly apply the Banker’s algorithm)

—–

For det første vil en prosess vente lenge på å få alle ressurser den
trenger, og samtidig vil den ofte unødvendig holde på mange ressurser.

#### No preempt

##### Attacking No Preemption

In practice, only possible with preemptable resources (CPU, Memory).

—–

Og som vi husker fra de første foilene, deadlock problematikken
involverer primært nonpreemptable ressurser.

#### Circular wait

##### Attacking Circular Wait

![image](/home/erikhje/office/kurs/opsys/12-deadlocks/tex/../../resources/MOS_3e/06-13.pdf)

*Just order the resources\!*

—–

Sirkulær venting kan unngås ved å nummerere ressursene, og innføre
regelen *Alle ressursforespørsler fra en prosess må være i riktig
numerisk rekkefølge.*

F.eks. hvis \(i>j\) så kan ikke A be om \(j\), mens hvis \(j>i\) så kan
ikke B be om \(i\), en syklus kan altså ikke oppstå.

Se Rule 1 og Rule 2 i Del 3 av

<http://www.ddj.com/hpc-high-performance-computing/204801163>

##### Deadlock Prevention Summary

![image](/home/erikhje/office/kurs/opsys/12-deadlocks/tex/../../resources/MOS_3e/06-14.pdf)

## Theory questions

1.  Det kreves at fire ulike betingelser alle må være oppfylt for at
    deadlock skal inntre. Beskriv disse kort.

2.  Hva vil det si at en tilstand er “unsafe” i forbindelse med
    deadlock?

3.  Forklar kort hva en ressursgraf er og hvordan den kan benyttes til å
    avsløre om vi har en deadlock situasjon.

4.  Tanenbaum oppgave 6.2  
    Students working at individual PCs in a computer laboratory send
    their files to be printed by a server which spools the files on its
    hard disk. Under what conditions may a deadlock occur if the disk
    space for the print spool is limited? How may deadlock be avoided?

5.  Tanenbaum oppgave 6.25  
    A computer has six tape drives, with \(n\) processes competing for
    them. Each process may need two drives. For which values of \(n\) is
    the system deadlock free?

6.  Tanenbaum oppgave 6.27  
    A system has four processes and five allocatable resources. The
    current allocation and maximum needs are as follows:
    
    ``` 
           Allocated  Maximum    Available
    ProcA  1 0 2 1 1  1 1 2 1 3  0 0 x 1 1
    ProcB  2 0 1 1 0  2 2 2 1 0
    ProcC  1 1 0 1 0  2 1 3 1 0
    ProcD  1 1 1 1 0  1 1 2 2 1
    ```
    
    What is the smallest value of `x` for which this is a safe state?

## Lab exercises

**Hjelp til lesbarhet: byte konvertering.**  
Skriv et script `human-readable-byte.ps1` som leser et tall, antall
bytes, fra `STDIN`, konverter tallet til KB, MB, GB eller TB avhengig av
størrelsen på tallet, og skriver den nye størrelsen i heltall. Scriptet
bør oppføre seg omtrent slik:

    $ Write-Output 25000000 | human-readable-bytes.ps1 
    23.8418579101563MB
    $ Write-Output 250 | human-readable-bytes.ps1
    250B

**Hjelp til lesbarhet: ns konvertering.**  
Skriv et script `human-readable-ns.ps1` som leser et tall, antall ns
(nanosekunder), fra `STDIN`, konverter tallet til us (microsekunder), ms
(millisekunder) eller sek (sekunder) avhengig av størrelsen på tallet,
og skriver den nye størrelsen i heltall. Scriptet bør oppføre seg
omtrent slik:

    $ Write-Output 25000000 | human-readable-ns.ps1
    25ms
    $ Write-Output 250 | human-readable-ns.ps1
    250ns

**(OBLIG) En prosess sin bruk av virtuelt og fysisk minne.**  
Skriv et script `procmi.ps1` som tar et sett med prosess-ID’er som
kommandolinjeargument og for hver av disse prosessene skriver til en fil
`PID-dato.meminfo` følgende info:

Total bruk av virtuelt minne (\(VirtualMemorySize\))

Størrelse på Working Set

Eksempel kjøring:

    $ procmi.ps1 420 8566
    $ Get-Content 420--20100314-221523.meminfo
    ******** Minne info om prosess med PID 420 ********
    Total bruk av virtuelt minne: 35.6875MB
    Størrelse på Working Set: 816KB
    
    $ 

# Virtualisering

## How much OS?

##### How much OS is needed for Mobile/Cloud/IoT?

![image](/home/erikhje/office/kurs/opsys/13-virtualization/tex/../img/virt.png)  
<span><http://www.corentindupont.info/blog/posts/Programming/2015-09-09-VM-evol.html></span>

Read the very nice and easy paper “The Rise and Fall of the Operating
System”.

Det er faktisk et pågående forskningsprosjekt ved HiOA for å bygge et
unikernel-OS: <http://www.includeos.org>

Simple demo of a unikernel-based app:
<https://www.youtube.com/watch?v=EyeRplLMx4c>

## Intro Virtual Machines

##### Virtualization

  - *Virtualization* means allowing a single computer to host multiple
    virtual machines.

  - 40 year old technology\!

—–

*Poenget med dette temaet er å få innblikk i teknologien som er
involvert i virtualiseringen som skjer på en fysisk maskin, slik at vi
lettere kan forstå all den videre debatten rundt virtualisering som
vanligvis er mye mer praktisk rettet: hvilke applikasjoner som bør/bør
ikke virtualiseres, hva som funker/funker ikke ved snapshots og live
migrering, hvorfor sammenheng hardware og versjon av VMM betyr mye, osv*

Se

<http://en.wikipedia.org/wiki/Virtualization>

vi snakker om platform virtualisering.

Se

<http://en.wikipedia.org/wiki/Comparison_of_platform_virtual_machines>

god oppdatert oversikt over de forskjellige variantene.

##### Why Virtualization?

  - Servers can run on different virtual machines, thus maintaining the
    partial failure model that a multicomputer.

  - It works because most service outages are not due to hardware.

  - Save electricity and *space*\!

—–

Først og fremst det siste punktet sammen med mye lettere sysadm,
mulighet for snapshot, live migrering, legacy systemer, prototyping osv

### Requirements

##### Virtualizable Hardware

  - Popek and Goldberg, 1974:
    
      - *A machine is virtualizable only if the sensitive instructions
        are a subset of the privileged instructions*.

  - this caused problems on X86 until 2005...

—–

  - Sensitiv instruksjon  
    kan bare utføres i kernel mode.

  - Privilegert instruksjon  
    forårsaker en `trap` (dvs en overgang til kernel mode) hvis utført
    utenfor kernel mode.

Merk: dette er altså “det strenge kravet”, det må ikke være oppfylt, det
har VMware bevist. Men det må være oppfylt for å få til klassisk
*trap-and-emulate* virtualisering slik det alltid har blitt gjort på
IBMs stormaskiner (siden ca 1970).

## Hypervisors

##### Hypervisors

![image](/home/erikhje/office/kurs/opsys/13-virtualization/tex/../../resources/MOS_3e/01-29.pdf)

Hypervisor = Virtual Machine Monitor (VMM)

  - Type 1 hypervisor  
    direkte på hardware som et OS. Krever at Popek og Goldberg kravet er
    oppfylt. Alle sensitive instruksjoner som utføres av gjesteOS’et
    fanges opp av hypervisoren og emuleres (utføres på vegne av
    gjesteOS’et).

  - Type 2 hypervisor  
    kjører på toppen av et OS (men har mye av koden sin sammen med OSet
    i kernel mode (i form av kjernemoduler/drivere).

(mao denne grensen kan betraktes som noe flytende siden den er en gammel
definisjon som stammer fra nettopp Popek og Goldbergs artikkel fra 1974,
lenge før det fantes teknologiene binæroversetting og
paravirtualisering)

## CPU

### Binary translation

##### Binary Translation

  - E.g. *Binary translation* in VMware:
    
      - During program exec, basic blocks (ending in jump, call, trap,
        etc) are scanned for sensitive instructions
    
      - Sensitive instructions are replaced with call to vmware
        procedures
    
      - These translated blocks are cached

  - Very powerful technique, can run at close to native speed because VT
    hardware generate many traps (which are expensive).

Dette er altså teknologien utviklet av VMware fra DISCO-prosjektet ved
Stanford.

Koden til gjesteOSet granskes rett før den kjøres, og sensitive
instruksjoner endres til kall til hypervisoren.

Noe tilsvarende teknologi finnes også i VirtualBox: “VirtualBox contains
a Code Scanning and Analysis Manager (CSAM), which disassembles guest
code, and the Patch Manager (PATM), which can replace it at runtime.”
fra  
<http://www.virtualbox.org/manual/ch10.html#idp21833280>

  - Figur side 3 “Hypervisor Architecture”  
    Ved software virtualisering (som i all hovedsak betyr VMware’s
    binæroversettelse og Xen’s paravirtualisering) kjører koden til
    gjeste OS’et i ring 1, og det er denne koden som dynamisk
    binæroversettes/statisk paravirtualiseres. Usermode-koden til
    applikasjonene i ring 3 er ikke noe problem og de kjøres direkte,
    men problemene oppstår når gjestekjernen i ring 1 forsøker gjøre
    sensitive instruksjoner som ikke er del av de privilegerte
    instruksjonene, det er disse instruksjonene som først og fremst må
    binæroversettes. Samtidig binæroversettes mye annet grunnet
    optimalisering.

  - Figur side 5  
    Alle systemkall havner altså hos VMM istedet for
    gjesteoperativsystemet, slik at VMM må videresende alle systemkall
    til gjesteoperativsystemet som kjører binæroversatt kode i ring 1.
    (*Les også første to avsnitt side 7 “Much has been...”*)

  - “It is clear ...” side 6  
    caching er viktig (TC = translator cache), men utfordringene er
    fortsatt systemkall, minnehåndtering og I/O.

  - Figur side 8 “Sysenter”  
    (sysenter og sysexit er altså enklere mode shifts enn int 0x80)
    Denne viser igjen det samme som i forrige figur men merk
    kommentarene under figuren som viser at et systemkall på en virtuell
    maskin fort kan ta opp imot 10 ganger så lang tid som på en vanlig
    maskin.

### Paravirtualization

##### True and Paravirtualization

![image](/home/erikhje/office/kurs/opsys/13-virtualization/tex/../../resources/MOS_3e/08-27.pdf)

Mens både type 1 og type 2 hypervisor fungerer med umodifisert OS,
krever paravirtualisering modifisering av OSet.

Alle sensitive instruksjoner i OSet erstattes med kall til hypervisoren.

Hypervisoren blir i praksis en mikrokjerne ved paravirtualisering.

Paravirtualisering er en statisk endring av gjesteOSet slik at sensitive
instruksjoner endres til kall til hypervisoren (med andre ord: samme
type endring i koden som type 2 hypervisor (men statisk, dvs
forhåndsendret) slik at en type 1 hypervisor kan benyttes).

*En fordel med paravirtualisering er at den tillater mye mer endring til
gjesteoperativsystemet enn binæroversetting, derav mulighet for enda mer
optimalisering (redusere antall overganger til VMM), men det går
selvfølgelig på bekostning av fleksibilitet, dvs det er ikke alle OS du
har tilgang til kildekoden til...*

  - Figur side 11  
    All I/O gjøres av en spesielt privilegert VM (kalt Domain0 i Xen). I
    denne figuren kan ikke VM3 kjøres fullt ut paravirtualisert siden
    det er et umodifisert gjesteOS, men det er ment å illustrere en
    kombinasjon av binæroversetting og paravirtualisering

### HW virtualization

##### Hardware Virtualization

![image](/home/erikhje/office/kurs/opsys/13-virtualization/tex/../../resources/MOS_3e/08-26.pdf)

  - Figur side 13  
    Innføring av hardwarestøtte for virtualisering (Intel VT-x og AMD-V)
    på x86 betyr å innføre en ny “Ring -1” som kalles “VMX root mode”
    hvor VMM kjører. På denne måten kan gjesteoperativsystemet kjøre i
    sin tiltenkte Ring 0 slik at de kan oppføre seg normalt (det trengs
    ikke binæroversetting eller paravirtualisering). Dette kan være en
    fordel ved enkle systemkall siden de kan utføres uten overgang til
    VMM. Ulempen er at man mister optimaliseringsmulighetene man har med
    binæroversetting og paravirtualisering.

CPU flags som vi må sjekke for å undersøke i hvilken grad vi har
hardware støtte for virtualisering (fra
<http://virt-tools.org/learning/check-hardware-virt/>):

  - vmx  
    Intel VT-x, basic virtualization.

  - svm  
    AMD SVM, basic virtualization.

  - ept  
    Extended Page Tables, an Intel feature to make emulation of guest
    page tables faster.

  - vpid  
    VPID, an Intel feature to make expensive TLB flushes unnecessary
    when context switching between guests.

  - npt  
    AMD Nested Page Tables, similar to EPT.

  - tpr\_shadow and flexpriority  
    Intel feature that reduces calls into the hypervisor when accessing
    the Task Priority Register, which helps when running certain types
    of SMP guests.

  - vnmi  
    Intel Virtual NMI feature which helps with certain sorts of
    interrupt events in guests.

`egrep -o '(vmx|svm|ept|vpid|npt|tpr_shadow|flexpriority|vnmi)' \`  
`/proc/cpuinfo | sort | uniq`

(på Windows bruk sysinternals-verktøyet `coreinfo -v`)

Noen ytelsesmålinger for å forsøke illustrere forskjeller:

Ren usermode prosess: `time ./sum`

Blandet usermode/kernelmode, mange enkle systemkall: `time ./getppid`
og  
`time ./gettimeofday`

Mye kernelmode med en del minneallokeringer: `time ./forkwait`

\- getpid/gettimeofday bør gi fordel til HW virtualisering siden VMM
ikke trengs (for BT så må syscall alltid passere VMM)

\- forkwait bør gi fordel BT siden mange VMM enter/exit, grunnet mye
jobb for kjernen spes ift opprette minneområde og pagetabeller (alle
disse vil trap-and-emulate, altså trap fra gjesteOSkjernen til VMM), mao
her trengs virtualiseringsstøtte i MMU.

## Memory

##### Virtual and Physical Addresses

![image](/home/erikhje/office/kurs/opsys/13-virtualization/tex/../../resources/MOS_3e/03-10.pdf)

Vi husker hvorfor vi har page tabeller og hvordan adresseoversetting fra
logisk/virtuelt minne til fysisk minne funker via page tabellen.

##### Virtual Memory Architecture

![image](/home/erikhje/office/kurs/opsys/13-virtualization/tex/../../07-memman-vm-paging-seg/extra/virtualmemory.pdf)

Og slik ser det overordnede bildet for virtuelt minne ut slik vi husker
det.

##### Hits, Misses and Page Faults

![image](/home/erikhje/office/kurs/opsys/13-virtualization/tex/../img/TLB-1.pdf)

Denne sammenhengen kjenner vi til siden vi har kodet den i bash og
powershell. Så lenge alle oppslag er cachet i TLB går alt veldig kjapt.

##### Traditional Page Tables

![image](/home/erikhje/office/kurs/opsys/13-virtualization/tex/../img/mem-virt-1.pdf)

<span>(Note: this and the following figures inspired by VMware White
Paper, “Performance Evaluation of Intel EPT Hardware Assist”,
2009.)</span>

Og hver prosess har altså sin page table (i det aller fleste
implementasjoner). Husk at denne som regel er en multilevel page table i
praksis (to nivåer på 32-bits X86, fire nivåer på 64 bits X86 (siden
bare 48 bits benyttes i praksis)).

La oss nå se på hva som skjer når vi må innføre et mellomnivå
(hypervisoren) mellom hardware og OS (siden OSet nå kjører i en virtuell
maskin styrt av en hypervisor).

##### Page Tables in Virtual Machines

![image](/home/erikhje/office/kurs/opsys/13-virtualization/tex/../img/mem-virt-2.pdf)

Her forholder prosessene i de virtuelle maskinene seg ikke lenger til
fysisk minne (RAM), men det de tror er fysisk minne. Page tables kalles
nå Guest page tables, og det som på en måte er den virkelige page table
kalles en physical page map som regel.

MERK: TLB må fortsatt fylles med mappingen fra logiske/virtuelle
adresser til fysiske/maskin adresser, og dette kan gjøres enten via
Shadow page tables i software, eller med Nested page tables i hardware.

### Shadow page tables

##### Shadow Page Tables (Software)

![image](/home/erikhje/office/kurs/opsys/13-virtualization/tex/../img/mem-virt-3.pdf)

Med Shadow page tables opprettes en tredje tabell som brukes til å fylle
TLB som vist i neste figur.

##### Shadow Page Tables (Software)

![image](/home/erikhje/office/kurs/opsys/13-virtualization/tex/../img/TLB-2.pdf)

Dette fungerer bra i de fleste tilfeller, og også bedre enn hardware
løsningen med nested page tables i noen tilfeller.

Problemer:

Hver endring i guest page table må fanges opp, og det er ikke trivielt
siden en prosess jo har lov til å skrive til minne (det forårsaker
normalt ikke en trap).

Hver endring i guest page table gjør at page map og shadow page table må
oppdateres, noe som forårsaker overgang til hypervisoren, noe som er
kostbart.

Hver endring i shadow page table (f.eks. oppdatering av references eller
dirty bit i TLB, som deretter skriver til shadow page table) forårsaker
også oppdateringer til page map og guest page table.

### Nested page tables

##### Nested Page Tables (Hardware)

![image](/home/erikhje/office/kurs/opsys/13-virtualization/tex/../img/mem-virt-2.pdf)

Her bruker vi altså ikke shadow page tables, med hardware støtte så gjør
vi altså ikke noe “kunstig” i software, vi lar løsningene muligens være
suboptimale og søker heller rask hardware implementasjon av disse.

Merk: vi mister altså den gule pilen, men vi får en bedre og mer optimal
TLB.

##### Nested Page Tables (Hardware)

![image](/home/erikhje/office/kurs/opsys/13-virtualization/tex/../img/TLB-3.pdf)

  - Figur side 16 “Hardware Support”  
    Andre generasjons hardwarestøtte for virtualisering innebærer altså
    en spesiell TLB som cacher guest page table og physical page map
    gjennomgangen (altså cacher hele 2D page walk’n) sammen med selve
    guest-virtuell-address til fysisk/maskin-adresse slik at TLB blir
    veldig effektiv (dvs man slipper vedlikeholde en shadow page table).
    Problemet er at en TLB miss medfører en langt mer omfattende rekke
    av tabelloppslag enn om man hadde en shadow page table. Istedet for
    N oppslag i en N-level shadow page table blir det N x N oppslag
    (eller N x M egentlig hvis man skal være presis) (siden man må via
    physical page maps for hver guest page table oppslag).

##### 64-bit Page Tables Native

![image](/home/erikhje/office/kurs/opsys/13-virtualization/tex/../img/amd-nested-fig1.png)

##### 64-bit Page Tables Virtualized

![image](/home/erikhje/office/kurs/opsys/13-virtualization/tex/../img/amd-nested-fig4.png)

  - AMD paper: Figur 1 side 7  
    Dette er altså slik multilevel page tabeller fungerer, i dette
    tilfelle på dagens 64-bits arkitektur, mao fire nivåer med page
    tabeller før man havner i RAM). CR3-registeret holder altså adressen
    til øverste-nivå page table.

  - AMD paper: Figur 4 side 13  
    “Guest and nested page tables are set up by the guest and hypervisor
    respectively. When a guest attempts to reference memory using a
    linear address and nested paging is enabled, the page walker
    performs a 2-dimensional walk using the gPT and nPT to translate the
    guest linear address to system physical address.” Og slik blir det
    da med VMer, for hvert oppslag en VM gjør i sitt virtuelle fysiske
    minne, må tilsvarende “page walk” gjøres i de virkelige page
    tabellene (altså de som vi har kalt physical page map). *Cluet er
    altså at et oppslag i ett av nivåene i en page tabell gir adressen
    til starten av neste nivå page tabell, og denne adressen må
    oversettes til korrekt fysisk/maskin adresse via en page walk.*

##### Implementations

  - Intel EPT

  - AMD RVI (NPT)

*These also include an ASID (Address Space IDentifier) field in the TLB
entries*

—–

ASID gjør at hver TLB entry kan tilhøre en bestemt VM, derav behøver
ikke TLB flushes ved context switch. Dette gjør TLB langt mer effektiv
(gitt at den er stor nok). Man kombinerer gjerne dette med større page
størrelser (f.eks. 2MB istedet for 4KB).

##### Performance Measurements

![image](/home/erikhje/office/kurs/opsys/13-virtualization/tex/../img/25-samples-crop.pdf)

(MERK: presise ytelsesmålinger er utrolig vanskelig siden så mange
forhold spiller inn på resultatet, så ikke se deg blind på disse
resultatene, de er bare en forsiktig indikator)

### Ballooning

##### A Balloon Driver in the VM

![image](/home/erikhje/office/kurs/opsys/13-virtualization/tex/../img/vmware-balloon.png)  
<span><http://www.vmware.com/files/pdf/perf-vsphere-memory_management.pdf></span>

from
<http://www.vmware.com/files/pdf/perf-vsphere-memory_management.pdf>:

> In Figure 6 (a), four guest physical pages are mapped in the host
> physical memory. Two of the pages are used by the guest application
> and the other two pages (marked by stars) are in the guest operating
> system free list. Note that since the hypervisor cannot identify the
> two pages in the guest free list, it cannot reclaim the host physical
> pages that are backing them. Assuming the hypervisor needs to reclaim
> two pages from the virtual machine, it will set the target balloon
> size to two pages. After obtaining the target balloon size, the
> balloon driver allocates two guest physical pages inside the virtual
> machine and pins them, as shown in Figure 6 (b). Here, “pinning” is
> achieved through the guest operating system interface, which ensures
> that the pinned pages cannot be paged out to disk under any
> circumstances. Once the memory is allocated, the balloon driver
> notifies the hypervisor the page numbers of the pinned guest physical
> memory so that the hypervisor can reclaim the host physical pages that
> are backing them. In Figure 6 (b) , dashed arrows point at these
> pages. The hypervisor can safely reclaim this host physical memory
> because neither the balloon driver nor the guest operating system
> relies on the contents of these pages. This means that no processes in
> the virtual machine will intentionally access those pages to
> read/write any values. Thus, the hypervisor does not need to allocate
> host physical memory to store the page contents. If any of these pages
> are re-accessed by the virtual machine for some reason, the hypervisor
> will treat it as normal virtual machine memory allocation and allocate
> a new host physical page for the virtual machine. When the hypervisor
> decides to deflate the balloon — by setting a smaller target balloon
> size — the balloon driver deallocates the pinned guest physical
> memory, which releases it for the guest’s applications. Typically, the
> hypervisor inflates the virtual machine balloon when it is under
> memory pressure. By inflating the balloon, a virtual machine consumes
> less physical memory on the host, but more physical memory inside the
> guest.

## I/O

##### I/O Virtualization

  - It is easy to add more CPUs/CPU-cores

  - It is easy to add more memory

  - *It is not easy to add more I/O capacity...*

Det er selvfølgelig lett å legge til mer diskplass, men vi tenker litt
mer generelt, I/O er mange enheter med en bestemt busstruktur.

*I/O er nok ofte den største flaskehalsen for virtualisering.*

##### I/O Virtualization: The DMA problem

  - VM1 is mapped to 1-2GB of RAM, VM2 is mapped to 2-3GB of RAM

  - VM1 is given direct access to a DMA-capable I/O-device

  - VM1 programs the DMA controller to write to the area 2-3GB of RAM,
    *overwrites VM2’s memory...*

—–

Løsningen er å innføre en IOMMU enhet som virker mye på samme måte som
den MMU vi kjenner fra før.

IOMMU er for øvrig ikke noe helt nytt på x86-arkitekturen, men en
generalisering av de teknologiene som tidligere er kjent som GART
(Graphics Address Remapping Table) og DEV (Device Exclusion Vector).

### IOMMU

![image](/home/erikhje/office/kurs/opsys/13-virtualization/tex/../img/MMU_and_IOMMU.png)

IOMMU virtualiserer adressene for I/O devicene på samme måte som
adressene som kommer fra CPUen, og har TLB akkurat som vanlig MMU.

Dette gjør at IOMMU tillater direkte tilordning av I/O-enheter til
VM’er.

Mao, en IOMMU baserer seg på:

  - I/O page tables som gjør adresseoversetting og *aksess kontroll*

  - En Device table som tilordner I/O-enheter direkte til VM’er

  - En interrupt remapping table for å mappe I/O fra riktig enhet
    tilbake til riktig gjesteoperativsystemet (dvs tilbake til riktig
    VM)

MERK: innføring av IOMMU er ikke uten kostnad, dvs den medfører
forsinkelse grunnet med overhead (og på samme måte som med MMU blir ved
veldig avhengig av at TLB har cachet de fleste entries).

##### Implementations

  - Intel VT-d

  - AMD-Vi

Mao, husk denne tabellen for hardware-støtte for virtualisering:

``` 
          CPU (1st gen)     Memory (2nd gen)    I/O
------------------------------------------------------
AMD       AMD-V             RVI/NPT             AMD-Vi

Intel     VT-x              EPT                 VT-d
```

### SR-IOV

##### SR-IOV

![image](/home/erikhje/office/kurs/opsys/13-virtualization/tex/../img/SR-IOV2.png)  
<span><http://www.youtube.com/watch?v=hRHsk8Nycdg></span>

SR-IOV (Single Root I/O Virtualization) gjør at VM’er kan knyttes
direkte mot nettverkskortet uten at hypervisor behøver være involvert i
pakkeflyten. Dette krever:

1.  nettverkskortet må støtte SR-IOV

2.  BIOS må støtte SR-IOV og dette må være enablet i BIOS

3.  hypervisor må ha driver for nettverkskortet som støtter SR-IOV

4.  VM’en må ha en driver som kan prate med en VF (Virtual Function) på
    nettverkskortet

## Nested Virt.

##### Nested Virtualization

  - A VM with a hypervisor

  - [Enabling Virtual Machine Control Structure Shadowing On A Nested
    Virtual Machine With The Intel® Xeon® E5-2600 V3 Product
    Family](https://software.intel.com/en-us/blogs/2014/12/12/enabling-virtual-machine-control-structure-shadowing-on-a-nested-virtual-machine)

## Containers

##### Containers and DevOps

See [figure at 38:10](https://youtu.be/4DBqIIkHrew?t=2292)

  - Devs can ship containers with all dependencies “directly into
    production”  
    (instead of sysadmin configuring a server with all the dependencies
    needed for the app)

  - Containers is OS-level virtualization (they share the OS):
    *container separation at the syscall interface, VM separation at the
    hypervisor (X86 machine instructions) interface*

Mellom virtuelle maskiner er skillet mye kraftigere enn mellom
containere. Containere er bare en skjermet samling med prosesser som
fortsatt deler operativsystemet med andre containere. Sikkerhetsmessig
betyr det at det en container trenger bare finne en “kernel exploit” for
å få tilgang til host’en den deler med andre containere (og derav få
tilgang til de andre containerne også). Virtuelle maskiner har hver sitt
eget operativsystemet så skal en virtuell maskin få tilgang til
underliggende maskinvare eller andre virtuelle maskiner som den deler
maskinvare med, så må den finne en “hypervisor exploit”. Begge typer
exploit dukker opp med jevne mellomrom (husk: det går ikke an å få til
100% sikkerhet i praksis), det er mye enklere å finne en “kernel
exploit” enn en “hypervisor exploit”.

### Cgroups

##### Cgroups

  - Limits use of resources (“CPU”, memory, I/O, device access, ...)

### Kernel namespaces

##### Kernel namespaces

  - PIDs, net, mount, ipc, ...

### CoW & Union mounts

##### Cow & Union mounts

  - Read-only layers, Copy on Write, White out files

Browse [Use the OverlayFS storage
driver](https://docs.docker.com/storage/storagedriver/overlayfs-driver)

### Windows containers

##### Windows containers

  - Windows containers

  - Hyper-V containers (allows Linux containers on Windows\!)

  - see
    [figure 1](https://msdn.microsoft.com/en-us/magazine/mt797649.aspx)

Windows containere er det vi typisk tenker på som containere. Hyper-V
containere er egentlig en virtuell maskin, men den er en litt spesiell
virtuell maskin siden den kjører et minimalistisk Windows-OS som er
spesielt tilrettelagt for å kjøre en container. Dvs Hyper-V container er
laget for kunder som vil ha den ekstra skjermingen som en VM gir ift en
container. Men siden det her er ett OS sammen med containeren så kan man
da også la det OS’et være Linux og dermed så kan man kjøre Linux
containere på Windows, men merk at dette er egentlig en VM med Linux,
det er IKKE Linux-containere som kjører rett på Windows OS. Det går ikke
an siden containere deler OS, og en Linux container må ha Linux
systemkall-grensesnittet.

[Cgroups, namespaces, and beyond: what are containers made
from?](https://www.youtube.com/watch?v=sK5i-N34im8)

    # 
    # Docker from the Ubuntu repo's is probably old, 
    # add Docker's repo and install from it instead:
    # https://docs.docker.com/install/linux/docker-ce/ubuntu/
    # (install the community edition docker-ce), probably commands:
    sudo apt-get install apt-transport-https ca-certificates curl \
                         gnupg-agent software-properties-common
    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | 
     sudo apt-key add -
    # SJEKK FINGERPRINT MOT DET SOM STÅR PÅ WEBSIDEN: 
    sudo apt-key fingerprint 0EBFCD88
    sudo add-apt-repository "deb [arch=amd64] \ 
     https://download.docker.com/linux/ubuntu \
     $(lsb_release -cs) stable"
    sudo apt-get update
    sudo apt-get install docker-ce docker-ce-cli containerd.io
    
    # before this demo please download the images beforehand:
    docker pull ubuntu
    docker pull prakhar1989/catnip
    docker image ls
    
    # btw, nice cheat sheet at
    # https://github.com/wsargent/docker-cheat-sheet
    
    
    3:30 chroot
    
    cd /tmp/
    mkdir newroot
    mkdir -p /tmp/newroot/{bin,lib,lib64}
    cp -v /bin/bash /tmp/newroot/bin
    # copy into the directores what bash needs:
    ldd /bin/bash
    # on ubuntu 16.04 this is:
    cp /lib/x86_64-linux-gnu/{libtinfo.so.5,libdl.so.2,libc.so.6} /tmp/newroot/lib
    cp /lib64/ld-linux-x86-64.so.2 /tmp/newroot/lib64/
    sudo chroot /tmp/newroot/ /bin/bash
    pwd # funker pga builtin i bash
    ls  # funker ikke
    exit
    
    5:15 cgroups
    
    pstree -p # viser at første prosess, dvs den som styrer alt er systemd
    systemd-cgls # systemd bruker cgroups, merk user.slice, dvs tjenestene 
                 # er egne grupper øverst i treet, mens de som er 
                 # brukerprosesser er under en node i treet
    
    6:45 cgroups: hierarchy, ja det er et nivå høyere enn hva vi så med systemd
    
    sudo apt-get install cgroup-tools
    lscgroup
    lscgroup | grep user
    
    25:00 namespaces
    
    # hva er et "name"? pids, net, mount, ipc, ...
    # demo PID-namespace
    ps -axo pid,command                  # PID namespace
    ip a                                 # net namespace
    mount # evn mount | awk '{print $1}' # mount namespace
    sudo docker run -i -t ubuntu
    apt-get update
    apt-get install figlet
    figlet
    ctrl z
    # i og utenfor container gjør, se forskjellig PID på figlet
    ps -axo pid,command | grep figlet
    
    32:00 copy_on_write
    
    # demo copy_on_write og white_out fil, se figur
    # https://docs.docker.com/storage/storagedriver/overlayfs-driver
    docker run -it ubuntu
    ctrl p-q
    ls -ltr /var/lib/docker/overlay2/diff/
    ls -l /var/lib/docker/overlay2/diff/<den siste>
    docker attach <ID til den du startet>
    echo mysil > hei.txt
    rm root/.profile
    ctrl p-q
    ls -l /var/lib/docker/overlay2/diff/<den siste>
    ls -la /var/lib/docker/overlay2/diff/<den siste>/root
    
    34:00 Capabilities, Apparmor, SElinux
    
    35:38 Oversikt over container runtimes
    
    41:25 Avslutt, resten er hvordan bygge en container manuelt (se selv)
    
    # Docker demo flask app
    docker search catnip
    docker run -p 5000:5000 prakhar1989/catnip
    # gå til http://localhost:5000/
    # se log live i terminal, stop og gjenta med
    docker run -d -p 5000:5000 prakhar1989/catnip
    
    
    # Docker compose demo
    sudo curl -L \
     https://github.com/docker/compose/releases/download/1.21.0/docker-compose-$(uname -s)-$(uname -m) \
     -o /usr/local/bin/docker-compose
    
    $ cat docker-compose.yml 
    nginx:
      image: jwilder/nginx-proxy
      ports:
        - 80:80
      volumes:
        - /var/run/docker.sock:/tmp/docker.sock
    
    wordpress:
      image: wordpress
      environment: 
        VIRTUAL_HOST: foo.bar.com
      links:
        - mysql:mysql
    
    mysql:
      image: mysql
      environment:
        MYSQL_ROOT_PASSWORD: V3kwOL8NE4HWaZFg
    
    $ docker-compose up -d
    $ docker-compose scale wordpress=4
    
    # generer trafikk:
    while true; do curl -s http://foo.bar.com > /dev/null; sleep 0.1; done
    
    # btw, utmerket foredrag om containers intro og security:
    # DEF CON 23 - Aaron Grattafiori - Linux Containers: Future or Fantasy?

## Theory questions

1.  Forklar påstanden til Popek og Goldberg fra 1974: *A machine is
    virtualizable only if the sensitive instructions are a subset of the
    privileged instructions*.

2.  Tanenbaum oppgave 7.16  
    VMware does binary translation one basic block at a time, then it
    executes the block and starts translating the next one. Could it
    translate the entire program in advance and then execute it? If so,
    what are the advantages and disadvantages of each technique?

3.  Forklar hvordan datamaskinarkitekturens beskyttelsesringer
    (*protection rings*) benyttes ved virtualisering når
    virtualiseringsteknikken er binæroversetting (VMware’s teknologi).

4.  Hva karakteriserer en applikasjon som vil være
    utfordrende/problematisk å kjøre på en virtuell maskin?.

5.  Forklar kort fordeler og ulemper med *shadow page tables* i forhold
    til *nested/extended page tables*.

6.  Forklar kort hvordan *IOMMU* kan være nyttig hardwarestøtte for
    virtualisering.

7.  Hvordan forbedrer Docker-containere samarbeidet mellom utvikling og
    drift?

8.  Hva er Linux cgroups? Hva er hensikten med cgroups?

9.  Hva gjør du hvis tjenesten du kjører i en container er avhengig av å
    lagre data lokalt på disk?

## Lab exercises

**Nylig endrede filer.**  
Skriv en PowerShell-kommandolinje som skriver ut Name og LastAccessTime
til alle filer i katalogen du står i som har blitt aksessert siste time.
Utskriften skal være sortert på filstørrelse (Length).

**(OBLIG) Prosesser og tråder.**  
Skriv et PowerShell-script `chromethreads.ps1` (på færrest mulig linjer)
som teller antall tråder hver av `chrome`-prosessene har. Dvs, ved
tilfelle

    PS C:\> Get-Process chrome
    
    Handles  NPM(K)    PM(K)      WS(K) VM(M)   CPU(s)     Id  SI ProcessName
    -------  ------    -----      ----- -----   ------     --  -- -----------
        223      20    52732      67144   793     1,58   2676   1 chrome
       1031      38    36596      83332   353     5,00   4332   1 chrome
        130       8     1372       5128    80     0,02   4660   1 chrome
        244      23    90424     108560   847     3,83   4780   1 chrome
        356      35    50336      65540   277     3,50   4808   1 chrome
    
    PS C:\> Get-Process | Get-Member
    ...
    Threads  Property  System.Diagnostics.ProcessThreadCollection Threads {get;}
    ...

Så skal ditt script oppføre seg slik:

    PS C:\> chromethreads.ps1
    chrome 2676 9
    chrome 4332 38
    chrome 4660 7
    chrome 4780 12
    chrome 4808 8

**(OBLIG) Informasjon om deler av filsystemet.**  
Skriv et script `fsinfo.ps1` som tar en directory som argument og
skriver ut

Hvor stor del av partisjonen directorien befinner seg på som er full

Hvor mange filer (utelat alle directories\!) finnes i directorien (inkl
subdirectorier), gjennomsnittlig filstørrelse, og full path og størrelse
til den største filen

Eksempel kjøring:

    $ fsinfo.ps1 cf3
    Partisjonen cf3 befinner seg på er 81.4456135970535% full
    Det finnes 4 filer.
    Den største er C:\Users\erikh\scripts\cf3\helloworld2.cf 
    som er 302B stor.
    Gjennomsnittlig filstørrelse er 245.25B.
    $ pwd
    
    Path
    ----
    C:\Users\erikh\scripts
    
    
    $ fsinfo.ps1 $(pwd)
    Partisjonen C:\Users\erikh\scripts befinner seg på er 
    81.4456135970535% full
    Det finnes 479 filer.
    Den største er C:\Users\erikh\scripts\Lessmsierables-20050611
    \LessMSIerables\wix.dll som er 992KB stor.
    Gjennomsnittlig filstørrelse er 12.4778509916493KB.
    $

**Regulære uttrykk anvendt på filsystemet.**  
Skriv et script `fnamecheck.ps1` som tar en directory som argument og
skriver ut alle filnavn (inkl alle subdirectories) som inneholder norske
tegn og/eller mellomrom i filnavnet.  
(repeter gjerne regexp med `Get-Help about_regular_expressions`)

# Objektsikkerhet

## Introduction

##### Introduction

  - Security is challenging.

  - Security is usually in conflict with *user-friendliness*.

Vi skal adressere sikkerhet knyttet til operativsystemer.

Opprinnelig (60-70 tallet) var ikke datamaskiner knyttet til nettverk så
sikkerhet dreide seg om å sørge for at brukerne ikke kunne få tilgang
til hverandres filer, og det er i stor grad dette
operativsystemsikkerhet dreier seg om.

I det datamaskiner kom på nett og Internet utviklet seg kom de store
nettverkssikkerhet-utfordringene og disse henger tett sammen (er
uadskillelige) med operativsystemsikkerhet.

Kapittel 9 har derfor en del temaer som ikke er typisk
operativsystemsikkerhet, vi skal ta litt lett på disse.

### CIA

##### Security Goals and Threats

![image](/home/erikhje/office/kurs/opsys/14-object-protection/tex/../../resources/MOS_4e/9-1.pdf)

Husk CIA\!

Her kunne man også nevnt *Privacy* (Personvern) som også er et mål, men
som ikke er trivielt siden det ofte er i konflikt med *Forensics*, dvs
dataetterforskning, ref. den store debatten om datalagringsdirektivet.

### Active adversaries

##### Botnets...

![image](/home/erikhje/office/kurs/opsys/14-object-protection/tex/../img/botnet.png)

Dette er ikke en statistikk å være stolte av...

##### Intruders and Adverseries

1.  Casual prying by nontechnical users.

2.  Snooping by insiders.

3.  Determined attempts to make money.

4.  Commercial or military espionage.

—–

*Adversary* er et mye brukt begrep innen informasjonssikkerhet og kan
brukes om motstander, rival eller fiende.

Husk at hvor mye ressurser som settes inn i sikkerhet må svare til
trusselnivået...

Og et problem er at det bare er den som rammes som er villig til å
betale, f.eks. er det vanskelig å få PC-brukere til å beskytte seg
tilstrekkelig mot Botnet trusselen fordi de selv ikke blir rammet men
PCn dere brukes til å ramme andre.

##### Estonia incident...

![image](/home/erikhje/office/kurs/opsys/14-object-protection/tex/../img/estonia.png)

##### Georgia incident...

![image](/home/erikhje/office/kurs/opsys/14-object-protection/tex/../img/georgia.png)

Mao, det har nå vært “cyber krig” i mange år allerede, og siden 2010 har
Norge bygd Cyberforsvaret som landets fjerde forsvarsgren (de tre andre
er hæren, sjøforsvaret, luftforsvaret).

### Accidents

##### Accidental Data Loss

1.  Acts of God.

2.  Hardware or software failure.

3.  Human errors.

—–

Det meste ordnes med gode backup/restore rutiner. Husk at sletting av
egne filer er mest vanlig årsak til restore.

Dette er en del av det store sysadm/security management bilde og kan
lett glemmes når man blir for teknisk fokusert på ren sikkerhet, backup
på egen harddisk virker høl i hue men løser altså de fleste kriser for
brukerne.

### Crypto - symm

##### Remember Crypto?

![image](/home/erikhje/office/kurs/opsys/14-object-protection/tex/../../resources/MOS_3e/09-02.pdf)

Hvis \(K_{E}=K_{D}\) er det symmetrisk krypto, hvis i stedet vi prater
om et nøkkelpar \((K_{E},K_{D})\) bestående av en privat og en offentlig
(public) nøkkel, så er asymmestrisk krypto (public-key crypto).

Eksempler på symmetriske algoritmer som brukes i dag er 3DES, AES,
Blowfish og Twofish.

### Crypto - Public Key / asymm crypto

##### crypto

  - What is \(314159265358979 * 314159265358979\)

  - What is \(\sqrt{3912571506419387090594828508241}\)

Asymmetrisk krypto, altså kryperting/dekryptering som bruker en nøkkel
til kryptering og en annen til dekryptering utnytter at det er lettere å
multiplisere to svære tall enn det er å faktorisere ett stort tall.

RSA er ett eksempel på en algoritme som benytter ulike nøkler for
kryptering og dekryptering.

### Crypto - hashes

##### Hash-functions

  - A cryptographic one-way function, which produces a fixed-length
    output, based on a variable length input.

  - Even a small change in input changes the output a lot.

  - Difficult to reverse

I tillegg må vi kjenne til hva en hash funksjon er, f.eks.  
`echo "erikh" | sha512sum`.

  - symmetrisk krypto (`gpg -c`, `gpg -d`)

  - asymmetrisk krypto

  - hash-funksjon (message digest)

samt

  - digital signatur

  - Trusted Third Party (TTP)

  - sertifikat (se i browsern din)

  - PKI infrastruktur

##### Kerckhoffs’ Principle from 1883

*A cryptosystem should be secure even if everything about the system,
except the key, is public knowledge*

as opposed to “Security by obscurity”

Poenget er at sikkerhet må aldri være basert på hemmelighold av
implementasjonen, dvs forsøker noe å selge deg et kryptosystem som ikke
er basert på kjente og offentlige tilgjengelige kryptoalgoritmer så skal
du være meget skeptisk.

### Crypto - hw - trust

##### Trusted Platform Module (TPM)

  - A cryptoprocessor with nonvolatile storage.

  - Allows for fast crypto and storage of keys.

  - Controversial because of possible use for OS running only authorized
    software (e.g. only Microsoft-approved software).

—–

TPM er altså et forslag fra industrien på en måte å oppbevare nøkler
sikkert på et usikkert (untrusted) system.

## Protection Mechanisms

### Protection domain

##### Three Protection Domains

![image](/home/erikhje/office/kurs/opsys/14-object-protection/tex/../../resources/MOS_3e/09-04.pdf)

Et (beskyttelses-) domene er et sett med (objekt,rettigheter) par.

Et domene defineres ut fra hva en prosess har tilgang til, f.eks. i UNIX
vil (UID,GID på Linux, SID/SID på Windows) paret definere et domene.

For best mulig sikkerhet er POLA (Principle of Least Authority, også
kalt need to know) et godt prinsipp.

Alle systemkall forårsaker en domeneswitch siden operativsystemet i
kernelmode har tilgang til mye mer enn en vanlig prosess i usermode.

### Protection matrix

##### A Protection Matrix

![image](/home/erikhje/office/kurs/opsys/14-object-protection/tex/../../resources/MOS_3e/09-05.pdf)

Operativsystemet kan holde oversikt hvilke objekter som hører til hvilke
domener med en matrise.

Figuren viser matrisen tilsvarende forrige figur.

##### Adding Domains as Objects

![image](/home/erikhje/office/kurs/opsys/14-object-protection/tex/../../resources/MOS_3e/09-06.pdf)

Domenene kan tas med som objekter og en rettighet kan være å tillate
domeneswitch som i figuren hvor en prosess i domene 1 kan svitsje til
domene 2 men ikke tilbake.

### ACL

##### Access Control Lists

![image](/home/erikhje/office/kurs/opsys/14-object-protection/tex/../../resources/MOS_3e/09-07.pdf)

Matrisen fra de to forrige figurene er lite praktisk å implementere, i
praksis lagrer vi enten radene eller kolonnene (og da bare de ikke-tomme
elementene).

  - Å lagre kolonnene kalles *Access Control List*.

  - A lagre radene kalles *Capability*.

Eierne/brukerne (som definerer domenene) kalles som regel
*subjects/principals* og tilgangen kontrolleres til *objects* (som regel
er objektene filer av en eller annen type, men kan være hardware
devicer, semaforer, el.l.).

Merk at tilgang bestemmes utfra en bruker (med UID/GID), ikke ut fra en
prosess (PID).

Man kan dele inn i generelle rettigheter (opprett eller slett objekt) og
objekt-spesifikke rettigheter (sorter alfabetisk innholdet i directory).

Figuren viser inndeling i kernel space og user space for å poengtere at
det er operativsystemet i kernel space som skal utføre aksess
kontrollen.

*MEN MERK: aksesskontrollmekanismene som vi nå snakker om, altså det å
kontrollere tilgangen fra et subjekt til et objekt, er en helt adskilt
mekanisme fra usermode/kernelmode/protection rings.*

##### ACLs with Groups

![image](/home/erikhje/office/kurs/opsys/14-object-protection/tex/../../resources/MOS_3e/09-08.pdf)

Som oftest har man både en bruker og en gruppe, og med gruppen innføres
konseptet *rolle* (siden grupper som regel brukes til å representere
roller).

Grupper benyttes som regel ofte via wildcard:  
`tana,*:RW`  
(dvs, i dette tilfelle er det det samme hvilken gruppe tana tilhører)

Alle utenom `mysil` skal ha tilgang:  
`mysil,*:(none); *,*:RW`

Entries (ACEer) i en ACL scannes i rekkefølge. DENY-entries står alltid
først, og scanningen av listen avsluttes så fort en DENY-entry matcher.
Hvis ikke så er tilfelle, scannes ALLOW-entriene inntil man har funnet
ALLOW for alle rettigheter som søkes etter.

En annen måte å benytte grupper på er å bare angi enten bruker eller
gruppe:  
`debbie:RW; phil:RW; pigfan:RW`

DEMO:

    echo mysil > a.txt
    (Get-Acl a.txt).Access | ft 
    # fjern mine rettigheter ved å legge til en Deny regel
    $acl=Get-Acl a.txt
    $rule=new-object System.Security.AccessControl.FileSystemAccessRule
     ("$env:USERDOMAIN\$env:USERNAME","FullControl","Deny")
    $acl.SetAccessRule($rule)
    Set-Acl a.txt $acl
    echo mysil > a.txt
    rm a.txt # hæ? hvorfor fikk jeg lov til det?
    
     
    # tilsvarende med cmd-kommander:
    # icacls a.txt
    icacls /? # scroll til listen over hva entriene betyr
    # fjern mine rettigheter:
    icacls a.txt /deny WIN-2LTE3ENH06A\"Erik Hjelmås":`(F`)
    echo mysil > a.txt
    rm a.txt # hæ? hvorfor fikk jeg lov til det?
    echo mysil > a.txt
    icacls a.txt
    #
    # hvis jeg skal fjerne Inherited-rettigheter må jeg fjerne 
    # inheritance først:
    icacls a.txt /inheritance:d

### Capabilities

##### Capabilities

![image](/home/erikhje/office/kurs/opsys/14-object-protection/tex/../../resources/MOS_3e/09-09.pdf)

Figuren viser tre prosesser med hver sin *capability list* som
inneholder *capabilities*.

En capability består altså av en identifikator for filen (objektet) og
et sett rettigheter.

Capabilities er mer effektive enn ACL fordi man slipper å søke gjennom
en potensielt lang ACL liste, men et stort problem med capabilities er
at det kan være tungvint å slette en fil (objekt) eller trekke tilbake
rettighetene til en bestemt fil (objekt) siden det da må søkes gjennom
alle subjekter (brukere).

Skumles 9.2.1 i Tanenbaum om utviklingen av email og www, *kompleksitet
(for mange nye “features”) er den største fienden til sikkerhet*.

### Reference monitor

##### A Reference Monitor

![image](/home/erikhje/office/kurs/opsys/14-object-protection/tex/../../resources/MOS_3e/09-11.pdf)

Det opprinnelige sikkerhetsarbeidet fra 60 og 70 tallet resulterte
delvis i the Orange book:

[Trusted Computer System Evaluation Criteria
(TCSEC)](https://csrc.nist.gov/csrc/media/publications/conference-paper/1998/10/08/proceedings-of-the-21st-nissc-1998/documents/early-cs-papers/dod85.pdf)

I et trusted system skal det være en Reference monitor.

I Windows heter denne eksplisitt “Security Reference Monitor”, men denne
komponenten finnes i alle moderne operativsystemer som kontrollerer
tilgang fra subjekt til objekt.

Samlingen av all hardware og software som er involvert i det å sørge for
at sikkerhetsreglene (security policy) overholdes, kalles *trusted
computing base*. Trusted computing base bør være så liten som mulig
(minst mulig kompleksitet), slik at den er en grunnleggende
sikkerhetsfunksjon som kan stoles på.

##### Security Reference Monitor in Windows Vista

![image](/home/erikhje/office/kurs/opsys/14-object-protection/tex/../../resources/MOS_3e/11-13.pdf)

Figuren viser at Windows Vista har en egen del av operativsystemet som
kalles Security Reference Monitor.

### Authorized States

##### Authorized States

![image](/home/erikhje/office/kurs/opsys/14-object-protection/tex/../../resources/MOS_3e/09-12.pdf)

Beskyttelsesmatrisen kan betraktes som en tilstand for systemet. En
*sikkerhetspolicy* kan da defineres som et sett med regler som skiller
de sikre fra de usikre tilstandene.

Figuren viser et system som går fra en sikker til en usikker tilstand
fordi bruker Robert har fått lesetilgang til Henry’s mailboks.

### Multilevel security

##### Multilevel Security

  - Discretionary Access Control (DAC)  
    The users decide access control.

  - Mandatory Access Control (MAC)  
    The system decide access control.

—–

MAC har vært lite i bruk, men hadde stort fokus i det militære spesielt
og finnes som opsjoner i mange operativsystemer (f.eks. som
kjernemoduler på Linux). Det er dog på vei oppover gjennom prosjekter
som AppArmor og SELinux, da behovet for sikre systemer øker år for år.

#### Bell-La Padula & Biba

##### Bell-La Padula

  - Simple security property (no-read-up)  
    A process running at security level \(k\) can read only objects at
    its level or lower.

  - \* property (no-write-down)  
    A process running at security level \(k\) can write only objects at
    its level or higher.

—–

En velkjent MAC policy er *Bell-La Padula* som sørger for å holde på
hemmeligheter, og er rettet mot militære anvendelser. I denne modellen
vil ikke informasjon kunne flyte nedover i systemet, dvs top secret info
kan ikke skrives ned til en som ikke har top secret klarering.

##### Bell-La Padula

![image](/home/erikhje/office/kurs/opsys/14-object-protection/tex/../../resources/MOS_3e/09-13.pdf)

Når det modellers slik, kan vi se om Bell-La Padula er overholdt ved å
sjekke at ingen piler peker nedover.

##### Biba

  - Simple integrity property (no-write-up)  
    A process running at security level \(k\) can write only objects at
    its level or lower.

  - Integrity \* property (no-read-down)  
    A process running at security level \(k\) can read only objects at
    its level or higher.

—–

*Biba*-modellen sørger for at integritet ivaretas men er da det stikk
motsatte av Bell-La Padula (\!).

### Covert channels

##### Covert Channel Model

![image](/home/erikhje/office/kurs/opsys/14-object-protection/tex/../../resources/MOS_3e/09-14.pdf)

Covert Channel er altså en skjult kanal som kan lekke informasjon.

Hvordan kan vi unngå at serverprosessen gir bort informasjon til
collaborator prosessen?

Vi kan hvertfall sørge for aksess kontroll og stenge for IPC osv.

##### Covert Channel Using File Locking

![image](/home/erikhje/office/kurs/opsys/14-object-protection/tex/../../resources/MOS_3e/09-15.pdf)

Men server prosessen kan alltids lekke informasjon ved alle mulige
systemoppgaver som klokkes og som kan overvåkes av andre prosesser,
eller ved systematisk fillåsing (som i figuren) som kan observeres av
collaborator, eller sikkert mange andre muligheter ...

Å finne (og unngå) alle covert channels er tilnærmet håpløst ...

## Windows

### Fundamentals

##### Access Token

![image](/home/erikhje/office/kurs/opsys/14-object-protection/tex/../../resources/MOS_3e/11-47.pdf)

En *SID* (Security ID) for hver bruker og gruppe, skal være globalt
unike.

En prosess har en access token som vist i figuren med:

  - Header  
    adm info.

  - Expiration time  
    brukes ikke.

  - Groups  
    gruppetilhørighet, benyttes av POSIX støtten (husk: primær
    gruppetilhørighet er et konsept om benyttes av POSIX men ikke av
    Windows).

  - Default ACL  
    standard DACL (tilsvarer `umask` på linux) som settes på objekter
    prosessen oppretter hvis ikke annet angis spesielt (det finnes også
    en default gruppe-SID som angir hvilken gruppe som skal settes som
    eier av objektet).

  - User SID  
    angir eier av prosessen.

  - Group SID  
    angir gruppetilhørighet (SID til de gruppene som prosessen
    tilhører).

  - Restricted SIDs  
    angi prosesser som kan delta i utførelsen med begrensede
    rettigheter.

  - Privileges  
    Spesielle rettigheter knyttet til en bruker (*en aksesskontroll på
    oppgaver istedet for mot objekter*). Privileges er en måte å gi bort
    deler av rettigheten en administrator har (f.eks. shutdown av
    maskina, endre tidssone).

  - Impersonation level  
    en access token kan brukes på vegne av noen andre (typisk hos en
    server på vegne av en klient), dette angis her.

  - Integrity level  
    Low, Medium, High og System (muligens untrusted og trusted installer
    også), innført i Vista brukes som MAC (benyttes spesielt for å sette
    lav integritet på internet explorer slik at ondsinnet kode via IE
    ikke kan skrive over systemfiler).

Windows har altså *Privileges*:  
<http://msdn.microsoft.com/en-us/library/bb530716(VS.85).aspx>

DEMO:

    whoami /all

(merk at `BUILTIN\Administrators` gruppen har en litt rar beskrivelse,
det kommer vi straks tilbake til ifb med UAC)

##### Security Descriptor

![image](/home/erikhje/office/kurs/opsys/14-object-protection/tex/../../resources/MOS_3e/11-48.pdf)

En fil (objekt) har en security descriptor som angir eier,
gruppetilhørighet, og DACL (som scannes i rekkefølge ved aksess
kontroll) og SACL som angir hva som skal logges spesielt for dette
objektet. SACL angir også integritetsnivå som vi ser mer om snart.

Objekter som lagres i NTFS (filer og directories) har [mange mulige
rettigheter som ofte vises gruppert i "basic
permissions"](https://www.ntfs.com/ntfs-permissions-file-folder.htm).
NTFS-rettighetene er ikke nødvendigvis lik rettighetene til andre typer
objekter, la oss sammenlikne de med rettighetene knyttet til nøkler i
registry:  
`((Get-Item filnavn).GetAccessControl()).Access` og bytt ut `filnavn`
med f.eks. `HKLM:\SYSTEM\CurrentControlSet` (HKLM er HKEY LOCAL MACHINE
registrydelen som i powershell blir gjort tilgjengelig som stasjonen
`HKLM:\`)

### System calls

##### Principal Win32 API Functions for Security

![image](/home/erikhje/office/kurs/opsys/14-object-protection/tex/../../resources/MOS_3e/11-49.pdf)

### Implementation

##### Login

1.  CTRL-ALT-DEL (Secure Attention Sequence) initiate winlogon

2.  Winlogon uses lsass to authenticate

3.  User ends up with the GUI shell explorer process with an access
    token

—–

CTRL-ALT-DEL (secure attention sequence) benyttes for å sikre at man
logger på via winlogon prosessen, lsass bruker SECURITY og SAM kubene
(hives) av registry for å sjekke pålogging og ved godkjent pålogging
startes et grafisk shell explorer.exe med tilhørende access token.

All videre aksess kontrol (dvs hver gang en prosess forsøker bruke et
objekt) gjøres av Security Reference Monitor som vist tidligere.

#### MIC

##### Mandatory Integrity Control

  - Processes have an integrity level (low, medium, high, system) in
    their access token

  - Objects have an integrity level in the SACL of their security
    descriptor

  - *The Security Reference Monitor (SRM), before going to DACL, checks
    SACL and allows a process to write or delete an object only if its
    integrity level is greater than or equal to that of the object
    (no-write-up like Biba)*

  - Processes cannot read process objects at a higher integrity level
    either (a limitedd no-read-up, a bit like Bell-LaPadula)

—–

Også kalt “Windows Integrity Levels”

Hvert integritetsnivå har sin SID: Low (SID: S-1-16-4096), Medium (SID:
S-1-16-8192), High (SID: S-1-16-12288), and System (SID: S-1-16-16384).

(Man kan ikke dynamisk endre integritetsnivå, dvs forsøk å endre
integritetsnivå på internet explorer, og du må restarte prosessen for at
endringen skal ta effekt)

Merk: Privileges og Integrity levels kan begge benyttes da til å
overstyre ACL.

DEMO (jeg har lov i ACL, men blir overstyrt):

    cd /Users/erikh
    powershell
    whoami /all                    # jeg er på medium
    Write-Output mysil > mysil.txt
    exit
    psexec -l powershell           # kjør powershell på lav
    whoami /all                    # jeg er på lav
    Write-Output solan > solan.txt # ikke lov fordi:
    accesschk -d -v .              # min homedir er på medium
    # venter på at PowerShellAccessControl publiseres på gallery

#### UAC

##### User Account Control (UAC)

The problem: *Software developers assume their application will run as
administrators on Windows.* UAC tries to promote change:

  - All admin accounts are launched with standard user privileges
    
      - *Membership in admin group marked DENY*
    
      - *Privilege set reduced to standard user set*

  - File system and registry namespace virtualization used for legacy
    application

—–

God artikkel som forklarer dette i detalj:  
<http://blogs.technet.com/markrussinovich/archive/2007/02/12/638372.aspx>

Når man logger på og startet prosesser som administrator, startes disse
med en aksess token som har begrensede rettigheter. Prosessen kan be om
økte rettigheter via “Run as administrator” eller ved at det er kodet i
applikasjon (“trustinfo” -tag som sier noe om “requestExecutionLevel” i
“application manifest”).

Gammeldagse applikasjoner som antar at de har admin-rettigheter og ikke
sier noe om “elevation” trenger file system og registry namespace
virtualisering for å funke skikkelig med begrensede rettigheter.

DEMO: - `whoami /all` og `accesschk -f -p powershell` i powershell
startet som adm og uten, sammenlikne like SID (merk forskjellen på
`BUILTIN\Administrators`) og privilege lista.

    taskmgr
    cd c:\windows
    Write-Output tiger > woods.txt (access denied)
    # høyreklikk i taskmgr, UAC virtualization på powershell.exe
    Write-Output tiger > woods.txt
    Get-Content woods.txt
    # UAC virtualization på cmd.exe OFF
    Get-Content woods.txt
    cd $env:LocalAppData
    cd VirtualStore\Windows
    Get-Content woods.txt

## Linux

### Fundamentals

##### File Permissions

![image](/home/erikhje/office/kurs/opsys/14-object-protection/tex/../../resources/MOS_3e/10-37.pdf)

Samme for filer og directories men for directories betyr x bit’n søk i
stedet for execute.

I tillegg har vi SetUID, SetGID og sticky bit.

SetUID er mye benyttes f.eks. `passwd` som må kunne skrive til
/etc/passwd men det har egentlig bare root lov til. Via disse innføres
konseptet *effective UID*.

Sticky bit benyttes for å ha felles kataloger hvor alle kan skrive til,
men man ikke kan slette hverandres filer (f.eks. `/tmp`).

Merk: *I Linux er dette altså en fast ACL med tre entries*. Det er
støtte for POSIX ACL i Linux også men ikke så mye brukt foreløpig (dvs
den er nok mye brukt i litt større sammenheng, men ikke på personlige
PCr typisk).

##### Capabilities

  - `man capabilities`, see “Capabilities list”

  - “Starting with kernel 2.2, Linux divides the privileges
    traditionally associated with superuser into distinct units, known
    as capabilities, which can be independently enabled and disabled.
    Capabilities are a per-thread attribute.”

from <http://blog.siphos.be/2013/05/capabilities-a-short-intro/>:

    getcap -r /usr
    ls -l $(which ping)
    ping 128.39.141.1
    cp /bin/ping myping
    chmod +x myping
    ./myping 128.39.141.1
    sudo setcap cap_net_raw+ep myping
    ./myping 128.39.141.1

Capabilities are similar to Windows privileges but belong to executables
instead of users (Windows privileges belong to accounts).

BUT note that both Linux Capabilities and Windows privileges are
different from the capabilities we talked about earlier as an
alternative to ACL’s.

### System calls

##### Security System Calls

![image](/home/erikhje/office/kurs/opsys/14-object-protection/tex/../../resources/MOS_3e/10-38.pdf)

`chmod` mest benyttet.

De tre siste kan bare root utføre.

### Implementation

##### Implemention

1.  Login checks username/password and groups
    
      - `/etc/passwd`, `/etc/shadow`
    
      - `/etc/groups`

2.  Starts shell with users UID, GID

3.  *sudo* similar to UAC

—–

Linux har altså en mye enklere sikkerhetsmodell enn Windows i
utgangspunktet, men det er fullt mulig å utvide linux med
sikkerhetsmoduler i kjernen som skaper mere avanserte sikkerhetsmodeller
(søk gjerne på SELinux).

## Theory questions

1.  Nevn fordeler og ulemper med aksesskontroll-lister i forhold til
    capability lister.

2.  Forklar kort hva *DAC* (Discretionary Access Control) og *MAC*
    (Mandatory Access Control) er. Gi eksempler.

3.  Forklar kort innholdet i minst fem av feltene i en *Windows Access
    Token*.

4.  Forklar kort hvordan *User Account Control* på Windows fungerer.

5.  Forklar kort forskjellen på filrettigheter i Unix/Linux og ACL’er
    knyttet til filer i Windows.

6.  Gitt følgende seanse i Bash-kommandolinje:
    
        $ ls -l mypw 
        ---------- 1 root root 129824 mai   11 10:16 mypw
        $ XXXXXXXXXXXXXXX
        $ ls -l mypw 
        -rwsr-xr-x 1 root root 129824 mai   11 10:16 mypw
    
    Hvilken kommando har blitt gitt der det står `'XXXXXXXXXXXXXXX'`?
    Begrunn svaret.

## Lab exercises

1)  Du er systemadministrator for en felles filserver som benyttes av
    studenter og ansatte. Du skal lage brukerne `ola`, `eli`, `ina` og
    `ali`, samt gruppene `studenter` med ola og eli, og gruppen
    `ansatte` med ina og ali. Anta at det er mange flere brukere i disse
    gruppene enn de som nevnes her. Gjør dette ved å skrive et script
    (PowerShell (gjerne med cmdlet `Import-CSV`) for Windows og Bash for
    Linux) som utfører oppgaven ved å lese inndata fra filen  
    <https://folk.ntnu.no/erikhje/opsys/users.csv>
    
    (Hint: bruk  
    `useradd -p KRYPTERTPASSORD -G GRUPPE BRUKERNAVN`  
    for å opprette en bruker på Linux, og for å lage det krypterte
    passordet kan du bruke  
    `openssl passwd -1 PASSORD` )
    
    (NB\! hva slags sikkerhetsproblem kan du ha hvis en annen bruker
    lister ut alle detaljer om alle kjørende prosesser/tråder mens du
    gjør kommandoene over?)
    
    Husk å slette alle disse brukerene etterpå.

2)  Lag directory `/emner` med følgende rettigheter:
    
      - ina har alle rettigheter
    
      - studenter har kun lese (dvs kan liste opp innholdet i
        directorien) rettigheter
    
      - ingen andre har noen rettigheter

3)  Lag directory `/prosjekt` med følgende rettigheter.
    
      - ina har alle rettigheter
    
      - studenter har alle rettigheter utenom å slette
    
      - ingen andre har noen rettigheter

4)  Lag directory `/parlament` med følgende rettigheter.
    
      - ola og eli har alle rettigheter
    
      - alle studenter har lese rettigheter
    
      - ali har lese rettigheter
    
      - ingen andre har noen rettigheter

5)  Hvilke av oppgavene ovenfor fikk du ikke til på Linux/Bash (med
    tradisjonell Unix/Linux sikkerhetsmodell)?

6)  Benyttes fil-eier aktivt i aksesskontrollen på samme måte i
    Unix/Linux og i Windows?

# Malware og minnesikkerhet

## Exploiting

##### General Attack Script

1.  Run an automated port scan to find machines that accept telnet
    connections.

2.  Try to log in by guessing login name and password combinations.

3.  Once in, run the flawed program with input that triggers the bug.

4.  If the buggy program is SetUID root, create a SetUID root shell.

5.  Fetch and start a zombie program that listens to an IP port for
    commands.

6.  Arrange that the zombie program is always started when the system
    reboots.

—–

Slike rene Attack scripts som vist her er ikke så vanlige lenger, det er
mer fokus på brukeren og lure brukeren til å kjøre malware, men som
oftest er det er kombinasjon, dvs lure brukeren til å besøke et nettsted
som inneholder kode som utnytter en svakhet i nettleseren.

Vi si at vi har en *ondsinnet hacker* eller *motstander (adversary)*
generelt som ønsker å få kjøre kode på vår datamaskin med mest mulig
rettigheter, ved å:

  - få oss til å kjøre kode (“klikk på vedlegg”)

  - få oss til å besøke et nettsted som utnytter en sårbarhet i
    nettleseren vår og kode kjøres uten at vi er klar over det

  - helautomatisk angrep på datamaskina vår uten at vi gjør noenting
    selv, slik som i eksempelet “General attack script”

### Buffer Overflows

##### A Simple Log Procedure in C

    01 void A( ) {
    02   char B[128];                  /* space for 128 B on stack */ 
    03   printf ("Type log message:"); 
    04   gets (B);                     /* read into buffer */ 
    05   writeLog (B);                 /* output to the log file */
    06 }

Hva skjer om brukeren skriver inn mer enn 128 bytes?

##### Buffer Overflow Attacks

![image](/home/erikhje/office/kurs/opsys/15-malwareandmemory/tex/../../resources/MOS_3e/09-24.pdf)

Veldig mange angrep skyldes dårlig koding i programmeringsspråket C
eller C++. C/C++-kompilatorer sjekker ikke array-grenser, og det er
derfor lett å skrive over andre deler av minne enn det man hadde tenkt.

Figuren viser:

(a) et vanlig program

(b) en funksjon er blitt kallet og allokert plass til en lokal variabel
B i funksjonen, B er f.eks. et buffer på 256 char for å holde et
filnavn.

(c) brukern angir filnavnet med 1500 tegn, hvorav filnavnet er meget
spesielt, det inneholder i starten ondsinnet kode og eksakt så langt ut
i filnavnet at B og evn andre lokal variable samt EBP (forrige stack
base pointer) er passert, så inneholder den returaddresse til starten av
den ondsinnede koden som overskriver den opprinnelige returaddressen.
Mao et buffer overskrives slik at man tar kontroll på returaddressen og
da altså endrer programflyten (istedet for å returnere til main hvor
funksjonen ble kallet fra returnerer man til den ondsinnede koden).

Dette er spesielt ille hvis programmet som har denne feilen er SetUID
root (dette er det klassiske unix exploit fra 80 og 90 tallet) på Linux
eller hvis brukeren kjører med administrator rettigheter på Windows
(husk mekanismene MIC og UAC på Windows fra forrige tema).

##### Heap Spraying

An attacker does not have to know where exactly to return to if attack
is based on a *nop sled*.

When this is applied to the heap (data segment in memory) it is called
*heap spraying*.

Så lenge en returnerer til et sted hvor det er nop (no-operation)
instruksjoner så ville disse utføres i sekvens helt til CPUn kommer til
noen andre instruksjoner. Så ved å fylle et stort minneområde med
nop-instruksjoner så er det bare å få programmet til å returnere et
eller annet sted på denne “nop-sleden” og så vil den føre til den
exploitkoden som agriperen har skrevet inn ved enden av nop-sleden.

##### Defence: Stack Canary

![image](/home/erikhje/office/kurs/opsys/15-malwareandmemory/tex/../img/canary.jpg)

"At places where the program makes a function call, the compiler inserts
code to save a random canary value on the stack, just below the return
address. Upon a return from the function, the compiler inserts code to
check the value of the canary. If the value changed, something is wrong"
(Tanenbaum side 643), se også

<http://en.wikipedia.org/wiki/Buffer_overflow_protection#Canaries>

og godt forklart med assembly her:

<https://xorl.wordpress.com/2010/10/14/linux-glibc-stack-canary-values/>

Men kort og godt, kode settes altså inn ved slutten av funksjoner rett
før de skal returnere.

##### Function Pointers

Stack canaries protect protect return addresses, but there are other
addresses an attacker can overwrite, e.g. *function pointers*.

(Tanenbaum, page 644)

##### Defence: Data Execution Prevention (DEP)

Memory should be `W^X` (`W XOR X`): either writeable og executable,
*never both*\!

<http://en.wikipedia.org/wiki/NX_bit>

`->` *gjøre stacken ikke-eksekverbar* (vanlig idag, hardware støttet på
INTEL via NX bit’et).

I tillegg advarer de fleste kompilatorer deg mot bruk av de funksjonene
som typisk fører til buffer overflow muligheter (`gets` er kanksje den
mest klassiske). Noen kompilatorer legger også på beskyttelsesmekanismer
(`man gcc` og søk etter `stack-protector`).

Det beste er hvis man kan unngå å bruke de funksjonene i C/C++ som kan
føre til buffer overflow, en god oversikt finnes på “Security
Development Lifecycle (SDL) Banned Function Calls”:
<http://msdn.microsoft.com/en-us/library/bb288454.aspx>

##### Return to Libc Attacks

Why write any executable code into memory when the standard libraries
already mapped into memory contains all the functions we need
(*system()*, *mprotect()*, ...)?

Bypasses DEP\!

Merk: stack canaries vil beskytte returadressen her også, men som nevnt,
hvis det er en annen adresse enn returadressen som benyttes for å ta
over programflyten så vil dette angrepet virke.

##### Return-Oriented Programming

![image](/home/erikhje/office/kurs/opsys/15-malwareandmemory/tex/../../resources/MOS_4e/9-23.pdf)

Return-oriented programming er en generalisering av return-to-libc. Se
også

<https://www.blackhat.com/html/bh-usa-08/bh-usa-08-archive.html#Shacham>

##### Defence: Address Space Layout Randomization (ASLR)

*Randomize the addresses of functions and data between every run of the
program.*

God beskyttelse, men ofte mulig å finne måter å forbigå denne mekanismen
på også, se Tanenbaum side 647-648.

### Format Strings

##### Format String Attacks

  - `printf, fprintf, snprintf, ...`

  - `syslog, ...`

  - `printf("%.50d%n",1,&i)` prints `1` but sets `i=50` \!

  - what if the format string is supplied by the user and the user also
    knows about `%m$` (`man 3 printf`).

##### Format String Attacks

"Was it a good idea to include this feature in printf? Definitely not,
but it seemed so handy at the time. *A lot of software vulnerabilities
started like this*."

(Tanenbaum page 651)

En ’Format string’ (formatteringsstreng) er altså en char string som
inneholder formateringsinformasjon, dvs hvordan den skal se ut når den
skrives ut. Formateringsinformasjonen angis som spesialtegnsekvenser
(f.eks. `%.2f` som sier ’rund av til to desimaler etter komma’).

Hvis en bruker har anledning til å skrive inn en slik format string
direkte, så kan vedkommende misbruke disse siden en av dem (`%n`) kan
brukes til å skrive til minne, og med `%m$` kan man skrive til variabel
nummer `m`.

Og med ytterligere finurlighet kan de kontrolleres hvor i minne man
skriver, og det er åpnet for angrep liknende buffer overflow som vi
nettopp så på.

### Dangling Pointers

##### Dangling Pointers

    01 int *A = (int *) malloc (128); /* allocate space for 128 integers */ 
    02 int year_of_birth = read_user_input();         /* read an integer */ 
    03 if (input < 1900) {
    04   printf ("Error, year of birth should be greater than 1900 \n");
    05   free(A); 
    06 } else {
    07   ... 
    08   /* do something interesting with array A */ 
    09   ...
    10 } 
    11 ... /* many more statements, containing malloc and free */ 
    12 A[0] = year_of_birth;

Bug’n her er altså linje 12 hvor det skrives til et sted i minne som
kanskje er allokert noe helt annet på det tidspkt `A[0]` brukes, og
brukeren har jo kontroll på hva som er i variabelen `year_of_birth`.
Ikke lett å utnytte dette for en angriper, men det er en liten åpning.

### Integer Overflow

##### Integer Overflow Attacks

1.  Multiplying integers can easily overflow (’wrap-around’).

2.  Typical attack can be providing specific height and width for an
    image to be opened:
    
      - height \(\times\) width results in overflow and allocation of
        not enough memory.
    
      - image overwrites memory and of course *the user decides the
        content of the image* ...

—–

som den står.

### Command Injection

##### Command Injection Attacks

![image](/home/erikhje/office/kurs/opsys/15-malwareandmemory/tex/../../resources/MOS_3e/09-26.pdf)

Innlysende som den står, hvis en bruker kan fylle inn det han/hun vil
inn i et kall til `system` så kan vilkårlig kode kjøres.

Nøkkelen er at flere shell kommandoer kan kjøres med `;` imellom.

### ToCToU

##### Time of Check to Time of Use

    01 int fd; 
    02 if (access ("./my document", W OK) != 0) 
    03   exit (1); 
    04 fd = open ("./my document", O WRONLY); 
    05 write (fd, user input, sizeof (user input));

"The attack is known as a TOCTOU (Time of Check to Time of Use) attack.
Another way of looking at this particular attack is to observe that the
`access` system call is simply not safe. It would be much better to open
the file first, and then check the permissions using the file descriptor
instead—using the `fstat` function." (Tanenbaum side 657)

## Insider Attacks

##### Insider Attack: Trap Doors /Backdoors

![image](/home/erikhje/office/kurs/opsys/15-malwareandmemory/tex/../../resources/MOS_3e/09-22.pdf)

Bakdør, her vist hvordan en bruker med brukernavn `zzzzz` alltids vil
slippe gjennom autentiseringsprosessen.

  - Logisk bombe

  - Tidsbombe

  - Bakdør

  - Login spoofing

(Punchline idag er å se litt på alle mulige måter sikkerhet kan
forbigås)

##### Insider Attack: Login Spoofing

![image](/home/erikhje/office/kurs/opsys/15-malwareandmemory/tex/../../resources/MOS_3e/09-23.pdf)

Hvem som helst bruker kan jo skrive et program som kjører i full skjerm
og ser ut som et login program, som ved forsøk på innlogging dreper
brukerns kjørende shell og sender systemet tilbake til opprinnelig
innloggingsvindu.

Eneste måte å unngå dette på er å starte login med en tastekombinasjon
som er brukerprogram ikke kan fange, `CTRL-ALT-DEL` er en slik
kombinasjon.

## Malware

##### Malware

1.  Trojan horses

2.  Viruses

3.  Worms

4.  Spyware

5.  Rootkits

—–

Andre relevante begreper:

  - Botnet  
    Robot Network (zombie backdoors)

  - Keylogger  
    Periodisk rapportering eller søk etter bestemte data (kredittkortnr)

  - ID tyveri  
    Misbruk av IDn din selv om ingen grove sikkerhetsbrudd har skjedd,
    fødselsnummeret må ALDRI betraktes som en hemmelighet men bør
    samtidig beskyttes\!

Ift kredittkort har selvfølgelig kredittkortfirmaene avviksprogramvare,
men den prøver jo de ondsinnede hackerne og holde seg akkurat under
terskelen for, det er alltid en kamp om hvem som ligger foran ...

Generelt: mangfold er ofte en god ting for sikkerhet (single-sign on kan
være skummelt selv om det er bra).

Og igjen: husk brukervennlighet vs. sikkerhet dilemma: hvem har passord
for å ringe fra mobilen sin? Dette hadde økt sikkerheten ikke sant?

### Trojan Horses

##### Trojan Horses

![image](/home/erikhje/office/kurs/opsys/15-malwareandmemory/tex/../img/trojan_wooden_horse.jpg)

En trojansk hest er altså noe som er noe annet enn det utgir seg for å
være, eller som gjør noe annet enn vi tror det skal gjøre.

En trojansk hest er altså ikke noe programvare som bryter seg inn på
maskinen din, det er noe brukeren selv installerer ... (fordi den følger
med noe annet, f.eks. skjermbeskyttere)

DEMO: bruk av PATH environment variabelen for å kjøre et annet program
enn det jeg tror jeg kjører...

    echo $PATH # ser at ~/bin er før de andre bin
    which ls
    ls
    cat /tmp/skummelt
    cp ls ~/bin
    ls 
    cat /tmp/skummelt
    rm ~/bin/ls

Alternativt: lag et skummelt program som har et navn som tilsvarer en
vanlig feiltasting, f.eks. `ls-ltr`

    ls-ltr
    cat /tmp/skummelt
    cp ls-ltr ~/bin
    ls-ltr
    cat /tmp/skummelt
    rm ~/bin/ls-ltr

### Viruses

##### Viruses

1.  (Companion viruses)

2.  Overwriting viruses

3.  Parasitic viruses

4.  Memory-resident viruses

5.  Boot sector viruses

6.  Device driver viruses

7.  Macro viruses

8.  Source code viruses

—–

Et virus reproduserer seg selv.

  - (Companion Virus)  
    ala trojaner eksempelet, men prog av samme navn, f.eks. `prog.com`
    og `prog.exe` (kjører disse i sekvens så brukern ikke er klar over
    at prog.com ble kjørt først).

  - Overwriting virus  
    skriver over programmer.

  - Parasitic virus  
    hekter seg på programmer, men lar sitt host-program ellers fungere
    normalt.

  - Memory-resident virus  
    befinner seg i minne til enhver tid.

  - Boot sector virus  
    befinner seg typisk i MBR eller et annet sted knyttet til bootloader
    for å laste seg selv sammen med OS’et.

  - Device driver virus  
    device drivere er jo vanligvis bare program de også, dermed kan
    virus hekte seg på de og lastes gjerne i kernel mode.

  - Macro virus  
    befinner seg typisk i office dokumenter knyttet til forskjellige
    funksjoner (typisk ikke binær kode men interpretert kode).

  - Source code virus  
    hekter seg inn som en typisk to linjer (en header og et
    funksjonskall) i kildekoden til programmer før de kompileres.

Husk sammenheng med aksess kontroll på objekter som vi pratet om sist:
kjører du som regel med admin/root rettigheter til enhver tid har du
store problemer om du får virus...

##### Viruses

![image](/home/erikhje/office/kurs/opsys/15-malwareandmemory/tex/../../resources/MOS_3e/09-29.pdf)

Et virus som legger seg i minne (og gjerne da i minne sammen med OS’et
og interrupt vektorene i resident minne, dvs som ikke er gjenstand for
paging) klarer å beholde kontroll som regel på følgende vis:

(a) ved oppstart skriver viruset over alle interrupt vektorer slik at de
peker på viruskoden istedet for interrupthandler

(b) og (c) når device drivere lastes skrives interrupt vektorene over
igjen (slik som for printer interrupt vektoren i (b)), MEN nøkkelen er
at de gjør de ikke alle samtidig, dvs det vil alltid forekomme bruk av
en eller flere interrupt vektorer (spesielt klokkeinterrupt) som viruset
kontrollerer før alle er overskrevet, dermed vil viruset kunne
overskrive igjen alle de som er blitt overskrevet av OS’et og gjenta
full kontroll som i (c).

Virus ønsker å få kjøre for hvert eneste systemkall fordi da kan det
dytte seg selv inn i minne på alle prosesser som gjør `exec`.

### Worms

##### Worms

1.  A *worm* replicates itself over the network.

—–

Prat litt om internetormen, og vis

<http://www.ee.ryerson.ca/~elf/hack/iworm.html>

som fører til

<http://pdos.csail.mit.edu/~rtm/>

![image](/home/erikhje/office/kurs/opsys/15-malwareandmemory/tex/../img/spilling.jpg)

### Spyware

##### Spyware

1.  Spyware is software that without the users consent leaks private
    information.

—–

Brukes oftest for å rapportere data nyttig for markedsføringsformål.

Installeres ofte via toolbars, activeX kontroller eller pop-ups (hvor
brukeren klikker ’No’ istedet for å bare lukke vinduet).

Spyware bruker ofte DLL injection metodikk:

<http://en.wikipedia.org/wiki/DLL_injection>

Nyttig innovativ norsk antispyware teknologi:

<http://www.promon.no>

### Rootkits

##### Rootkits

![image](/home/erikhje/office/kurs/opsys/15-malwareandmemory/tex/../../resources/MOS_3e/09-30.pdf)

  - Firmware rootkits

  - Hypervisor rootkits

  - Kernel rootkits

  - Library rootkits

  - Application rootkits

Generelt skiller rootkits seg fra trojanske hester med to hovedpoeng:

1.  Rootkits er noe som installeres etter et angrep er kjørt mens en
    trojaner kan selv være et angrep.

2.  Rootkits består som regel av mange filer og og systemendringer og er
    langt mer komplisert enn en enkelt trojaner.

DEMO:

`sudo chkrootkit` som viser sjekk av typisk manipulerte filer og sjekk
etter signaturer på kjente rootkits.

Som med all malware: den tryggeste måten er å boote fra et sikkert
medium og scanne disk/partisjon.

![image](/home/erikhje/office/kurs/opsys/15-malwareandmemory/tex/../extra/tanenbaum-pp-690-sony.pdf)

## Defenses

### Firewalls

##### Without Firewall

![image](/home/erikhje/office/kurs/opsys/15-malwareandmemory/tex/../img/port1.png)

Trafikk fra internet ankommer på porter som identifiseres med et 16-bits
portnr (i tillegg til IP addresse selvfølgelig, og skille mellom
protokoll TCP, UDP, og evn andre).

Generelt om beskyttelse: *Defense in depth:* Løkmodell - flest mulig lag
med sikkerhet.

##### With Firewall

![image](/home/erikhje/office/kurs/opsys/15-malwareandmemory/tex/../img/port2.png)

En brannmur kan altså være sett med ACLer (aksess kontroll lister) som
legger seg over disse portnummerne. Tenk på hver port som en fil som har
hver sin aksess kontroll liste og subjektene som skal ha tilgang er
identifisert ved IP, portnr, protokoll, o.l.

Vi har og konseptet "Next Generation Firewall" (NGFW) som er brannmurer
som i tillegg til å se til at portnumrene stemmer også ser til at
innholdet i pakkene som blir sent samsvarer med hva som er forventet.
F.eks at det er HTTP i en pakke sendt til port 80. NGFW har også ofte
IDS/IPS er og lignende.

Vi tar med brannmur her fordi det er en av operativsystemets viktigste
beskyttelsesmekanismer, selv om det er en mekanisme som ikke tilhører
operativsystemet.

### Antivirus

##### How Viruses Hide

![image](/home/erikhje/office/kurs/opsys/15-malwareandmemory/tex/../../resources/MOS_3e/09-32.pdf)

(b) og (c) et virus gjør jo filen større så host-programmet må
komprimeres for å skjule at viruset er blitt en del av det, og
komprimerings og dekomprimeringskoden må lagres også.

(d) for å skjule viruset må viruset krypteres md en ny nøkkel hver gang,
random nøkkel kan f.eks. genereres ved `XOR` mellom tiden, og
minneordene på addressene 34622 og 129836 (dvs to tilfeldige
minnelokasjoner).

Et virus kan gjøre mye for å gjemme seg, men til slutt i (e) vil i denne
figuren vil dekrypteringsrutinen fortsatt være synlig og antivirus
programvaren kan lete etter denne.

##### Polymorphic Virus

![image](/home/erikhje/office/kurs/opsys/15-malwareandmemory/tex/../../resources/MOS_3e/09-33.pdf)

Fem varianter av samme kode, i (e) er R5 og Y bare avledningsmanøvre.

Et polymorfisk virus endrer seg selv (f.eks. sin dekrypteringsrutine)
hver gang en ny fil infiseres. Typisk ved å sette inn `NOP` eller kode
som gjør tilsvarende ingenting (dvs har ingen annen effekt enn å
forvirre).

Måten disse endringene gjøres på bestemmes av en mutasjonsrutine
(*mutation engine* og denne kan gjemmes i den krypterte delen i (e) i
forrige figur.

*Og dermed kan ingen fast signatur på noe av viruskoden finnes ...*

### Code Signing

##### Code Signing

![image](/home/erikhje/office/kurs/opsys/15-malwareandmemory/tex/../../resources/MOS_3e/09-34.pdf)

### Jailing

##### Jailing

![image](/home/erikhje/office/kurs/opsys/15-malwareandmemory/tex/../../resources/MOS_3e/09-35.pdf)

All oppførsel ved et program kan overvåkes med jailing, dvs hvert
systemkall overføres til *’jailer’ som bestemmer om de skal tillates*,
f.eks. åpne nettforbindelse til en ukjent IP.

Systemkallene overføres altså til ’jailer’ istedet for å utføres av
kernel direkte.

Det er også vanlig å “jaile” prosesser i sin egen del av filsystemet
slik som godt forklart i “Create a simple chroot jail”:
<http://www.adminarticles.com/create-a-simple-chroot-jail/> Dette gjøres
i stor grad på mobile enheter som installeres “app’er”.

### Host-based IDS

##### Model-based IDS

![image](/home/erikhje/office/kurs/opsys/15-malwareandmemory/tex/../../resources/MOS_3e/09-36.pdf)

Finnes mange typer Host-based IDS, konseptuelt veldig likt antivirus
(tenk på det som en generalisering av antivirus).

Model-based IDS er et eksempel hvor det sjekkes for avvik fra
*systemkallgrafen* i (b). Systemkallgrafen er da på en måte et
alternativ til en signatur.

Kan implementeres via jailing (fra forrige figur).

### Encapsulating Mobile Code

##### Sandboxing

![image](/home/erikhje/office/kurs/opsys/15-malwareandmemory/tex/../../resources/MOS_3e/09-37.pdf)

Virtual memory kan deles opp i sandboxer hvor hver applet får en kode og
en data sandbox som den må holde seg innenfor.

Ved å ha en egen kodesandbox kan denne merkes read-only slik at man
unngår faren for selvmodifiserende kode.

I det en applet lastet relokaliseres alle minnereferanser til å være
innenfor appletens sandboxer.

(b) viser hvordan 4 instruksjoner og 2 registre benyttes (settes inn)
før hver dynamisk JMP instruksjon, for å sjekke om addresseringen er
innenfor sandboxene (`SHR` er shift-right for å isolere prefix-bitene
som er de som de skal sjekkes på, dvs alle minnereferanser innenfor
samme sandboxer vil ha lik higher-order bits prefix).

Alle forsøk på systemkall omdirigeres til en egen Reference monitor.

##### Interpretation

![image](/home/erikhje/office/kurs/opsys/15-malwareandmemory/tex/../../resources/MOS_3e/09-38.pdf)

Alternativt kan kode også tolkes, dvs den mobile koden er ikke kompilert
til direkte kjørbare kode men skal tolkes slik som f.eks. java applets
gjerne funker, da er det enda letteres å sjekke koden før kjøring.

I figuren vises en trusted applet (f.eks. som er verfisert via
kodesignatur) som har lov til å gjøre systemkall, og en untrusted som må
kjøre i en sandbox.

## Human in the Loop

##### Levels of Human Intervention

1.  Fully automated attack script

2.  Trick users into visiting a website with their buggy outdated
    browser

3.  Trick users into double-clicking on attachments

—–

Det er forskjellige nivåer med brukerinteraksjon påkrevet i angrepene.
Husk at vi kan sikkert klare å sikre oss godt mot automatiserte angrep,
men vi må også hjelpe brukerne beskytte seg mot seg selv så godt de kan
(f.eks. Windows Protected Mode Internet Explorer som bruker low
integrity level). INGEN har noengang verfisiert på forhånd absolutt alt
de har klikka på...

Husk at prosesskonseptet ble innført for å skille mellom prosesser som
tilhører forskjellige brukere, men det gir liten beskyttelse mellom to
prosesser fra samme bruker siden de tillates og sende meldinger til
hverandre. Men code signing, jailing, sandboxing sammen med integrity
levels hjelper altså brukeren å beskytte seg mot seg selv.

## Theory questions

1.  Forklar kort og presist klassisk buffer overflow angrep og
    return-to-libc angrep.

2.  Tanenbaum oppgave 9.43  
    What is the difference between a virus and a worm? How do they each
    reproduce?

3.  Forklar kort hva som menes med et *integer overflow attack*.

4.  Forklar hvordan et virus kan beholde kontrollen over alle
    interruptvektorene med utgangspunkt i denne figuren:
    
    ![image](/home/erikhje/office/kurs/opsys/00-REVIEW/../resources/MOS_3e/09-29.pdf)

5.  Hva er et *rootkit*? Hvilke fem steder kan et rootkit typisk gjemme
    seg?

6.  Hva menes med *jailing*? Forklar kort hvordan jailing typisk
    fungerer.

## Lab exercises

1)  Bare fortsett på forrige ukes oppgaver...

# Bash

## Intro

![image](/home/erikhje/office/kurs/tutorial-bash/./img/scripting.png)  
<span>(OUSTERHOUT, J., “Scripting: Higher-Level Programming for the 21st
Century”, IEEE Computer, Vol. 31, No. 3, March 1998, pp. 23-30.)</span>

From Ousterhout, 1998:  
While programming languages like C/C++ are designed for low-level
construction of data structures and algorithms, scripting languages are
designed for high-level “gluing” of existing components. Components are
created with low-level languages and glued together with scripting
languages.

##### WARNING\!

The following presentation is NOT meant to be a comprehensive/complete
tour of the Bash language.

*The purpose is to get you started with some basic program constructions
which you will recognize based on some-sort-of-programming-background.*

At the end of the presentation you will find pointers to more
comprehensive material.

##### Practice

*You need a GNU/Linux distribution (e.g. Ubuntu) running on a physical
or virtual machine with working access to the internet, and with `wget`
installed*.

Log in and open a terminal window, download the examples as we go along
from <https://bitbucket.org/ehjelm/iikos-bash>

You will find the `FILENAME` on the second line of each example. Execute
the code  
`bash FILENAME`  
or (make it executable with `chmod +x FILENAME`)  
`./FILENAME`

*It is easy to write bash scripts, but sometimes your scripts will
behave strangely. This is due to the fact that there are many pitfalls
in Bash. It is very easy to write statements that appear logical to you
in your way of thinking programming, but due to the nature of a shell
environment such as Bash, they will not produce the expected results. I
strongly recommend that you quickly browse (and remember as a reference)
the following excellent document:*  
<http://mywiki.wooledge.org/BashPitfalls>  
*and use the following tool to check the quality of your script (you can
also probably apt-get install shellcheck):* <http://www.shellcheck.net/>

##### Hello World

    #!/bin/bash
    # hello.bash
    
    echo "Hello world!"

make executable and execute:

    chmod +x hello.bash
    ./hello.bash

The SheBang/HashBang `#!` is treated as a comment by the interpreter,
but it has a special meaning to the operating system’s program loader
(the code that is run when one of the `exec` system calls are executed)
on Unix/Linux systems. The program loader will make sure this script is
interpreted by the program listed after the `#!`

Since Unix/Linux does not use file endings for identifying the file,
there is no reason for us to do so in scripts either. The OS knows from
the SheBang/HashBang what interpreter to use. However during the
development of a script it can be nice to know something about the file
content based on the file ending, so it is common to use `.bash` or
`.sh` endings. Since this tutorial is about the Bash language, including
Bash-specific features that are not POSIX-compliant, we stick with
`.bash` as file endings.

## Variables

##### Single Variables

    #!/bin/bash
    # single-var.bash
    
    firstname=Mysil
    lastname=Bergsprekken
    fullname="$firstname $lastname"
    echo "Hello $fullname, may I call you $firstname?"

A single variable is not typed, it can be a number or a string.

Do not put spaces before or after `=` when assigning values to
variables.

If you need to substitute a variable immediately before a string, e.g
the variable `sum` and the string `KB`, use curly braces around the
variable (you will have to do this every time for array elements as well
as you will see below):  
`echo "disk usage is ${sum}KB"`

Scope: variables are global unless specified inside a block and starting
with the keyword `local`.

(in general, use lower case variable names, upper case implies it’s a
SHELL/ENVIRONMENT variable)

##### Single and Double Quotes

    #!/bin/bash
    # quotes.bash
    
    name=Mysil
    echo Hello   $name
    echo "Hello   $name"
    echo 'Hello   $name'

Variables are expanded/interpolated inside double quotes, but not inside
single quotes. We use double quotes when we have a string.

### Arrays

##### Arrays

Bash supports simple one-dimensional arrays

    #!/bin/bash
    # array.bash
    
    os=('linux' 'windows')
    os[2]='mac'
    echo "${os[1]}"  # print windows
    echo "${os[@]}"  # print array values
    echo "${!os[@]}" # print array indices
    echo "${#os[@]}" # length of array

Automatic expansion of arrays (automatic declaration and garbage
collection). `os[2]='mac'` can also be written as `os+=('mac')`

##### Associative Arrays

    #!/bin/bash
    # assoc-array.bash
    
    declare -A user        # must be declared
    user=(                       \
           [frodeh]="Frode Haug" \
           [ivarm]="Ivar Moe"    \
         )
    user[lailas]="Laila Skiaker"
    echo "${user[ivarm]}"  # print Ivar Moe
    echo "${user[@]}"      # print array values
    echo "${!user[@]}"     # print array indices (keys)
    echo "${#user[@]}"     # length of array

Associative arrays were introduced with Bash version 4 in 2009. If we
don’t declare the variable as an associative array with `declare -A`
before we use it, it will be an ordinary indexed array.

`user[lailas]="Laila Skiaker"` can also be written as  
`user+=([lailas]="Laila Skiaker")`

### Structures/Classes

##### Structures/Classes

Sorry, no structs or classes in Bash ...

### Command-line args

##### Command-Line Arguments

Scriptname in $0, arguments in $1, $2, ...

    #!/bin/bash
    # cli-args.bash
    
    echo "I am $0, and have $# arguments \
          first is $1"

Bash accepts the first nine arguments as `$1`...`$9`, for further
arguments use `${10}`, `${11}`, ...

## Input

### Input

##### Input From User

    #!/bin/bash
    # input-user.bash
    
    echo -n "Say something here:"
    read -r something
    echo "you said $something"

##### Input From STDIN

Same way, commonly without an echo first

    #!/bin/bash
    # input-stdin.bash
    
    read -r something
    echo "you said $something"

can be executed as

    echo "hey hey!" | ./input-stdin.bash

Of course, input from user is from `STDIN`.

We use `read -r` to avoid `read` removing backslashes in the input.

### System commands

##### Input from System Commands

You can use `$(cmd)` (supports nesting) or `` `cmd` `` (deprecated)

    #!/bin/bash
    # input-commands.bash
    
    kernel="$(uname -sr)"
    echo "I am running on $kernel in $(pwd)"

This is also called *command substitution*. `` `...` `` (backticks) is
deprecated because it’s difficult to read, and can create some problems,
see <http://mywiki.wooledge.org/BashFAQ/082>

## Conditions

### if/else

##### if/else

    #!/bin/bash
    # if.bash
    
    if [[ "$#" -ne 1 ]]; then
       echo "usage: $0 <argument>"
    fi

Note: there must be spaces around `[[` and `]]`.

There is also an older (slower) and more portable (meaning POSIX
defined) operator, `[` which is actually an alias for the operator
`test`, meaning

    [ "$#" -ne 2 ]
    # is the same as
    test "$#" -ne 2

### Operators

##### Arithmetic Comparison

| Operator | Meaning                  |
| :------: | :----------------------- |
|  `-lt`   | Less than                |
|  `-gt`   | Greater than             |
|  `-le`   | Less than or equal to    |
|  `-ge`   | Greater than or equal to |
|  `-eq`   | Equal to                 |
|  `-ne`   | Not equal to             |

##### String Comparison

| Operator | Meaning                                   |
| :------: | :---------------------------------------- |
|   `<`    | Less than, in ASCII alphabetical order    |
|   `>`    | Greater than, in ASCII alphabetical order |
|   `=`    | Equal to                                  |
|   `==`   | Equal to                                  |
|   `!=`   | Not equal to                              |

##### File Tests

| Operator | Meaning                       |
| :------: | :---------------------------- |
|   `-e`   | Exists                        |
|   `-s`   | Not zero size                 |
|   `-f`   | Regular file                  |
|   `-d`   | Directory                     |
|   `-l`   | Symbolic link                 |
|   `-u`   | Set-user-id (SetUID) flag set |

There are many more file test operators of course.

##### Boolean

<span> | c | l | </span> Operator & Meaning  
`!` & Not  
`&&` & And  
`||` & Or  

##### Numerical or String Compare

    #!/bin/bash
    # if-num-string.bash
    
    if [[ "$#" -ne 2 ]]; then
       echo "usage: $0 <argument> <argument>"
       exit 0
    elif [[ "$1" -eq "$2" ]]; then
       echo "$1 is arithmetic equal to $2"
    else
       echo "$1 and $2 arithmetic differs"
    fi
    if [[ "$1" == "$2" ]]; then
       echo "$1 is string equal to $2"
    else
       echo "$1 and $2 string differs"
    fi
    if [[ -f "$1" ]]; then
       echo "$1 is also a file!"
    fi

This shows the if-elif-else construction, the difference between string
and numerical comparison, and a file test operator.

Note the difference between `-eq` and `==`

    $ ./if-num-string.bash 1 01
    1 is arithmetic equal to 01
    1 and 01 string differs

##### Boolean example

    #!/bin/bash
    # if-bool.bash
    
    if [[ 1 -eq 2 && 1 -eq 1 || 1 -eq 1 ]]; then
       echo "And has precedence"
    else
       echo "Or has precedence"
    fi
    
    # force OR precedence: 
    
    if [[ 1 -eq 2 && (1 -eq 1 || 1 -eq 1) ]]; then
       echo "And has precedence"
    else
       echo "Or has precedence"
    fi

AND is always (as known from mathematics courses) evaluated before OR
(binds more tightly). Write it down in logic (truth table) if you are
unsure.

### Switch/case

##### Case

    #!/bin/bash
    # switch.bash
    
    read -r ans
    case $ans in
    yes)
       echo "yes!"
       ;;&             # keep testing
    no)
       echo "no?"
       ;;              # do not keep testing
    *)
       echo "$ans???"
       ;;
    esac

See also `select` and `whiptail`.

## Iteration

### For

##### For loop

    #!/bin/bash
    # for.bash
    
    for i in {1..10}; do
       echo -n "$i "
    done
    echo
    
    # something more useful:
    
    for i in ~/*; do
       if [[ -f $i ]]; then
          echo "$i is a regular file"
       else
          echo "$i is not a regular file"
       fi
    done

We can also use `for i in $(ls -1 ~/)` or `for i in $(stat -c "%n" ~/*)`
but this creates problems if we have filenames with spaces, so it’s much
better to use Bash’ builtin expansion operator `*` as we do in this
example. See more on this below when we talk about `while`.

If you just quickly want to iterate over a short list of fixed numbers,
`{1..10}` is ok, but this has two downsides: 1. these numbers are
generated at once, so this consumes memory if the list is long, 2. if
you want to iterate up to a variable `$max` this does not work and you
shoud insted use the more traditional programming syntax  
`for ((i=1; i<$max; i++))`

Many times however, we want to avoid loops and just solve the problem
with use of the right commands in a pipeline ("There is more than one
way to do it", TIMTOWTDI)

##### TIMTOWTDI

    #!/bin/bash
    # loop-or-not.bash
    
    for f in *; do [[ -f "$f" ]] && 
     [[ $(stat -c%s "$f") -gt 200 ]] && echo "$f"; done
    
    find . -maxdepth 1 -type f -size +200c -print0 | 
     xargs -0 ls -1
    
    find . -maxdepth 1 -type f -size +200c \
     -exec ls -1 {} \+
    
    find . -maxdepth 1 -type f -size +200c \
     -printf '%f/\n'

### While

In general, we prefer using a while loop instead of a for loop when
iterating over a set of items (items being files, process names, user
names, lines in a file, etc). This is due to the fact that when using a
for loop as shown above, the list of items is generated beforehand and
thereby can consume significant amounts of memory. A while loop allows
us to iterative item by item without pre-generating all the items in
memory.

##### While

We want to read from STDIN and do stuff line by line

    #!/bin/bash
    # while.bash
    
    i=0
    while read -r line; do
       foo[i]=$line
       ((i++))
    done
    echo "i is $i, size of foo ${#foo[@]}"

    $ ls -1 | ./while.bash
    i is 20, size of foo is 20

##### A problem ...

What if we want to pipe into a while inside our script:

    #!/bin/bash
    # while-pipe-err.bash
    
    i=0
    find | while read -r line; do
       foo[i]=$line
       ((i++))
    done
    echo "i is $i, size of foo ${#foo[@]}"

    $ ./while-pipe-err.bash
    i is 0, size of foo is 0

*In other words, this does not work due to a subshell being used
(because of the pipe) inside while\!*

Meaning that the variables outside the while loop are not accessible
inside the while loop since it is run as a new process.

##### Proper Solution

Inspired by  
<http://mywiki.wooledge.org/BashGuide/Arrays>

    #!/bin/bash
    # while-pipe.bash
    
    i=0
    while read -r -d ''; do
       foo[i]=$REPLY
       ((i++))
    done < <(find . -maxdepth 1 -print0) 
    echo "i is $i, size of foo ${#foo[@]}"

    $ ./while-pipe.bash
    i is 20, size of foo is 20

`while-pipe.bash` is the proper way of processing output from commands,
note

  - `-d` is delimiter which we set to the empty string (NUL-byte, `\0`)

  - if we don’t give a variable name to `read` it will place contents in
    the default variable `REPLY`

  - `<(...)` is process substitution meaning it’s the same as `$(...)`
    but we pipe in the output as well into the redirection `<` which
    "sends" the output into the loop.

  - `find . -maxdepth 1 -print0` is the safe way of doing `ls`,
    `-maxdepth 1` limits find to only list files in `./` and `-print0`
    inserts the NUL-byte (`\0`) as a separator between the file names to
    avoid any confusion with odd characters in file names (the NUL-byte
    is not allowed in file names)

We can also solve this problem (if the problem is only processing files
in the directory, note that the solution above with while is more
general and can be used by any command capable of inserting a NUL-byte
separator in its output) by rewriting it as a for loop instead (but, as
mentioned, this has the downside of consuming memory if the list of
files is big since it is generated before the loop initiates):

##### Solution with for instead of while

    #!/bin/bash
    # for-commands.bash
    
    i=0
    for line in ./*; do
       foo[i]=$line
       ((i++))
    done
    echo "i is $i, size of foo ${#foo[@]}"

## Math

##### Operators

|   Operator    | Meaning  |
| :-----------: | :------- |
|      `+`      | Add      |
|      `-`      | Subtract |
|      `*`      | Multiply |
|      `/`      | Divide   |
| `%` & Modulus |          |

*Only on integers\!*

    #!/bin/bash
    # math.bash
    
    i=0
    ((i++))
    echo "i is $((i-2))" # print -1

##### A trick for floats

    #!/bin/bash
    # math-float.bash
    
    echo "3.1+5.6 is $(echo '3.1+5.6' | bc)"

## Functions

##### Functions

    #!/bin/bash
    # func.bash
    
    # declare:
    function addfloat {
       echo "$1+$2" | bc
    }
    # use:
    addfloat 5.12 2.56 

## RegExp

##### Regular expressions intro 1/5

Special/Meta-characters:

`  \ | ( ) [ ] { } ^ $ * + ? .  `

*These have to be protected with `\`*, e.g.  
`http://www\.hig\.no`

> To match `c:\temp`, you need to use the regex `c:\\temp`. As a string
> in C++ source code, this regex becomes `"c:\\\\ temp"`. Four
> backslashes to match a single one indeed.

(from <http://www.regular-expressions.info/characters.html>):

Regular expressions are the generic way to describe a string/syntax/word
that you use for either syntax/compliance checking or searching. It is
commonly used in configuration files. Think of it as a generic way of
doing advanced search. Google would probably prefer user to only enter
regular expression as search terms, but that would be to hard for the
general population so Google offers “advanced search” instead:  
<http://www.google.com/advanced_search>

There are many different regular expression engines, which differs
mostly in features and speed. In this tutorial we will try to stick with
simple examples which will the same in most engines (perl, pcre,
extended posix, .NET, ...).

Good intro video for [tools to help you write regular
expressions](https://www.youtube.com/watch?v=hprXxJHQVfQ).

##### Regular expressions intro 2/5

Describing characters:

|   Operator    | Meaning                      |
| :-----------: | :--------------------------- |
|      `.`      | Any single character         |
|   `[abcd]`    | One of these characters      |
|   `[^abcd]`   | Any one but these characters |
| `[A-Za-z0-9]` | A character in these ranges  |
|   `:word:`    | A word (A-Za-z0-9\_)         |
|   `:digit:`   | A digit                      |

`\w` is the same as `[a-zA-Z0-9]` and `\d` is the same as `[0-9]` . Many
more of course ...

##### Regular expressions intro 3/5

Grouping:

| Operator | Meaning |
| :------: | :------ |
|   `()`   | Group   |
|   `\|`   | OR      |

Anchoring:

| Operator | Meaning           |
| :------: | :---------------- |
|   `^`    | Beginning of line |
|   `$`    | End of line       |

##### Regular expressions intro 4/5

Repetition operators/Modifiers/Quantifiers:

| Operator | Meaning                        |
| :------: | :----------------------------- |
|   `?`    | 0 or 1 time                    |
|   `*`    | 0 or more times                |
|   `+`    | 1 or more times                |
|  `{N}`   | N times                        |
|  `{N,}`  | At least N times               |
| `{N,M}`  | At least N but not more than M |

Demo: example with `cat a.html | egrep REGEXP` (four steps).

##### Regular expressions intro 5/5

Finding URLs in HTML:  
`(mailto|http):[^"]*`

Each line should be an email address:  
`^[A-Za-z0-9._-]+@[A-Za-z0-9.-]+$`

Remember that regexp engines are most often greedy, they try to match as
much as possible, so using e.g. `.*` might match more than you were
planning for.

### Bash example

##### Bash example

    #!/bin/bash
    # regexp.bash
    
    while read -r line; do
       if [[ $line =~ \
          ^[A-Za-z0-9._-]+@([A-Za-z0-9.-]+)$ ]]
       then
          echo "Valid email ${BASH_REMATCH[0]}"
          echo "Domain is ${BASH_REMATCH[1]}"
       else
          echo "Invalid email address!"
       fi
    done

When we use regular expressions inside scripts, it is very useful to be
able to extract parts of the match. We can do this by specifying the
part with `(part)` and refer to it later in the `$BASH_REMATCH` array
(if we specify two parts, the second one will be in `$BASH_REMATCH[2]`
etc).

If you for some reason (maybe optimization) do not want the part inside
parenthesis to be stored in `$BASH_REMATCH`, you can do this by saying
`(?:part)`. In other words, the two characters `?:` here has the special
meaning "do not include what this parenthesis matches in
`BASH_REMATCH`".

Of course you can use regexp in many different components which you can
include in your bash script (sed, grep, perl, ...).

## Bash only

##### Advanced stuff

See Advanced Bash-Scripting Guide at

<http://tldp.org/LDP/abs/html/>

for everything you can do with Bash

## Credits

##### Credits

<span>J. Ousterhout, "Scripting: Higher-Level Programming for the 21st
Century," IEEE Computer, Vol. 31, No. 3, March 1998, pp. 23-30.  
<http://tldp.org/HOWTO/Bash-Prog-Intro-HOWTO.html>
<http://www.linuxconfig.org/Bash_scripting_Tutorial>  
<http://www.thegeekstuff.com/2010/06/bash-array-tutorial/>  
<http://www.panix.com/~elflord/unix/bash-tute.html>  
<http://www.codecoffee.com/tipsforlinux/articles2/043.html>  
<http://tldp.org/LDP/abs/html/>  
<http://linuxsig.org/files/bash_scripting.html>  
<http://mywiki.wooledge.org/BashGuide>  
<http://www.regular-expressions.info/>  
Alexander Berntsen</span>

# PowerShell

![image](/home/erikhje/office/kurs/tutorial-powershell/./scripting.png)  
<span>(OUSTERHOUT, J., “Scripting: Higher-Level Programming for the 21st
Century”, IEEE Computer, Vol. 31, No. 3, March 1998, pp. 23-30.)</span>

From Ousterhout, 1998:  
While programming languages like C/C++ are designed for low-level
construction of data structures and algorithms, scripting languages are
designed for high-level “gluing” of existing components. Components are
created with low-level languages and glued together with scripting
languages.

##### WARNING\!

The following presentation is NOT meant to be a comprehensive/complete
tour of the PowerShell language.

*The purpose is to get you started with some basic program constructions
which you will recognize based on some-sort-of-programming-background.*

At the end of the presentation (Credits section) you will find pointers
to more comprehensive material (reference material).

##### Practice

*You need a Windows host running on a physical or virtual machine with
working access to the internet*.

Log in and open a terminal window, download the examples as we go along
from <https://bitbucket.org/ehjelm/iikos-powershell>

We assume that you are using PowerShell as shipped with Windows 10

To allow for execution of scripts in powershell you need to set the
correct execution policy:

    # check what is current policy
    Get-ExecutionPolicy
    # change to only require signature on remote scripts
    Set-ExecutionPolicy RemoteSigned
    # you probably need to "run as administrator" to do this

Check code quality with:

    # static code analysis
    # https://github.com/PowerShell/PSScriptAnalyzer
    
    Install-Module -Name PSScriptAnalyzer -Scope CurrentUser
    
    # use it on script a.ps1
    Invoke-ScriptAnalyzer a.ps1
    
    # for bigger projects look into Pester
    # https://github.com/pester/Pester

##### Hello World

    # hello.ps1
    
    Write-Output "hello world!"

execute as long as filename ends with `.ps1`:

    .\hello.ps1

or direct from command line `cmd` (DOSPROMPT)

    powershell -command "Write-Output \"hello world!\""

or direct from command line `powershell`

    Write-Output "hello world!"

PowerShell commands are called *cmdlets* (pronounced “commandlets”) and
have the syntax `verb-noun`, e.g. `Write-Output`. Fortunately most of
the cmdlets have aliases corresponding to the commands you might know
from DOS (cmd.exe) or Unix/Linux. In addition there is also a short
PowerShell alias to most cmdlets. To find the cmdlet to a command you
know from before you can use the cmdlet `Get-Alias`:

    Get-Alias ls     # is there a cmdlet corresponding to Unix/Linux ls?
    Get-Alias        # list all the aliases
    # use the DOS command findstr to list all lines containing Get-ChildItem
    Get-Alias | findstr Get-ChildItem 
    # do the same thing but do it the PowerShell-way:
    Get-Alias | Where-Object {$_.Definition -eq "Get-ChildItem"}
    # dont worry about this unknown syntax for now, we will get to it soon

To get help with the cmdlets, use the cmdlet `Get-Help`, e.g. `Get-Help
Write-Output | more`. A nice feature is that you can view the help page
in your browser (on the internet) by adding the parameter `-online`,
e.g. `Get-Help Write-Output -online`.

Note that you can use TAB-completion on both commands and parameters.

## Variables

##### Single Variables

    # single-var.ps1
    
    $firstname="Mysil"
    $lastname="Bergsprekken"
    $fullname="$firstname $lastname"
    Write-Output "Hello $fullname, may I call you" `
               "$firstname`?"

*All variables* are prefixed with `$`

We need to use `` ` `` between `$firstname` and `?` to avoid `?` being
“part of” the variable name.

A single variable (sometimes called a *scalar*) is typed, but PowerShell
chooses the type automatically for us by "guessing". Typing can be
forced by prefixing the variable with e.g. `[int]`. *What is important
to know is that variables are instances of .NET objects, and these
objects are also what is being passed through the pipe of piped commands
(as opposed to just piping byte streams in other shells).*

PowerShell uses namespaces, e.g. you can write `$fullname` or
`$variable:fullname`. You can list all current variables with
`Get-Variable $variable:*`

Scope for a variable can be defined with `Set-Variable -Scope`.
PowerShell can also *dot-source* script files to make a script’s
variables accessible from the command line.

    . single-var.ps1        # dot-source it
    $firstname.GetType()    # what kind of object is it?
    $firstname | Get-Member # Which methods and properties are available?

PowerShell in itself, like much of Windows, is case-insensitive, however
it preserves case when used.

Btw, `` ` `` *is the protection character* (and line continuation
character) in PowerShell (same as `\` in bash). PowerShell does this
differently from Unix/Linux scripts since `\` (in addition to `/`) is
used as a directory separator on Windows, see also

    Get-Help about_escape_characters

##### Exercise

    $name="Mysil"

Use the properties and methods of this object to

find out how many characters the string contains

print the string in upper case

##### Single and Double Quotes

    # quotes.ps1
    
    $name="Mysil"
    Write-Output Hello   $name
    Write-Output "Hello   $name"
    Write-Output 'Hello   $name'

Variables are expanded/interpolated inside double quotes, but not inside
single quotes.

### Arrays

##### Arrays

One-dimensional arrays:

    # array.ps1
    
    $os=@("linux", "windows")
    $os+=@("mac")
    Write-Output $os[1]     # print windows
    Write-Output $os        # print array values
    Write-Output $os.Count  # length of array

*Arrays are created with `@(...)`*

Note how we display the length of the array by viewing a property
(`Count`) of the object. Btw, `Count` is just a reference to the
`Length` property

    . ./array.ps1
    $os.PSExtended | Get-Member

If you want to access an array element within an interpolated string,
you have to place the array element in parentheses like this:

    Write-Output "My operating system is $($os[1])"

##### Associative Arrays

    # assoc-array.ps1
    
    $user=@{
           "frodeh" = "Frode Haug";
           "ivarm"  = "Ivar Moe"
           }
    $user+=@{"lailas"="Laila Skiaker"}
    Write-Output $user["ivarm"]  # print Ivar Moe
    Write-Output @user           # print array values
    Write-Output $user.Keys      # print array keys
    Write-Output $user.Count     # print length of array

*Associative arrays are created with `@{...}`* and are called Hashtables
in PowerShell.

### Structures/Classes

##### Structures/Classes

A simple object used as a struct:

    # struct.ps1
    
    $myhost=New-Object PSObject -Property `
            @{os="";
              sw=@();
              user=@{}
             }
    $myhost.os="linux"
    $myhost.sw+=@("gcc","flex","vim")
    $myhost.user+=@{
                    "frodeh"="Frode Haug";
                    "monicas"="Monica Strand"
                   }
    Write-Output $myhost.os
    Write-Output $myhost.sw[2]
    Write-Output $myhost.user["monicas"]

Of course, since PowerShell is based on the object-oriented framework
.NET, creating and manipulating objects is a world by it self, there are
a plethora of ways of doing these things.

See what kind of object this is by running the commands on the command
line and doing

    $myhost
    $myhost.GetType()
    $myhost | Get-Member

Note also that we don’t need the line continuation character `` ` ``
when inside a block (`{...}`).

### Command-line args

##### Command-Line Arguments

All command-line arguments in the array `$args`

Scriptname retrieved from the object `$MyInvocation`

    # cli-args.ps1
    $str = "I am $($MyInvocation.InvocationName)" +
           " and have $($args.Count) arguments " +
           "first is $($args[0])"
    Write-Output $str

`$MyInvocation` is one of PowerShell’s builtin variables. Again, check
what kind of object this is with

    $MyInvocation.GetType()
    $MyInvocation | Get-Member
    # or check what a typical PowerShell command returns
    Get-Process | Get-Member
    (Get-Process).GetType()
    # contrast this with a traditional cmd command
    ipconfig | Get-Member
    (ipconfig).GetType()

For all special variables in PowerShell, a good resource is
<http://www.neolisk.com/techblog/powershell-specialcharactersandtokens>

##### Exercise

  - Rewrite the previous script to only have one string (just one set of
    double quotes (`"`)), one at the beginning and one at the end, do
    not use single quotes either

## Input

### Input

##### Input From User

    # input-user.ps1
    
    $something=Read-Host "Say something here"
    Write-Output "you said" $something

##### Input From the Pipeline

    # input-pipe.ps1
    
    $something="$input"
    Write-Output "you said" $something

can be executed as

    Write-Output "hey hey!" | .\input-pipe.ps1

`$input` (another one of PowerShell’s builtin variables) is a special
variable which enumerates the incoming objects in the pipeline.

##### Input From Files

    # input-file.ps1
    
    $file=Get-Content hello.ps1
    Write-Output @file -Separator "`n" 

You can assign the entire output of a command directly to a variable.

### System commands

##### Input from System Commands

    # input-commands.ps1
    
    $name=(Get-CimInstance Win32_OperatingSystem).Name
    $kernel=(Get-CimInstance `
             Win32_OperatingSystem).Version
    Write-Output "I am running on $name, version" `
               "$kernel in $(Get-Location)" 

Using `$(expr)` inside a string will treat it as an *ad-hoc variable*
evaluating the expression `expr` and inserting the output into the
string.

## Conditions

### if/else

##### if/else

    # if.ps1
    
    if ($args.Length -ne 1) {
       $str = "usage: " +
              $MyInvocation.InvocationName +
              " <argument>"
       Write-Output $str
    }

### Operators

##### Comparison

| Operator | Meaning                  |
| :------: | :----------------------- |
|  `-lt`   | Less than                |
|  `-gt`   | Greater than             |
|  `-le`   | Less than or equal to    |
|  `-ge`   | Greater than or equal to |
|  `-eq`   | Equal to                 |
|  `-ne`   | Not equal to             |

Note that many other test operators (e.g. file tests) are used as
methods in the objects instead of separate operators.

##### Boolean

| Operator | Meaning |
| :------: | :------ |
|  `-not`  | Not     |
|   `!`    | Not     |
|  `-and`  | And     |
|  `-or`   | Or      |

    # if-num-string.ps1
    
    if ($args.Count -ne 2) {
       $str = "usage: " +
              $MyInvocation.InvocationName +
              " <argument> <argument>"
       Write-Output $str
       exit 1
    } elseif ($args[0] -gt $args[1]) {
       Write-Output "$($args[0]) larger than $($args[1])"
    } else {
       $str = "$($args[0]) smaller than or" +
              " equal to $($args[1])"
       Write-Output $str
    }
    if (Test-Path $args[0]) {
       if (!(Get-Item $args[0]).PSIsContainer) {
          Write-Output $args[0] "is a file"
       }
    }

There are not separate comparison operators for numbers and strings. Be
careful when comparing objects with different types. Behaviour might be
a bit strange (see page 209 of "Mastering PowerShell" by Weltner):

    $ 123 -lt "123.4"
       False
    $ 123 -lt "123.5"
       True

By forsing the type of the first operator, the second would be
converted:

    $ [int]123 -lt "123.4"
       True

A set of *file test operators* is not available since this functionality
is covered through cmdlets (e.g. `Test-Path`) and methods (e.g.
`PSIsContainer`).

##### Boolean example

    # if-bool.ps1
    
    if ((1 -eq 2) -and (1 -eq 1) -or (1 -eq 1)) {
       Write-Output "And has precedence"
    } else {
       Write-Output "Or has precedence"
    }
    
    # force OR precedence:
    
    if ((1 -eq 2) -and ((1 -eq 1) -or (1 -eq 1))) {
       Write-Output "And has precedence"
    } else {
       Write-Output "Or has precedence"
    }

AND is always (as known from mathematics courses) evaluated before OR
(binds more tightly). Write it down in logic (truth table) if you are
unsure.

### Switch/case

##### Switch/Case

    # switch.ps1
    
    $short = @{ yes="y"; nope="n" }
    $ans = Read-Host
    switch ($ans) {
       yes { Write-Output "yes" } 
       nope { Write-Output "nope"; break }
       {$short.ContainsKey("$ans")} `
          { Write-Output $short[$ans] }
       default {Write-Output "$ans`???"}
    }

Run example and see the difference between inputting `yes`, `nope` and
`nei`.

In the example above `{$short.ContainsKey("$ans")}` checks if the
content of `$ans` has an entry (matches a key) in the associative array
`$short`. `Switch` in PowerShell continues testing each case unless it
reads a `break`.

### Where

##### Where/Where-Object

    # where.ps1
    
    Get-ChildItem | Where-Object {$_.Length -gt 1KB}

`$_` represents the current object in the pipeline.

In a pipeline we use `Where-Object` and `ForEach-Object`, but when
processing a collection/array in a script we would use `Where` and
`ForEach` (in other words: without the `-object`).

We can use `KB`, `MB` and `GB` and PowerShell understands what we mean.

##### Exercise

Use `Get-Process` and `Where-Object` to

list all powershell processes

store the process table in an array `$procs`

list all processes with a working set greater than 10MB

## Iteration

### For

##### For loop

    # for.ps1
    
    for ($i=1;$i-le3;$i++) {
      Write-Output "$i"
    }
    
    # something more useful:
    
    $file=Get-ChildItem
    for ($i=0;$i-lt$file.Count;$i++) {
       if (!(Get-Item $file[$i]).PSIsContainer) {
          Write-Output $file[$i].Name "is a file"
       } else {
          Write-Output $file[$i].Name "is a directory"
       }
    }

Normally you would use `ForEach` instead of `for` since you can simplify
the first loop above like this:

    ForEach ($i in 1..3) {
      Write-Output "$i"
    }

### While

##### While

    # while.ps1
    
    while ($i -le 3) {
       Write-Output $i
       $i++
    }
    
    # something more useful:
    
    $file=Get-ChildItem
    $i=0
    while ($i -lt $file.Count) {
       if (!(Get-Item $file[$i]).PSIsContainer) {
          Write-Output $file[$i].Name "is a file"
       } else {
          Write-Output $file[$i].Name "is a directory"
       }
       $i++
    }

The `for` example converted to `while`.

### Foreach

##### Foreach loop

    # foreach.ps1
    
    foreach ($i in Get-ChildItem) {
       Write-Output $i.Name
    }
    
    # with associative arrays
    
    $user=@{
           "frodeh" = "Frode Haug";
           "monicas"  = "Monica Strand";
           "ivarm"  = "Ivar Moe"
           }
    foreach ($key in $user.Keys) {
       Write-Output $user[$key]
    }

In a pipeline we would use `ForEach-Object`.

##### ForEach

If we want to read from the pipeline and do stuff object by object:

    # foreach-pipe.ps1
    
    foreach ($i in $input) {
       $foo += @($i)
    }
    Write-Output "size of foo is" $foo.Count

or

    # foreach-object-pipe.ps1
    
    $input | ForEach-Object {
       $foo += @($_)
    }
    Write-Output "size of foo is" $foo.Count

    $ Get-ChildItem | ./foreach-object-pipe.ps1
    size of foo is 20

`$input` represents the pipeline and `$_` the current object in the
pipeline.

## Math

##### Operators

|   Operator    | Meaning  |
| :-----------: | :------- |
|      `+`      | Add      |
|      `-`      | Subtract |
|      `*`      | Multiply |
|      `/`      | Divide   |
| `%` & Modulus |          |

    # math.ps1
    
    Write-Output "3+5 is" (3+5)

    Write-Output "3+5 is" 3+5
    Write-Output "3+5 is" (3+5)
    Write-Output "3+5 is" $(3+5)
    Write-Output "3+5 is (3+5)"
    Write-Output "3+5 is $(3+5)"

## Functions

##### Functions

    # func.ps1
    
    # declare:
    function add($a, $b) {
       Write-Output "$a+$b is" ($a+$b)
    }
    # use:
    add 5.12 2.56

## RegExp

##### Regular expressions intro 1/5

Special/Meta-characters:

`  \ | ( ) [ ] { } ^ $ * + ? .  `

*These have to be protected with* `\`, e.g.  
`http://www\.hig\.no`

> To match `c:\temp`, you need to use the regex `c:\\temp`. As a string
> in C++ source code, this regex becomes `"c:\\\\ temp"`. Four
> backslashes to match a single one indeed.

(from <http://www.regular-expressions.info/characters.html>):

There are many different regular expression engines, which differs
mostly in features and speed. In this tutorial we will try to stick with
simple examples which will the same in most engines (perl, pcre,
extended posix, .NET, ...).

##### Regular expressions intro 2/5

Describing characters:

|   Operator    | Meaning                      |
| :-----------: | :--------------------------- |
|      `.`      | Any single character         |
|   `[abcd]`    | One of these characters      |
|   `[^abcd]`   | Any one but these characters |
| `[a-zA-Z0-9]` | A character in these ranges  |

##### Regular expressions intro 3/5

Grouping:

| Operator | Meaning |
| :------: | :------ |
|   `()`   | Group   |
|   `\|`   | OR      |

Anchoring:

| Operator | Meaning           |
| :------: | :---------------- |
|   `^`    | Beginning of line |
|   `$`    | End of line       |

##### Regular expressions intro 4/5

Repetition operators/Modifiers/Quantifiers:

| Operator | Meaning                        |
| :------: | :----------------------------- |
|   `?`    | 0 or 1 time                    |
|   `*`    | 0 or more times                |
|   `+`    | 1 or more times                |
|  `{N}`   | N times                        |
|  `{N,}`  | At least N times               |
| `{N,M}`  | At least N but not more than M |

Demo: four step example with  
`` cat a.html | ForEach-Object {if($_ -match REGEXP)` `` `{Write-Output
$matches[0]}}`

##### Regular expressions intro 5/5

Finding URLs in HTML:  
`(mailto|http)://[^"]*`

Each line should be an email address:  
`^[A-Za-z0-9._-]+@[A-Za-z0-9.-]+$`

Remember that regexp engines are most often greedy, they try to match as
much as possible, so using e.g. `.*` might match more than you were
planning for.

### PowerShell example

##### PowerShell example

    # regexp.ps1
    
    $input | ForEach-Object {
       if ($_ -match 
           "^[A-Za-z0-9._-]+@([A-Za-z0-9.-]+)$") {
          Write-Output "Valid email", $matches[0]
          Write-Output "Domain is", $matches[1]
       } else {
          Write-Output "Invalid email address!"
       }
    }

When we use regular expressions inside scripts, it is very useful to be
able to extract parts of the match. We can do this by specifying the
part with `(part)` and refer to it later using `$matches[1]`,
`$matches[2]`, etc. `$matches[0]` matches the entire expression.

<http://www.regular-expressions.info/powershell.html>

## PowerShell only

##### Advanced stuff

See the complete Mastering PowerShell book at

<http://powershell.com/cs/blogs/ebook/>

for much more of what you can do with PowerShell

## Credits

##### Credits

<span> <http://refcardz.dzone.com/refcardz/windows-powershell>  
<http://powershell.com/cs/blogs/ebook/>  
<http://technet.microsoft.com/en-us/library/ee692948.aspx>  
<http://www.techotopia.com/index.php/Windows_PowerShell_1.0_String_Quoting_and_Escape_Sequences>  
<http://dmitrysotnikov.wordpress.com/2008/11/26/input-gotchas/>  
<http://stackoverflow.com/questions/59819/how-do-i-create-a-custom-type-in-powershell-for-my-scripts-to-use>  
<http://www.powershellpro.com/powershell-tutorial-introduction/>  
<http://en.wikipedia.org/wiki/Windows_PowerShell>  
<http://www.johndcook.com/powershell.html>  
<http://www.regular-expressions.info/>  
OUSTERHOUT, J., “Scripting: Higher-Level Programming for the 21st
Century”, IEEE Computer, Vol. 31, No. 3, March 1998, pp. 23-30.)  
</span>
